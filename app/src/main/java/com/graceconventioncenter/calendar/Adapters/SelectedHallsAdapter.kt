package com.graceconventioncenter.calendar.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.graceconventioncenter.calendar.ApiInterface.HallsListDataResponse
import com.graceconventioncenter.calendar.R


class SelectedHallsAdapter(context: Context, list: ArrayList<HallsListDataResponse>) :
    RecyclerView.Adapter<SelectedHallsAdapter.ViewHolder>() {

    private var mContext: Context? = null
    private var mList: ArrayList<HallsListDataResponse>? = null

    init {
        this.mContext = context
        mList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {

        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_hall_name, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        holder.tv_name.text = mList!![position].name
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_name: TextView = view.findViewById(R.id.tv_name)
    }

}
    

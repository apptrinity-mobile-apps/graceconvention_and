package com.graceconventioncenter.calendar.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.graceconventioncenter.calendar.ApiInterface.BookingHistoryPaymentDataResponse
import com.graceconventioncenter.calendar.R
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class PaymentListAdapter(context: Context, list: ArrayList<BookingHistoryPaymentDataResponse>) :
    RecyclerView.Adapter<PaymentListAdapter.ViewHolder>() {

    private var mContext: Context? = null
    private var mList: ArrayList<BookingHistoryPaymentDataResponse>? = null

    init {
        this.mContext = context
        mList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {

        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_paymnent_historyview, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        val inputFormat =
            SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        val outputFormat = SimpleDateFormat("dd MMMM, yyyy", Locale.getDefault())
        val inputDateStr = mList!![position].created_date
        val date = inputFormat.parse(inputDateStr!!)
        val outputDateStr = outputFormat.format(date!!)
        if (mList!![position].staff_name.isNullOrEmpty()){
            holder.tv_payment_itemview.text =
                outputDateStr + " Paid Rs " + mList!![position].amount + " by " + mList!![position].pay_type
        }else{
            holder.tv_payment_itemview.text =
                outputDateStr + " Paid Rs " + mList!![position].amount + " by " + mList!![position].pay_type + " received by " + mList!![position].staff_name
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_payment_itemview: TextView = view.findViewById(R.id.tv_payment_itemview)

    }

}
    

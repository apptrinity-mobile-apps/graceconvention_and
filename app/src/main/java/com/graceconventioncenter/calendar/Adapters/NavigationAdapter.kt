package com.graceconventioncenter.calendar.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.graceconventioncenter.calendar.R

class NavigationAdapter(context: Context, title: Array<String>) :
    RecyclerView.Adapter<NavigationAdapter.ViewHolder>() {

    private var mContext: Context? = null
    private var mTitle: Array<String>? = null
    private var selected = -1

    init {
        this.mContext = context
        this.mTitle = title
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {

        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_navigation_drawer, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return mTitle!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        holder.tv_item_nav_header.text = mTitle!![position]
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_item_nav_header = view.findViewById(R.id.tv_item_nav_header) as TextView
    }

    fun getItem(position: Int): String {
        return mTitle!![position]
    }

    fun setSelected(position: Int) {
        selected = position
    }
}
    

package com.graceconventioncenter.calendar.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.graceconventioncenter.calendar.ApiInterface.BookingHistoryFeatureDataResponse
import com.graceconventioncenter.calendar.ApiInterface.BookingHistoryPaymentDataResponse
import com.graceconventioncenter.calendar.R
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class FeaturesListAdapter(context: Context, list: ArrayList<BookingHistoryFeatureDataResponse>) :
    RecyclerView.Adapter<FeaturesListAdapter.ViewHolder>() {

    private var mContext: Context? = null
    private var mList: ArrayList<BookingHistoryFeatureDataResponse>? = null

    init {
        this.mContext = context
        mList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {

        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_feature_historyview, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {

        holder.tv_name.text = mList!![position].name
        holder.tv_amount.text ="Rs ${mList!![position].no_of_persons!!.toInt()*mList!![position].feature_amount!!.toInt()}"
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_name: TextView = view.findViewById(R.id.tv_name)
        var tv_amount: TextView = view.findViewById(R.id.tv_amount)

    }

}
    

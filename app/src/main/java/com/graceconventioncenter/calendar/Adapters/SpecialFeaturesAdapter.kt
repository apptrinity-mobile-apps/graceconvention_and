package com.graceconventioncenter.calendar.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.graceconventioncenter.calendar.ApiInterface.FeaturesDataResponse
import com.graceconventioncenter.calendar.R


class SpecialFeaturesAdapter(context: Context, list: ArrayList<FeaturesDataResponse>) :
    RecyclerView.Adapter<SpecialFeaturesAdapter.ViewHolder>() {

    private var mContext: Context? = null
    private var mList: ArrayList<FeaturesDataResponse>? = null

    init {
        this.mContext = context
        mList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {

        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_feature, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        var isButtonChecked = false
        holder.tv_amount.text = mList!![position].amount
        holder.checkBox.text = mList!![position].name
        holder.checkBox.setOnCheckedChangeListener { compoundButton, isChecked ->
            isButtonChecked = isChecked
            Log.d("adapter","checkbox====$isButtonChecked")
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var checkBox: CheckBox = view.findViewById(R.id.checkBox)
        var tv_amount: TextView = view.findViewById(R.id.tv_amount)
    }

}
    

package com.graceconventioncenter.calendar.ApiInterface

class CouponCodeResponse {

    val status: String? = null
    val result: String? = null
    val coupondata: CouponDataResponse? = null

}

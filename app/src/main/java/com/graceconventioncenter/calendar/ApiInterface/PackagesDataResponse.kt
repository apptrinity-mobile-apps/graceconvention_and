package com.graceconventioncenter.calendar.ApiInterface

class PackagesDataResponse {

    val id: String? = null
    val name: String? = null
    val amount: String? = null
    val status: String? = null
    val image: String? = null
    val created: String? = null
    val hall_names: String? = null
    val available:ArrayList<PackagesDatesavailableResponse>? = null
}

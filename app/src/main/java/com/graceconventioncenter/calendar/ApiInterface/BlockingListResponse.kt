package com.graceconventioncenter.calendar.ApiInterface

class BlockingListResponse {
    val status: String? = null
    val result: String? = null
    val blockingList:ArrayList<BlockingDataList>?=null

    /*{
    "status": "1",
    "result": "success",
    "blockingList": [
        {
            "blocking_id": "1",
            "unique_id": "GCC000001",
            "staff_id": "3",
            "first_name": "gig8g8",
            "last_name": "c7fuf",
            "mobile": "5383838",
            "alt_mobile": "838383",
            "email": "v7c7f7f6",
            "city": "cucu",
            "state": "7fgigigub8",
            "pincode": "6",
            "address": "7g7g6g6g",
            "message": "g77g7g7g",
            "from_date": "2020-03-30",
            "to_date": "2020-03-30",
            "session_type": "First Half",
            "type_of_event": "ygff7g7g7g",
            "blocking_status": "1",
            "created_on": "2020-03-30 21:37:56"
        }
    ]
}*/

}
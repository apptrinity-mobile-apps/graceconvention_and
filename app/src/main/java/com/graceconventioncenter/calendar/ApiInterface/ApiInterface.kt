package com.graceconventioncenter.calendar.ApiInterface


import com.google.gson.JsonObject
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit


interface ApiInterface {

    @FormUrlEncoded
    @POST("login")
    fun loginApi(
        @Field("email") email: String,
        @Field("password") password: String
    ): Call<LoginResponse>

    @Headers("Content-type: application/json")
    @POST("hallBooking")
    fun hallBookingApi(@Body booking: JsonObject): Call<BookedListResponse>

    @Headers("Content-type: application/json")
    @POST("hallBlocking")
    fun hallBlockingApi(@Body booking: JsonObject): Call<BookedListResponse>

    @Headers("Content-type: application/json")
    @POST("editBooking")
    fun editBookingApi(@Body booking: JsonObject): Call<BookedListResponse>

    @Headers("Content-type: application/json")
    @POST("bookingDaywiseCount")
    fun bookingDayWiseCountApi(@Body count: JsonObject): Call<DayWiseBookingCountResponse>

    @Headers("Content-type: application/json")
    @POST("bookingHistoryByMonth")
    fun bookingmonthwiseApi(@Body count: JsonObject): Call<BookingHistoryResponse>

    @FormUrlEncoded
    @POST("hallsList")
    fun hallsListApi(@Field("booking_date") booking_date: String): Call<HallsListResponse>

    @FormUrlEncoded
    @POST("checkCoupon")
    fun checkCouponApi(@Field("coupon_code") coupon_code: String): Call<CouponCodeResponse>

    @POST("featureList")
    fun featureListApi(): Call<FeaturesResponse>

    @FormUrlEncoded
    @POST("bookingHistory")
    fun bookingHistoryApi(@Field("search") search: String): Call<BookingHistoryResponse>

    @Headers("Content-type: application/json")
    @POST("makePayment")
    fun makePaymentApi(@Body payment: JsonObject): Call<LoginResponse>

    @Headers("Content-type: application/json")
    @POST("changeToBooking")
    fun makeBlockingPaymentApi(@Body payment: JsonObject): Call<LoginResponse>

    @FormUrlEncoded
    @POST("bookingHistorySingleView")
    fun bookingHistorySingleViewApi(@Field("booking_id") booking_id: String): Call<BookingHistoryDetailViewResponse>

    @FormUrlEncoded
    @POST("forgotpass")
    fun forgotpassApi(
        @Field("staff_email") staff_email: String
    ): Call<LoginResponse>

    @FormUrlEncoded
    @POST("cancelBooking")
    fun cancelBookingApi(
        @Field("booking_id") booking_id: String,
        @Field("reason") reason: String
    ): Call<LoginResponse>

    @FormUrlEncoded
    @POST("unBlocking")
    fun unBlockingApi(
        @Field("booking_id") booking_id: String
    ): Call<LoginResponse>

    @FormUrlEncoded
    @POST("singleCancelBooking")
    fun singleCancelBookingApi(
        @Field("booking_id") booking_id: String,
        @Field("hall_id") hall_id: String,
        @Field("reason") reason: String
    ): Call<LoginResponse>

    @FormUrlEncoded
    @POST("changePassword")
    fun changePasswordApi(
        @Field("staff_id") staff_id: String,
        @Field("curr_password") Curr_password: String,
        @Field("new_password") new_password: String
    ): Call<LoginResponse>
    @Headers("Content-type: application/json")
    @POST("addEnquiry")
    fun addEnquiryApi(@Body addenquiry: JsonObject): Call<AddEnquiryResponse>
    @GET("enquiryList")
    fun getenquiryListApi():Call<EnquiryListResponse>

    @Headers("Content-type: application/json")
    @POST("getPackages")
    fun getPackagesApi(@Body getpackages: JsonObject): Call<PackagesResponse>
    @GET("blockingList")
    fun getblockingListApi():Call<BlockingListResponse>

    companion object Factory {

        private val BASE_URL = "http://18.188.199.56/gconvention/api/"

        private val client = OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)

        fun create(): ApiInterface {
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client.build())
                .build()
            return retrofit.create(ApiInterface::class.java)
        }
    }
}
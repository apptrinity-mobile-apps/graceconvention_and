package com.graceconventioncenter.calendar.ApiInterface

class LoginResponse {

    val status: String? = null
    val result: String? = null
    val admin_info: LoginDataResponse? = null

}

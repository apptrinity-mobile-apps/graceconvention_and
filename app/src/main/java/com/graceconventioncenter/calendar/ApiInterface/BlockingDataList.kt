package com.graceconventioncenter.calendar.ApiInterface

class BlockingDataList {
    /*   "blocking_id": "1",
            "unique_id": "GCC000001",
            "staff_id": "3",
            "first_name": "gig8g8",
            "last_name": "c7fuf",
            "mobile": "5383838",
            "alt_mobile": "838383",
            "email": "v7c7f7f6",
            "city": "cucu",
            "state": "7fgigigub8",
            "pincode": "6",
            "address": "7g7g6g6g",
            "message": "g77g7g7g",
            "from_date": "2020-03-30",
            "to_date": "2020-03-30",
            "session_type": "First Half",
            "type_of_event": "ygff7g7g7g",
            "blocking_status": "1",
            "created_on": "2020-03-30 21:37:56"*/

    val booking_id:String?=null
    val unique_id:String?=null
    val staff_id:String?=null
    val first_name:String?=null
    val last_name:String?=null
    val mobile:String?=null
    val alt_mobile:String?=null
    val email:String?=null
    val city:String?=null
    val state:String?=null
    val pincode:String?=null
    val address:String?=null
    val message:String?=null
    val from_date:String?=null
    val to_date:String?=null
    val session_type:String?=null
    val type_of_event:String?=null
    val blocking_status:String?=null
    val created_on:String?=null


}
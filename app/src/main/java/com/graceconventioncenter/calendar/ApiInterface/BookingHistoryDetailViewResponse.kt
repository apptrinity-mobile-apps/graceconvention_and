package com.graceconventioncenter.calendar.ApiInterface

class BookingHistoryDetailViewResponse {

    val status: String? = null
    val result: String? = null
    val data: BookingHistoryDetailDataResponse? = null

}

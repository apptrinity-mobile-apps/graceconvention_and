package com.graceconventioncenter.calendar.ApiInterface

class BookingHistoryFeatureDataResponse {

    val booking_id: String? = null
    val id: String? = null
    val feature_id: String? = null
    val feature_amount: String? = null
    val name: String? = null
    val no_of_persons: String? = null

}

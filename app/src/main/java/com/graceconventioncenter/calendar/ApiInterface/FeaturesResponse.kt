package com.graceconventioncenter.calendar.ApiInterface

class FeaturesResponse {

    val status: String? = null
    val result: String? = null
    val details: ArrayList<FeaturesDataResponse>? = null

}

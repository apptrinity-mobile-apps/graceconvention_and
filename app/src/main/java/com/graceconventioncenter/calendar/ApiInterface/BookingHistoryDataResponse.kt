package com.graceconventioncenter.calendar.ApiInterface

class BookingHistoryDataResponse {

    val booking_id: String? = null
    val unique_id: String? = null
    val first_name: String? = null
    val last_name: String? = null
    val grand_total: String? = null
    val booking_status: String? = null
    val booking_date: String? = null
    val eventtype: String? = null

}

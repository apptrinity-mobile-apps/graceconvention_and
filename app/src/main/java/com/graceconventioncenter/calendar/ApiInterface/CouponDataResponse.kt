package com.graceconventioncenter.calendar.ApiInterface

class CouponDataResponse {

    val discount_code: String? = null
    val amount: String? = null
    val amount_type: String? = null
    val id: String? = null

}

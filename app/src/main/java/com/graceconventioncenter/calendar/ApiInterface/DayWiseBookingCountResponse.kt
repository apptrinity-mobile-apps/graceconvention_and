package com.graceconventioncenter.calendar.ApiInterface

class DayWiseBookingCountResponse {
    val status: String? = null
    val result: String? = null
    val data:ArrayList<DayWiseBookingCountDataResponse>? = null
    val specialday:ArrayList<SpecialDayDataResponse>? = null
    val blockeddays:ArrayList<BlockedDayDataResponse>? = null
}

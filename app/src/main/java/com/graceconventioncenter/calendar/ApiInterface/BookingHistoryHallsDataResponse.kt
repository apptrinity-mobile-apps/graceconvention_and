package com.graceconventioncenter.calendar.ApiInterface

class BookingHistoryHallsDataResponse {

    val booking_id: String? = null
    val hall_id: String? = null
    val booked_on: String? = null
    val name: String? = null
    val amount: String? = null
    val booking_status: String? = null
    val reason: String? = null
    val package_halls: String? = null
    val session_type: String? = null
    val available:ArrayList<PackagesDatesavailableResponse>? = null
}

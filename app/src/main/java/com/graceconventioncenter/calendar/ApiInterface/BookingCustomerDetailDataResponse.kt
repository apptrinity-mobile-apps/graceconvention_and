package com.graceconventioncenter.calendar.ApiInterface

class BookingCustomerDetailDataResponse {
    val booking_status: String? = null
    val booking_date: String? = null
    val booking_id: String? = null
    val first_name: String? = null
    val last_name: String? = null
    val mobile: String? = null
    val alt_mobile: String? = null
    val email: String? = null
    val paid_amount: String? = null
    val message: String? = null
    val id: String? = null
    val hall_id: String? = null
    val hall_amount: String? = null
    val session_type: String? = null
    val booked_on: String? = null
    val reason: String? = null
    val package_halls: String? = null
    val booking_type: String? = null
    val unique_id: String? = null
    val staff_id: String? = null
    val address: String? = null
    val city: String? = null
    val state: String? = null
    val pincode: String? = null
    val type_of_event: String? = null
    val session: String? = null
    val total_price: String? = null
}

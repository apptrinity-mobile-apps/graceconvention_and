package com.graceconventioncenter.calendar.ApiInterface

class LoginDataResponse {

    val staff_id: String? = null
    val staff_name: String? = null
    val staff_email: String? = null
    val staff_password: String? = null
    val staff_mobile_number: String? = null
    val status: String? = null
    val admin_status: String? = null

}

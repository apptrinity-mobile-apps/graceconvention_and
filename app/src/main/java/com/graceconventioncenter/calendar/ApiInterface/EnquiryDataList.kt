package com.graceconventioncenter.calendar.ApiInterface

class EnquiryDataList {
    /*  "enquiry_id": "1",
            "staff_id": "3",
            "enquiry_date": "2020-03-26",
            "name": "naveen",
            "email": "naveen@gmail.com",
            "address": "Bdbdbdb",
            "mobile_number": "123456789",
            "function_date": "2020-03-31",
            "event_type": "wedding ",
            "event_time": "21:03:00",
            "status": "1",
            "created_on": "2020-03-28 17:03:35"*/

    val enquiry_id:String?=null
    val staff_id:String?=null
    val enquiry_date:String?=null
    val name:String?=null
    val email:String?=null
    val address:String?=null
    val mobile_number:String?=null
    val function_date:String?=null
    val event_type:String?=null
    val event_time:String?=null
    val created_on:String?=null

}
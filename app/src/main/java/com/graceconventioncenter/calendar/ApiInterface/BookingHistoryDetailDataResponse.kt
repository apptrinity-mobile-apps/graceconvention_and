package com.graceconventioncenter.calendar.ApiInterface

import java.io.Serializable

class BookingHistoryDetailDataResponse : Serializable {

    val booking_id: String? = null
    val first_name: String? = null
    val last_name: String? = null
    val email: String? = null
    val alt_mobile: String? = null
    val mobile: String? = null
    val booking_date: String? = null
    val booking_status: String? = null
    val booked_on: String? = null
    val to_date: String? = null
    val session: String? = null
    val address: String? = null
    val city: String? = null
    val state: String? = null
    val pincode: String? = null
    val type_of_event: String? = null

    val halls_details: ArrayList<BookingHistoryHallsDataResponse>? = null
    val payment_detals: ArrayList<BookingHistoryPaymentDataResponse>? = null
    val feature_detals: ArrayList<BookingHistoryFeatureDataResponse>? = null

    val base_price:Double?=null
    val spcl_price:Double?=null
    val total_price:Double?=null
    val paid_amount:Double?=null
    val discount_total_amount:Double?=null



    val staff_discount_type:String?=null
    val staff_discount_amount:Double?=null
    val staff_discount_total:Double?=null
    val due_amount:Double?=null

}

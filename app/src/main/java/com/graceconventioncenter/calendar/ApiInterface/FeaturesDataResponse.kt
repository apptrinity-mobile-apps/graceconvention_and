package com.graceconventioncenter.calendar.ApiInterface

class FeaturesDataResponse {

    val id: String? = null
    val name: String? = null
    val amount: String? = null
    val status: String? = null

}

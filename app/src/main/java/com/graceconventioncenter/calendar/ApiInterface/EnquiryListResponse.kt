package com.graceconventioncenter.calendar.ApiInterface

class EnquiryListResponse {
    val status: String? = null
    val result: String? = null
    val enqList:ArrayList<EnquiryDataList>?=null

    /*{
    "status": "1",
    "result": "success",
    "enqList": [
        {
            "enquiry_id": "1",
            "staff_id": "3",
            "enquiry_date": "2020-03-26",
            "name": "naveen",
            "email": "naveen@gmail.com",
            "address": "Bdbdbdb",
            "mobile_number": "123456789",
            "function_date": "2020-03-31",
            "event_type": "wedding ",
            "event_time": "21:03:00",
            "status": "1",
            "created_on": "2020-03-28 17:03:35"
        },
        {
            "enquiry_id": "2",
            "staff_id": "3",
            "enquiry_date": "1970-01-01",
            "name": "testnoew",
            "email": "testnoew@gmail.com",
            "address": "testnoew",
            "mobile_number": "testnoew",
            "function_date": "1970-01-01",
            "event_type": "testnoew",
            "event_time": "00:00:00",
            "status": "1",
            "created_on": "2020-03-28 18:03:38"
        },
        {
            "enquiry_id": "3",
            "staff_id": "3",
            "enquiry_date": "2020-03-31",
            "name": "testcase ",
            "email": "test@gmail.com",
            "address": "Bdbdbdbbdbdb. Bdbdbbdbd.  Dhdhhdbdbdbd. Shbsbbvbdvdbdbbdbdbd. Dbbsb",
            "mobile_number": "1234567890",
            "function_date": "2020-04-16",
            "event_type": "marriage ",
            "event_time": "05:03:00",
            "status": "1",
            "created_on": "2020-03-28 19:09:34"
        }
    ]
}*/

}
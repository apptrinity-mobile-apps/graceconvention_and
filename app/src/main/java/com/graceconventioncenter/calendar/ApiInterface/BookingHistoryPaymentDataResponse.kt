package com.graceconventioncenter.calendar.ApiInterface

class BookingHistoryPaymentDataResponse {

    val booking_id: String? = null
    val amount: String? = null
    val pay_type: String? = null
    val created_date: String? = null
    val id: String? = null
    val bank_name : String? = null
    val chqno: String? = null
    val ifsc: String? = null
    val account_number: String? = null
    val ac_holder: String? = null
    val message: String? = null
    val staff_name: String? = null

}

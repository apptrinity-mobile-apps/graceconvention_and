package com.graceconventioncenter.calendar.ApiInterface

import java.io.Serializable

class HallsListDataResponse : Serializable {
    val name: String? = null
    val hall_id: String? = null
    val halls_list: String? = null
    val base_price: String? = null
    val customer: ArrayList<BookingCustomerDetailDataResponse>? = null
}

package com.graceconventioncenter.calendar.ApiInterface

class DayWiseBookingCountDataResponse {
    val count: String? = null
    val booked_on: String? = null
    val first_half: String? = null
    val second_half: String? = null
    val full_day: String? = null

}

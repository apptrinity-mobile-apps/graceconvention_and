package com.graceconventioncenter.calendar.ApiInterface

class HallsListResponse {

    val status: String? = null
    val result: String? = null
    val halls_list: ArrayList<HallsListDataResponse>? = null

}

package com.graceconventioncenter.calendar.Helper

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences

@SuppressLint("CommitPrefEdits")
class SessionManager(context: Context) {

    private var pref: SharedPreferences
    private var editor: SharedPreferences.Editor
    private var PRIVATE_MODE = 0

    init {
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        editor = pref.edit()
    }

    val isLoggedIn: Boolean
        get() = pref.getBoolean(IS_LOGGED_IN, false)

    fun loginSession(email: String, name: String, id:String) {
        editor.putBoolean(IS_LOGGED_IN, true)
        editor.putString(KEY_EMAIL, email)
        editor.putString(KEY_NAME, name)
        editor.putString(KEY_ID, id)

        editor.commit()
    }

    fun getUserDetails(): HashMap<String, String> {
        val user = HashMap<String, String>()
        user[KEY_USERNAME] = pref.getString(
            KEY_USERNAME, "")!!
        user[KEY_EMAIL] = pref.getString(
            KEY_EMAIL, "")!!
        user[KEY_NAME] = pref.getString(
            KEY_NAME, "")!!
        user[KEY_IMAGE] = pref.getString(
            KEY_IMAGE, "")!!
        user[KEY_ID] = pref.getString(
            KEY_ID, "")!!

        return user
    }

    fun logout() {
        editor.clear()
        editor.commit()
    }

    companion object {
        private val PREF_NAME = "GraceConvention"
        const val IS_LOGGED_IN = "isLoggedIn"
        const val KEY_USERNAME = "username"
        const val KEY_ID = "id"
        const val KEY_NAME = "name"
        const val KEY_EMAIL = "email"
        const val KEY_IMAGE = "image"
    }

}
package com.graceconventioncenter.calendar.Fragments

import android.annotation.SuppressLint
import android.app.Dialog
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.graceconventioncenter.calendar.Activity.ConnectionDetector
import com.graceconventioncenter.calendar.Activity.NavigationDrawerActivity
import com.graceconventioncenter.calendar.ApiInterface.ApiInterface
import com.graceconventioncenter.calendar.ApiInterface.BookingHistoryDetailDataResponse
import com.graceconventioncenter.calendar.ApiInterface.LoginResponse
import com.graceconventioncenter.calendar.Helper.SessionManager
import com.graceconventioncenter.calendar.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class PaymentFragment : Fragment() {

    private var rootview: View? = null
    lateinit var sessionManager: SessionManager
    lateinit var cd: ConnectionDetector
    private var isInternetConnected: Boolean? = false
    private lateinit var user_details: HashMap<String, String>
    private var user_name = ""
    private lateinit var iv_back: ImageView
    private lateinit var iv_nav_bar: ImageView
    private lateinit var tv_submit: TextView
    private lateinit var loading_dialog: Dialog
    private lateinit var et_paid_amount: EditText
    private lateinit var sp_payment_type: Spinner
    lateinit var ll_online_payment: LinearLayout
    private lateinit var et_acc_holder: EditText
    private lateinit var et_acc_no: EditText
    private lateinit var et_cheque_no: EditText
    private lateinit var et_bank_name: EditText
    private lateinit var et_ifsc: EditText
    private lateinit var et_message: EditText
    private lateinit var tv_total_due_id: TextView
    var payment_type = ""
    var booking_id_stg = ""
    var due_stg = ""
    var staff_id = ""
    private lateinit var paymentAdapter: ArrayAdapter<String>
    var obj = JSONObject()

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {

        if (rootview != null) {
            val parent = rootview!!.parent as ViewGroup
            parent.removeView(rootview)
        }
        try {
            rootview = inflater.inflate(R.layout.fragment_make_payment, container, false)
        } catch (e: InflateException) {
            e.printStackTrace()
        }

        loadingDialog()

        val bundle = arguments!!
        val customer_info =
            bundle.getSerializable("customer_info") as BookingHistoryDetailDataResponse
        booking_id_stg = customer_info.booking_id!!
        due_stg = customer_info.due_amount.toString()!!

        Log.e("due_stg", due_stg)
        initialize(rootview)
        user_details = sessionManager.getUserDetails()
        user_name = user_details[SessionManager.KEY_NAME]!!
        staff_id = user_details[SessionManager.KEY_ID]!!
        return rootview
    }

    private fun initialize(view: View?) {

        sessionManager =
            SessionManager(activity!!)
        cd = ConnectionDetector(
            activity!!
        )
        isInternetConnected = cd.isConnectingToInternet
        user_details = HashMap()

        iv_nav_bar = view!!.findViewById(R.id.iv_nav_bar)
        iv_back = view.findViewById(R.id.iv_back)
        tv_submit = view.findViewById(R.id.tv_submit)
        et_paid_amount = view.findViewById(R.id.et_paid_amount)
        sp_payment_type = view.findViewById(R.id.sp_payment_type)
        ll_online_payment = view.findViewById(R.id.ll_online_payment)
        et_acc_holder = view.findViewById(R.id.et_acc_holder)
        et_acc_no = view.findViewById(R.id.et_acc_no)
        et_cheque_no = view.findViewById(R.id.et_cheque_no)
        et_bank_name = view.findViewById(R.id.et_bank_name)
        et_ifsc = view.findViewById(R.id.et_ifsc)
        et_message = view.findViewById(R.id.et_message)
        tv_total_due_id = view.findViewById(R.id.tv_total_due_id)

        tv_total_due_id.text = " : Rs " + due_stg

        iv_nav_bar.setOnClickListener {
            NavigationDrawerActivity.openDrawer()
        }

        paymentAdapter = object : ArrayAdapter<String>(
            activity!!,
            R.layout.spinner_item,
            resources.getStringArray(R.array.payment_types)
        ) {
            override fun isEnabled(position: Int): Boolean {
                return position != 0
            }

            override fun getDropDownView(
                position: Int,
                convertView: View?,
                parent: ViewGroup
            ): View {
                val view = super.getDropDownView(position, convertView, parent)
                val tv = view.findViewById(R.id.tv_spinner_header) as TextView
                if (position == 0) {
                    tv.setTextColor(Color.GRAY)
                }
                return view
            }
        }
        paymentAdapter.setDropDownViewResource(R.layout.spinner_item)

        iv_back.setOnClickListener {
            if (NavigationDrawerActivity.navigation_drawer.isDrawerOpen(GravityCompat.START)) {
                NavigationDrawerActivity.navigation_drawer.closeDrawer(GravityCompat.START)
            } else if (activity!!.supportFragmentManager.backStackEntryCount > 0) {
                activity!!.supportFragmentManager.popBackStack()
            }
        }

        tv_submit.setOnClickListener {
            if (isInternetConnected!!) {
                if (isValid()) {
                    obj.put("booking_id", booking_id_stg)
                    obj.put("amount", et_paid_amount.text.toString())
                    obj.put("pay_type", payment_type)
                    obj.put("message", et_message.text.toString())
                    obj.put("ifsc", et_ifsc.text.toString())
                    obj.put("bank_name", et_bank_name.text.toString())
                    obj.put("chqno", et_cheque_no.text.toString())
                    obj.put("account_number", et_acc_no.text.toString())
                    obj.put("ac_holder", et_acc_holder.text.toString())
                    obj.put("staff_id", staff_id)

                    Log.d("PaymentFragment", obj.toString())
                    val jsonParser = JsonParser()
                    val jsonObject = jsonParser.parse(obj.toString()) as JsonObject
                    makePaymentApi(jsonObject)
                }
            } else {
                Toast.makeText(activity!!, "No network connection", Toast.LENGTH_SHORT)
                    .show()
            }
        }

        sp_payment_type.adapter = paymentAdapter

        sp_payment_type.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parentView: AdapterView<*>, selectedItemView: View, position: Int, id: Long
            ) {
                (parentView.getChildAt(0) as TextView).setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.text_color
                    )
                )
                payment_type = parentView.getItemAtPosition(position).toString()
                sp_payment_type.setSelection(position)
                et_acc_holder.setText("")
                et_acc_no.setText("")
                et_ifsc.setText("")
                et_cheque_no.setText("")
                et_bank_name.setText("")
                Log.d("BookingFragment", "payment_type : $payment_type")
                when {
                    payment_type.equals(resources.getString(R.string.cash), true) -> {
                        ll_online_payment.visibility = View.GONE
                        et_cheque_no.visibility = View.GONE
                    }
                    payment_type.equals(resources.getString(R.string.online), true) -> {
                        ll_online_payment.visibility = View.GONE
                        et_acc_holder.visibility = View.GONE
                        et_acc_no.visibility = View.GONE
                        et_ifsc.visibility = View.GONE
                        et_cheque_no.visibility = View.GONE
                    }
                    payment_type.equals(resources.getString(R.string.cheque), true) -> {
                        ll_online_payment.visibility = View.GONE
                        et_cheque_no.visibility = View.GONE
                        et_acc_holder.visibility = View.GONE
                        et_acc_no.visibility = View.GONE
                        et_ifsc.visibility = View.GONE
                    }
                }
            }

            override fun onNothingSelected(parentView: AdapterView<*>) {
                // do nothing
            }
        }
    }

    private fun isValid(): Boolean {

        if (et_paid_amount.text.toString().isEmpty()) {
            Toast.makeText(activity!!, "Field should not be empty", Toast.LENGTH_SHORT)
                .show()
        } else if (et_message.text.toString().isEmpty()) {
            Toast.makeText(activity!!, "Field should not be empty", Toast.LENGTH_SHORT)
                .show()
        } /*else if (payment_type.equals(resources.getString(R.string.online), true)) {
            if (et_acc_holder.text.toString().isEmpty()) {
                Toast.makeText(activity!!, "Field should not be empty", Toast.LENGTH_SHORT)
                    .show()
            } else if (et_acc_holder.text.toString().isEmpty()) {
                Toast.makeText(activity!!, "Field should not be empty", Toast.LENGTH_SHORT)
                    .show()
            } else if (et_ifsc.text.toString().isEmpty()) {
                Toast.makeText(activity!!, "Field should not be empty", Toast.LENGTH_SHORT)
                    .show()
            } else if (et_bank_name.text.toString().isEmpty()) {
                Toast.makeText(activity!!, "Field should not be empty", Toast.LENGTH_SHORT)
                    .show()
            } else {
                return true
            }
        } else if (payment_type.equals(resources.getString(R.string.cheque), true)) {
            if (et_cheque_no.text.toString().isEmpty()) {
                Toast.makeText(activity!!, "Field should not be empty", Toast.LENGTH_SHORT)
                    .show()
            } else if (et_bank_name.text.toString().isEmpty()) {
                Toast.makeText(activity!!, "Field should not be empty", Toast.LENGTH_SHORT)
                    .show()
            } else {
                return true
            }
        }*/ else {
            return true
        }
        return false
    }

    private fun makePaymentApi(final_object: JsonObject) {

        loading_dialog.show()

        val apiInterface = ApiInterface.create()
        val call = apiInterface.makePaymentApi(final_object)

        call.enqueue(object : Callback<LoginResponse> {
            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                try{
                    Log.d("EditBookingFragment", "makePaymentApi error $t")

                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(activity!!, "Please try again!", Toast.LENGTH_SHORT)
                        .show()
                }catch (e:Exception){
                    e.printStackTrace()
                }

            }

            override fun onResponse(
                call: Call<LoginResponse>,
                response: Response<LoginResponse>
            ) {

                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Log.d(
                        "EditBookingFragment",
                        "makePaymentApi success ${response.body()!!.result}"
                    )
                    val responsee = response.body()
                    if (responsee!!.status == "1") {
                        // go to previous fragment
                        Toast.makeText(activity!!, "Payment successful!", Toast.LENGTH_SHORT)
                            .show()
                        if (activity!!.supportFragmentManager.backStackEntryCount > 0) {
                            activity!!.supportFragmentManager.popBackStack()
                        }
                    } else {
                        Toast.makeText(activity!!, responsee.result, Toast.LENGTH_SHORT)
                            .show()
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(activity!!)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view = LayoutInflater.from(activity!!).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

}
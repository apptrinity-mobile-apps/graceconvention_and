package com.graceconventioncenter.calendar.Fragments

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.core.util.Preconditions
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.graceconventioncenter.calendar.Activity.ConnectionDetector
import com.graceconventioncenter.calendar.Activity.NavigationDrawerActivity
import com.graceconventioncenter.calendar.ApiInterface.*
import com.graceconventioncenter.calendar.Helper.SessionManager
import com.graceconventioncenter.calendar.R
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap


class BlockingtobookingPaymentFragment : Fragment() {

    private var rootview: View? = null
    lateinit var sessionManager: SessionManager
    lateinit var cd: ConnectionDetector
    private var isInternetConnected: Boolean? = false
    private lateinit var user_details: HashMap<String, String>
    private var user_name = ""
    private lateinit var iv_back: ImageView
    private lateinit var iv_nav_bar: ImageView
    private lateinit var tv_submit: TextView
    private lateinit var loading_dialog: Dialog
    private lateinit var et_paid_amount: EditText
    private lateinit var sp_payment_type: Spinner
    lateinit var ll_online_payment: LinearLayout
    private lateinit var et_acc_holder: EditText
    private lateinit var et_acc_no: EditText
    private lateinit var et_cheque_no: EditText
    private lateinit var et_bank_name: EditText
    private lateinit var et_ifsc: EditText
    private lateinit var et_message: EditText
    private lateinit var et_dicsount: EditText
    private lateinit var tv_total_due_id: TextView
    var payment_type = ""
    var booking_id_stg = ""
    var due_stg = ""
    var staff_id = ""
    var session_type = ""
    var grand_total = ""
    var total_price = ""
    private lateinit var paymentAdapter: ArrayAdapter<String>
    var obj = JSONObject()


    private lateinit var sessionradiogroup: RadioGroup
    private lateinit var discountradiogroup: RadioGroup
    private lateinit var et_from: EditText
    private lateinit var et_todate: EditText
    var postfromdate = ""
    var posttodate = ""
    lateinit var packagesList: ArrayList<PackagesDataResponse>
    lateinit var featuresList: ArrayList<FeaturesDataResponse>
    lateinit var spl_packages_list: ArrayList<PackagesDataResponse>
    lateinit var spl_features_list: ArrayList<FeaturesDataResponse>
    var jsarray_packages_features: JSONArray = JSONArray();
    lateinit var jsarray_spl_features:JSONArray
    private lateinit var rv_selected_halls: RecyclerView
    private lateinit var rv_spl_features: RecyclerView
    val hashMap:HashMap<Int,String> = HashMap<Int,String>() //define empty hashmap
    var base_price = 0
    var pkg_prices = 0
    var coupon_price = 0
    var discount_price = 0.00
    var isCouponClicked = false
    var isDiscountClicked = false
    var discount_code = ""
    var discount_type = ""
    var discount_amount = ""
    var discount_total_amount = ""
    var paid_amount = ""
    var spl_prices = 0
    var disc_type = "amount"
    lateinit var ll_spl_amount: LinearLayout
    lateinit var ll_discount: LinearLayout
    lateinit var tv_apply_coupon: TextView
    lateinit var tv_discount_apply: TextView
    lateinit var tv_base_price: TextView
    lateinit var tv_due_amount: TextView
    lateinit var tv_spl_amount: TextView
    lateinit var tv_paid_amount: TextView
    lateinit var tv_discount_amount: TextView
    lateinit var tv_discount_price: TextView
    lateinit var tv_coupon_success: TextView
    lateinit var tv_packages: TextView
    lateinit var et_coupon: EditText
    private lateinit var packagesAdapter: PackagesAdapter
    private lateinit var specialFeaturesAdapter: SpecialFeaturesAdapter

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {

        if (rootview != null) {
            val parent = rootview!!.parent as ViewGroup
            parent.removeView(rootview)
        }
        try {
            rootview = inflater.inflate(R.layout.fragment_blocking_payment, container, false)
        } catch (e: InflateException) {
            e.printStackTrace()
        }

        loadingDialog()

        val bundle = arguments!!
        val customer_info =
            bundle.getSerializable("customer_info") as BookingHistoryDetailDataResponse
        booking_id_stg = customer_info.booking_id!!
        due_stg = customer_info.due_amount.toString()!!
        val date_to = customer_info.to_date
        val date_from = customer_info.booking_date
        Log.e("outputDateStr",date_from+"--"+date_to)
        initialize(rootview)


        try {


            val inputFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            val dayFormat = SimpleDateFormat("dd", Locale.getDefault())
            val inputDateStr = date_to
            val date = inputFormat.parse(inputDateStr!!)

             /*val day = dayFormat.format(date!!)
             val dayNumberSuffix = getDayOfMonthSuffix(day.toInt())
             val outputFormat =
                 SimpleDateFormat("MMMM dd'$dayNumberSuffix', yyyy", Locale.getDefault())
             val outputDateStr = outputFormat.format(date)*/

            val df = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
            val outputDateSt = df.format(date)

            val inputDateStr1 = date_from
            val date1 = inputFormat.parse(inputDateStr1!!)
            //val dft = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
            val outputDateSt1 = df.format(date1)

            Log.e("outputDateStr",""+outputDateSt)
            et_from.setText(outputDateSt)
            et_todate.setText(outputDateSt1)

            val df1 = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            postfromdate = df1.format(date1)
            posttodate = df1.format(date)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        Log.e("due_stg", due_stg)
        user_details = sessionManager.getUserDetails()
        user_name = user_details[SessionManager.KEY_NAME]!!
        staff_id = user_details[SessionManager.KEY_ID]!!
        return rootview
    }
    @SuppressLint("RestrictedApi")
    private fun getDayOfMonthSuffix(n: Int): String {
        Log.d("BookingFragment", n.toString())
        Preconditions.checkArgument(n in 1..31, "illegal day of month: $n")
        if (n in 11..13) {
            return "th"
        }
        return when (n % 10) {
            1 -> "st"
            2 -> "nd"
            3 -> "rd"
            else -> "th"
        }
    }
    private fun initialize(view: View?) {

        sessionManager =
            SessionManager(activity!!)
        cd = ConnectionDetector(
            activity!!
        )
        isInternetConnected = cd.isConnectingToInternet
        user_details = HashMap()

        iv_nav_bar = view!!.findViewById(R.id.iv_nav_bar)
        iv_back = view.findViewById(R.id.iv_back)
        tv_submit = view.findViewById(R.id.tv_submit)
        et_paid_amount = view.findViewById(R.id.et_paid_amount)
        sp_payment_type = view.findViewById(R.id.sp_payment_type)
        ll_online_payment = view.findViewById(R.id.ll_online_payment)
        et_acc_holder = view.findViewById(R.id.et_acc_holder)
        et_acc_no = view.findViewById(R.id.et_acc_no)
        et_cheque_no = view.findViewById(R.id.et_cheque_no)
        et_bank_name = view.findViewById(R.id.et_bank_name)
        et_ifsc = view.findViewById(R.id.et_ifsc)
        et_message = view.findViewById(R.id.et_message)
        tv_total_due_id = view.findViewById(R.id.tv_total_due_id)
        et_from = view.findViewById(R.id.et_from)
        et_todate = view.findViewById(R.id.et_todate)
        tv_base_price = view.findViewById(R.id.tv_base_price)
        tv_spl_amount = view.findViewById(R.id.tv_spl_amount)
        tv_due_amount = view.findViewById(R.id.tv_due_amount)
        ll_spl_amount = view.findViewById(R.id.ll_spl_amount)
        tv_paid_amount = view.findViewById(R.id.tv_paid_amount)
        tv_discount_amount = view.findViewById(R.id.tv_discount_amount)
        tv_discount_price = view.findViewById(R.id.tv_discount_price)
        tv_coupon_success = view.findViewById(R.id.tv_coupon_success)
        ll_discount = view.findViewById(R.id.ll_discount)
        et_coupon = view.findViewById(R.id.et_coupon)
        tv_apply_coupon = view.findViewById(R.id.tv_apply_coupon)
        tv_discount_apply = view.findViewById(R.id.tv_discount_apply)
        tv_total_due_id.text = " : Rs " + due_stg

        iv_nav_bar.setOnClickListener {
            NavigationDrawerActivity.openDrawer()
        }

        et_dicsount = view.findViewById(R.id.et_dicsount)
        discountradiogroup = view.findViewById(R.id.discountradiogroup)
        discountradiogroup?.setOnCheckedChangeListener { group, checkedId ->
            if (R.id.rd_dicountprice == checkedId) {
                et_dicsount.setText("0.00")
                tv_discount_price.setText("Rs 0")
                disc_type = "amount"
            }else if (R.id.rd_discountpercent == checkedId){
                et_dicsount.setText("0.00")
                tv_discount_price.setText("Rs 0")
                disc_type = "percent"
            }

        }

        featuresList = ArrayList()
        spl_features_list = ArrayList()
        packagesList = ArrayList()
        spl_packages_list = ArrayList()

        rv_selected_halls = view.findViewById(R.id.rv_selected_halls)
        val layoutManager = LinearLayoutManager(activity!!, RecyclerView.VERTICAL, false)
        rv_selected_halls.layoutManager = layoutManager
        rv_selected_halls.setHasFixedSize(true)

        rv_spl_features = view.findViewById(R.id.rv_spl_features)
        val layoutManager1 = LinearLayoutManager(activity!!, RecyclerView.VERTICAL, false)
        rv_spl_features.layoutManager = layoutManager1
        rv_spl_features.setHasFixedSize(true)
        featureListApi();
        sessionradiogroup = view.findViewById(R.id.sessionradiogroup)
        sessionradiogroup?.setOnCheckedChangeListener { group, checkedId ->
            if (R.id.rd_firsthalf == checkedId) {
                session_type = "First Half"
            }else if (R.id.rd_secondhalf == checkedId){
                session_type = "Second Half"
            }else if (R.id.rd_fullday == checkedId){
                session_type = "Full Day"
            }

            val datajobj = JSONObject()
            datajobj.put("session_type", session_type)
            datajobj.put("to_date", posttodate)
            datajobj.put("from_date",postfromdate)

            Log.e("datajobj", "" + datajobj)
            val jsonParser = JsonParser()
            val jsonObject = jsonParser.parse(datajobj.toString()) as JsonObject
            getpackagesApi(jsonObject)

        }





        paymentAdapter = object : ArrayAdapter<String>(
            activity!!,
            R.layout.spinner_item,
            resources.getStringArray(R.array.payment_types)
        ) {
            override fun isEnabled(position: Int): Boolean {
                return position != 0
            }

            override fun getDropDownView(
                position: Int,
                convertView: View?,
                parent: ViewGroup
            ): View {
                val view = super.getDropDownView(position, convertView, parent)
                val tv = view.findViewById(R.id.tv_spinner_header) as TextView
                if (position == 0) {
                    tv.setTextColor(Color.GRAY)
                }
                return view
            }
        }
        paymentAdapter.setDropDownViewResource(R.layout.spinner_item)

        iv_back.setOnClickListener {
            if (NavigationDrawerActivity.navigation_drawer.isDrawerOpen(GravityCompat.START)) {
                NavigationDrawerActivity.navigation_drawer.closeDrawer(GravityCompat.START)
            } else if (activity!!.supportFragmentManager.backStackEntryCount > 0) {
                activity!!.supportFragmentManager.popBackStack()
            }
        }

        tv_submit.setOnClickListener {
            if (isInternetConnected!!) {
                if (isValid()) {
                    Log.d("PaymentFragment", jsarray_packages_features.length().toString())
                    if (jsarray_packages_features.length() == 0) {
                            Toast.makeText(activity!!, "Must select atleast one package", Toast.LENGTH_SHORT)
                                .show()
                        } else {
                        var disctype = "";
                        var disc_per_amount = "";
                        if(disc_type == "amount"){
                            disctype = "Amount"
                            disc_per_amount = discount_price.toString();
                        }else{
                            disctype = "Percentage"
                            disc_per_amount = et_dicsount.text.toString();
                        }

                        total_price = (base_price + spl_prices).toString()
                        grand_total =
                            (((base_price - coupon_price)-discount_price) + spl_prices - paid_amount.toInt()).toString()

                        obj.put("booking_id", booking_id_stg)
                        obj.put("paid_amount", et_paid_amount.text.toString())
                        obj.put("payment_type", payment_type)
                        obj.put("message", et_message.text.toString())
                        obj.put("ifsc", "")
                        obj.put("bank_name", "")
                        obj.put("chqno", "")
                        obj.put("account_number", "")
                        obj.put("ac_holder", "")
                        obj.put("staff_id", staff_id)
                        obj.put("halls", jsarray_packages_features)
                        obj.put("features", jsarray_spl_features)
                        obj.put("discount_code", discount_code)
                        obj.put("discount_type", discount_type)
                        obj.put("discount_amount", discount_amount)
                        obj.put("discount_total_amount", discount_total_amount)
                        obj.put("staff_discount_type", disctype)
                        obj.put("staff_discount_amount", disc_per_amount)
                        obj.put("staff_discount_total", discount_price)
                        obj.put("base_price", base_price)
                        obj.put("spcl_price", spl_prices)
                        obj.put("total_price", total_price)
                        obj.put("grand_total", grand_total)
                        Log.d("PaymentFragment", obj.toString())
                        val jsonParser = JsonParser()
                        val jsonObject = jsonParser.parse(obj.toString()) as JsonObject
                        makeBlockingPaymentApi(jsonObject)
                        }

                }
            } else {
                Toast.makeText(activity!!, "No network connection", Toast.LENGTH_SHORT)
                    .show()
            }
        }

        sp_payment_type.adapter = paymentAdapter

        sp_payment_type.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parentView: AdapterView<*>, selectedItemView: View, position: Int, id: Long
            ) {
                (parentView.getChildAt(0) as TextView).setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.text_color
                    )
                )
                payment_type = parentView.getItemAtPosition(position).toString()
                sp_payment_type.setSelection(position)
                et_acc_holder.setText("")
                et_acc_no.setText("")
                et_ifsc.setText("")
                et_cheque_no.setText("")
                et_bank_name.setText("")
                Log.d("BookingFragment", "payment_type : $payment_type")
                when {
                    payment_type.equals(resources.getString(R.string.cash), true) -> {
                        ll_online_payment.visibility = View.GONE
                        et_cheque_no.visibility = View.GONE
                    }
                    payment_type.equals(resources.getString(R.string.online), true) -> {
                        ll_online_payment.visibility = View.GONE
                        et_acc_holder.visibility = View.GONE
                        et_acc_no.visibility = View.GONE
                        et_ifsc.visibility = View.GONE
                        et_cheque_no.visibility = View.GONE
                    }
                    payment_type.equals(resources.getString(R.string.cheque), true) -> {
                        ll_online_payment.visibility = View.GONE
                        et_cheque_no.visibility = View.GONE
                        et_acc_holder.visibility = View.GONE
                        et_acc_no.visibility = View.GONE
                        et_ifsc.visibility = View.GONE
                    }
                }
            }

            override fun onNothingSelected(parentView: AdapterView<*>) {
                // do nothing
            }
        }

        tv_paid_amount.text = "Rs 0"
        tv_due_amount.text = "Rs 0"
        tv_discount_amount.text = "Rs 0"
        et_paid_amount.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, end: Int) {
                tv_paid_amount.text = "Rs 0"
                tv_due_amount.text = "Rs 0"
                paid_amount = 0.toString()
            }

            override fun onTextChanged(s: CharSequence?, start: Int, count: Int, end: Int) {
                if (s!!.isEmpty()) {
                    paid_amount = 0.toString()
                    tv_paid_amount.text = "Rs 0"
                    tv_due_amount.text =
                        "Rs ${(((base_price - coupon_price)-discount_price) + spl_prices - paid_amount.toInt())}"
                } else {
                    paid_amount = s.toString()
                    tv_paid_amount.text = "(-) Rs $s"
                    tv_due_amount.text =
                        "Rs ${(((base_price - coupon_price) - discount_price) + spl_prices - paid_amount.toInt())}"
                }
            }
        })

        tv_apply_coupon.setOnClickListener {
            if (isCouponClicked) {
                et_coupon.setText("")
                isCouponClicked = false
                tv_apply_coupon.text = "Apply"
                et_coupon.isFocusable = true
                et_coupon.isFocusableInTouchMode = true
                tv_coupon_success.visibility = View.GONE
                ll_discount.visibility = View.GONE
                tv_discount_amount.text = "Rs 0"
                coupon_price = 0
                tv_due_amount.text =
                    "Rs ${(((base_price - coupon_price)-discount_price) + spl_prices - paid_amount.toInt())}"
            } else {
                if (isInternetConnected!!) {
                    if (et_coupon.text.isNotEmpty()) {
                        couponApi(et_coupon.text.toString())
                    } else {

                        et_coupon.isFocusable = true
                        et_coupon.isFocusableInTouchMode = true
                        Toast.makeText(activity!!, "Empty coupon code!", Toast.LENGTH_SHORT)
                            .show()
                    }
                } else {
                    Toast.makeText(activity!!, "No network connection", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }

        tv_discount_apply.setOnClickListener {
            if(paid_amount == ""){
                paid_amount = "0"
            }
            if(et_dicsount.text.toString() == "0.0" || et_dicsount.text.toString() == "0.00" || et_dicsount.text.toString() == "0" || et_dicsount.text.toString() == ""){
                Toast.makeText(activity!!, "Enter Valid Amount or Percentage", Toast.LENGTH_SHORT)
                    .show()
            }else {
                if (isDiscountClicked) {
                    isDiscountClicked = false;
                    tv_discount_apply.text = "Apply"
                    et_dicsount.setText("0.0")
                    discount_price = 0.00
                    tv_discount_price.text = "(-) Rs " + discount_price.toString()
                    tv_due_amount.text =
                        "Rs ${(((base_price - coupon_price) - discount_price) + spl_prices - paid_amount.toInt())}"

                } else {
                    isDiscountClicked = true;
                    tv_discount_apply.text = "Remove"
                    if (disc_type == "amount") {
                        discount_price = et_dicsount.text.toString().toDouble()
                    } else {
                        val total_amount: Int = base_price + spl_prices
                        val dsc_en_price: Double = et_dicsount.text.toString().toDouble() / 100
                        val cal_price: Double = total_amount.toDouble() * dsc_en_price
                        discount_price = cal_price
                    }

                    Log.e(
                        "obj",
                        "" + discount_price.toString() + "--" + coupon_price + "--" + spl_prices + "--" + paid_amount
                    )
                    tv_discount_price.text = "(-) Rs " + discount_price.toString()
                    tv_due_amount.text =
                        "Rs ${(((base_price - coupon_price) - discount_price) + spl_prices - paid_amount.toInt())}"
                }
            }


        }


    }

    private fun isValid(): Boolean {

        if (et_paid_amount.text.toString().isEmpty()) {
            Toast.makeText(activity!!, "Paid Amount should not be empty", Toast.LENGTH_SHORT)
                .show()
        } else if (et_message.text.toString().isEmpty()) {
            Toast.makeText(activity!!, "Message should not be empty", Toast.LENGTH_SHORT)
                .show()
        } else if (payment_type.equals("Payment Type")) {
            Toast.makeText(activity!!, "Payment Type should not be empty", Toast.LENGTH_SHORT)
                .show()
        }/*else if (payment_type.equals(resources.getString(R.string.online), true)) {
            if (et_acc_holder.text.toString().isEmpty()) {
                Toast.makeText(activity!!, "Field should not be empty", Toast.LENGTH_SHORT)
                    .show()
            } else if (et_acc_holder.text.toString().isEmpty()) {
                Toast.makeText(activity!!, "Field should not be empty", Toast.LENGTH_SHORT)
                    .show()
            } else if (et_ifsc.text.toString().isEmpty()) {
                Toast.makeText(activity!!, "Field should not be empty", Toast.LENGTH_SHORT)
                    .show()
            } else if (et_bank_name.text.toString().isEmpty()) {
                Toast.makeText(activity!!, "Field should not be empty", Toast.LENGTH_SHORT)
                    .show()
            } else {
                return true
            }
        } else if (payment_type.equals(resources.getString(R.string.cheque), true)) {
            if (et_cheque_no.text.toString().isEmpty()) {
                Toast.makeText(activity!!, "Field should not be empty", Toast.LENGTH_SHORT)
                    .show()
            } else if (et_bank_name.text.toString().isEmpty()) {
                Toast.makeText(activity!!, "Field should not be empty", Toast.LENGTH_SHORT)
                    .show()
            } else {
                return true
            }
        }*/ else {
            return true
        }
        return false
    }
    private fun getpackagesApi(final_object: JsonObject) {

        loading_dialog.show()

        val apiInterface = ApiInterface.create()
        val call = apiInterface.getPackagesApi(final_object)
        call.enqueue(object : Callback<PackagesResponse> {
            override fun onFailure(call: Call<PackagesResponse>, t: Throwable) {
                try{
                    Log.d("Fragment", "PackagesResponse error $t")
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(activity!!, "Please try again!", Toast.LENGTH_SHORT)
                        .show()
                }catch (e:Exception){
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<PackagesResponse>,
                response: Response<PackagesResponse>
            ) {

                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Log.d("Fragment", "PackagesResponse success")
                    rv_selected_halls.visibility = View.VISIBLE
                    val packagesResponse = response.body()
                    if (packagesResponse!!.status == "1") {
                        packagesList = ArrayList()
                        spl_packages_list.clear()
                        jsarray_packages_features = JSONArray()
                        base_price = 0
                        tv_base_price.text = "Rs ${base_price}"
                        pkg_prices = 0
                        for(key in hashMap.keys){
                            println("Element at key $key = ${hashMap[key]}")
                            pkg_prices += hashMap[key]!!.toInt()
                        }
                        if (paid_amount.isEmpty()) {
                            tv_due_amount.text = "Rs ${(base_price + pkg_prices - 0)}"
                        } else {
                            tv_due_amount.text =
                                "Rs ${(((base_price - coupon_price) - discount_price) + pkg_prices - paid_amount.toInt())}"
                        }
                        val data = packagesResponse.packagesList
                        if (data!!.size > 0) {
                            for (i in 0 until data.size) {
                                packagesList.add(data[i])
                            }
                        }
                        packagesAdapter = PackagesAdapter(activity!!, packagesList)
                        rv_selected_halls.adapter = packagesAdapter
                        packagesAdapter.notifyDataSetChanged()
                    } else {
                        Toast.makeText(activity!!, packagesResponse.result, Toast.LENGTH_SHORT)
                            .show()
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }
    private fun featureListApi() {

        loading_dialog.show()

        val apiInterface = ApiInterface.create()
        val call = apiInterface.featureListApi()
        call.enqueue(object : Callback<FeaturesResponse> {
            override fun onFailure(call: Call<FeaturesResponse>, t: Throwable) {
                try{
                    Log.d("Fragment", "FeaturesResponse error $t")
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(activity!!, "Please try again!", Toast.LENGTH_SHORT)
                        .show()
                }catch (e:Exception){
                    e.printStackTrace()
                }

            }

            override fun onResponse(
                call: Call<FeaturesResponse>,
                response: Response<FeaturesResponse>
            ) {

                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Log.d("Fragment", "FeaturesResponse success")

                    val featuresResponse = response.body()
                    if (featuresResponse!!.status == "1") {
                        featuresList = ArrayList()
                        spl_features_list.clear()
                        hashMap.clear()
                        jsarray_spl_features = JSONArray()
                        base_price = 0
                        spl_prices = 0
                        tv_due_amount.text = "Rs ${(base_price + spl_prices - 0)}"
                        tv_spl_amount.text = "Rs ${spl_prices}"
                        val data = featuresResponse.details
                        if (data!!.size > 0) {
                            for (i in 0 until data.size) {
                                featuresList.add(data[i])
                            }
                        }
                        specialFeaturesAdapter = SpecialFeaturesAdapter(activity!!, featuresList)
                        rv_spl_features.adapter = specialFeaturesAdapter
                        specialFeaturesAdapter.notifyDataSetChanged()
                    } else {
                        Toast.makeText(activity!!, featuresResponse.result, Toast.LENGTH_SHORT)
                            .show()
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }
    inner class PackagesAdapter(context: Context, list: ArrayList<PackagesDataResponse>) :
        RecyclerView.Adapter<PackagesAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var mList: ArrayList<PackagesDataResponse>? = null

        init {
            this.mContext = context
            mList = list
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {

            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_package, parent, false)
            return ViewHolder(view)

        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {

            //holder.tv_amount.text = mList!![position].amount
            holder.checkBox.text = mList!![position].name
            holder.tv_availablehallspackage.text = mList!![position].hall_names
            //Log.e(" mList!![position].name", ""+position+"=="+mList!![position].available!!.size)
            if(mList!![position].available!!.size == 0){
                holder.tv_availabledate.text = "Not Available"
                holder.checkBox.isEnabled = false
            }else{
                val vaialabdates = mList!![position].available
                val stringsOrNulls = arrayOfNulls<String>(vaialabdates!!.size)
                val senddatestoserver = arrayOfNulls<String>(vaialabdates!!.size)
                val sum_samount = arrayOfNulls<Int>(vaialabdates!!.size)
                for (i in 0 until vaialabdates!!.size) {
                    //  Log.d("loopdate", vaialabdates[i])
                    val inputFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                    val dateee = inputFormat.parse(vaialabdates.get(i).adate)
                    val df1 = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                    val outputDateSend = df1.format(dateee)
                    senddatestoserver[i] = outputDateSend
                    val df = SimpleDateFormat("dd-MM", Locale.getDefault())
                    val outputDateSt = df.format(dateee)
                    stringsOrNulls[i] = outputDateSt
                    sum_samount[i] = vaialabdates.get(i).samount!!.toInt()
                }

                val commaSeperatedString = stringsOrNulls.joinToString { it -> "${it}" }
                val commaSeperatedString1 = senddatestoserver.joinToString { it -> "${it}" }
                holder.tv_availabledate.text = commaSeperatedString
                holder.tv_ssenddate.text = commaSeperatedString1
                val commaSeperatedString11 = sum_samount.sumBy{ it!!.toInt() }
                Log.e("SUM",""+commaSeperatedString11)
                holder.tv_amount.text = commaSeperatedString11.toString()
            }
            holder.checkBox.setOnCheckedChangeListener { compoundButton, isChecked ->
                if (isChecked) {
                    val avail_date_amount =  mList!![position].available
                    val avail_date_array = JSONArray()
                    for (i in 0 until avail_date_amount!!.size) {
                        val avail_date_amount_obj = JSONObject()
                        avail_date_amount_obj.put("adate", avail_date_amount.get(i).adate)
                        avail_date_amount_obj.put("samount", avail_date_amount.get(i).samount)
                        avail_date_array.put(avail_date_amount_obj)
                    }
                    val obj1 = JSONObject()
                    obj1.put("hall_id", mList!![position].id.toString())
                    obj1.put("hall_amount", mList!![position].amount.toString())
                    obj1.put("available",  avail_date_array)
                    obj1.put("hall_names", holder.tv_availablehallspackage.text.toString())
                    // val smount = holder.tv_amount.text.toString().toInt()
                    base_price += holder.tv_amount.text.toString().toInt()
                    tv_base_price.text = "Rs $base_price"
                    jsarray_packages_features.put(obj1)
                    spl_packages_list.add(mList!![position])

                } else {
                    if (jsarray_packages_features.length() > 0) {
                        base_price = base_price.toInt() - holder.tv_amount.text.toString().toInt()
                        tv_base_price.text = "Rs $base_price"
                        for (i in 0 until jsarray_packages_features.length()) {
                            val obj = jsarray_packages_features.getJSONObject(i)
                            if (obj.get("hall_id") == mList!![position].id.toString()) {
                                jsarray_packages_features.remove(i)
                                break
                            }
                            Log.d("jsarray_packages_features", obj.toString())
                        }
                    }else {
                        base_price = base_price.toInt() - holder.tv_amount.text.toString().toInt()
                        spl_packages_list.clear()
                        ll_spl_amount.visibility = View.VISIBLE
                        pkg_prices = 0
                        base_price = 0
                        for(key in hashMap.keys){
                            println("Element at key $key = ${hashMap[key]}")
                            pkg_prices += hashMap[key]!!.toInt()
                        }
                        tv_spl_amount.text = "Rs $pkg_prices"
                        if (paid_amount.isEmpty()) {
                            tv_due_amount.text = "Rs ${(base_price + pkg_prices - 0)}"
                        } else {
                            tv_due_amount.text =
                                "Rs ${(((base_price - coupon_price) - discount_price) + pkg_prices - paid_amount.toInt())}"
                        }
                    }
                    if (spl_packages_list.contains(mList!![position])) {
                        spl_packages_list.remove(mList!![position])
                    }
                }
                Log.d(
                    "adapter",
                    "spl_packages_list===== ${spl_packages_list.size} ===== $jsarray_packages_features"
                )
                if (spl_packages_list.size > 0) {
                    ll_spl_amount.visibility = View.VISIBLE
                    pkg_prices = 0
                    for(key in hashMap.keys){
                        println("Element at key $key = ${hashMap[key]}")
                        pkg_prices += hashMap[key]!!.toInt()
                    }
                    if (paid_amount.isEmpty()) {
                        tv_due_amount.text = "Rs ${(base_price + pkg_prices - 0)}"
                    } else {
                        tv_due_amount.text =
                            "Rs ${(((base_price - coupon_price)-discount_price)+ pkg_prices - paid_amount.toInt())}"
                    }
                } else {
                    ll_spl_amount.visibility = View.VISIBLE
                    pkg_prices = 0
                    for(key in hashMap.keys){
                        println("Element at key $key = ${hashMap[key]}")
                        pkg_prices += hashMap[key]!!.toInt()
                    }
                    tv_spl_amount.text = "Rs $pkg_prices"
                    if (paid_amount.isEmpty()) {
                        tv_due_amount.text = "Rs ${(base_price + pkg_prices - 0)}"
                    } else {
                        tv_due_amount.text =
                            "Rs ${(((base_price - coupon_price) -discount_price) + pkg_prices - paid_amount.toInt())}"
                    }
                }
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var checkBox: CheckBox = view.findViewById(R.id.checkBox)
            var tv_amount: TextView = view.findViewById(R.id.tv_amount)
            var tv_availabledate: TextView = view.findViewById(R.id.tv_availabledate)
            var tv_availablehallspackage: TextView = view.findViewById(R.id.tv_availablehallspackage)
            var tv_ssenddate: TextView = view.findViewById(R.id.tv_ssenddate)


        }

    }
    inner class SpecialFeaturesAdapter(context: Context, list: ArrayList<FeaturesDataResponse>) :
        RecyclerView.Adapter<SpecialFeaturesAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var mList: ArrayList<FeaturesDataResponse>? = null


        init {
            this.mContext = context
            mList = list
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {

            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_feature, parent, false)
            return ViewHolder(view)

        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {

            holder.tv_amount.text = mList!![position].amount
            holder.checkBox.text = mList!![position].name
            holder.checkBox.setOnCheckedChangeListener { compoundButton, isChecked ->
                if (isChecked) {
                    val obj1 = JSONObject()
                    obj1.put("feature_id", mList!![position].id.toString())
                    obj1.put("feature_amount", mList!![position].amount.toString())
                    obj1.put("no_of_persons", holder.integer_number.text.toString())

                    jsarray_spl_features.put(obj1)
                    hashMap.put(position,mList!![position].amount!!)
                    spl_features_list.add(mList!![position])
                    holder.increase.isEnabled = true
                    holder.decrease.isEnabled = true
                } else {
                    holder.increase.isEnabled = false
                    holder.decrease.isEnabled = false
                    hashMap.remove(position)
                    if (jsarray_spl_features.length() > 0) {
                        for (i in 0 until jsarray_spl_features.length()) {
                            val obj = jsarray_spl_features.getJSONObject(i)
                            if (obj.get("feature_id") == mList!![position].id.toString()) {
                                jsarray_spl_features.remove(i)
                                break
                            }
                            Log.d("jsarray_spl_features", obj.toString())
                        }
                    }
                    if (spl_features_list.contains(mList!![position])) {
                        spl_features_list.remove(mList!![position])
                    }
                }
                Log.d(
                    "adapter",
                    "spl_features_list===== ${spl_features_list.size} ===== $jsarray_spl_features"
                )
                if (spl_features_list.size > 0) {
                    ll_spl_amount.visibility = View.VISIBLE
                    /*  spl_prices = 0
                      for (i in 0 until spl_features_list.size) {
                          spl_prices += spl_features_list[i].amount!!.toInt()
                      }*/
                    spl_prices = 0
                    for(key in hashMap.keys){
                        println("Element at key $key = ${hashMap[key]}")
                        spl_prices += hashMap[key]!!.toInt()
                    }

                    Log.e("defaultspl_prices",""+spl_prices)

                    tv_spl_amount.text = "Rs $spl_prices"
                    //if(bookingtype.equals("blocking")){
                        tv_due_amount.text = "Rs ${(base_price + spl_prices - 0)}"
                   /* }else {
                        if (paid_amount.isEmpty()) {
                            tv_due_amount.text = "Rs ${(base_price + spl_prices - 0)}"
                        } else {
                            tv_due_amount.text =
                                "Rs ${(((base_price - coupon_price) - discount_price) + spl_prices - paid_amount.toInt())}"
                        }
                    }*/
                } else {
                    spl_features_list.clear()
                    ll_spl_amount.visibility = View.GONE
                    spl_prices = 0
                    tv_spl_amount.text = "Rs $spl_prices"
                    //if(bookingtype.equals("blocking")){
                        tv_due_amount.text = "Rs ${(base_price + spl_prices - 0)}"
                   /* }else {
                        if (paid_amount.isEmpty()) {
                            tv_due_amount.text = "Rs ${(base_price + spl_prices - 0)}"
                        } else {
                            tv_due_amount.text =
                                "Rs ${(((base_price - coupon_price) - discount_price) + spl_prices - paid_amount.toInt())}"
                        }
                    }*/
                }
            }

            holder.increase.setOnClickListener {
                var add = holder.integer_number.text.toString().toInt() + 1
                holder.integer_number.setText("$add")
                holder.tv_amount.setText(""+add.toInt() * mList!![position].amount!!.toInt())

                Log.d("Adding"+position,""+add.toInt() * mList!![position].amount!!.toInt())
                hashMap.put(position,""+add.toInt() * mList!![position].amount!!.toInt())
                spl_prices = 0
                for(key in hashMap.keys){
                    println("Element at key $key = ${hashMap[key]}")
                    spl_prices += hashMap[key]!!.toInt()
                }
                Log.e("defaultspl_prices",""+spl_prices)

                tv_spl_amount.text = "Rs $spl_prices"
                if (paid_amount.isEmpty()) {
                    tv_due_amount.text = "Rs ${(base_price + spl_prices - 0)}"
                } else {
                    tv_due_amount.text =
                        "Rs ${(((base_price - coupon_price) - discount_price) + spl_prices - paid_amount.toInt())}"
                }
                if (jsarray_spl_features.length() > 0) {
                    for (i in 0 until jsarray_spl_features.length()) {
                        val obj = jsarray_spl_features.getJSONObject(i)
                        if (obj.get("feature_id") == mList!![position].id.toString()) {
                            jsarray_spl_features.remove(i)
                            break
                        }
                        Log.d("jsarray_spl_features", obj.toString())
                    }
                }
                val obj1 = JSONObject()
                obj1.put("feature_id", mList!![position].id.toString())
                obj1.put("feature_amount", mList!![position].amount.toString())
                obj1.put("no_of_persons", holder.integer_number.text.toString())
                jsarray_spl_features.put(obj1)
            }
            holder.decrease.setOnClickListener {
                var number = holder.integer_number.text.toString().toInt() - 1
                if(number<1){
                    number = 1
                }
                holder.integer_number.setText("$number")
                holder.tv_amount.setText(""+number.toInt() * mList!![position].amount!!.toInt())
                Log.d("minus"+position,""+number.toInt() * mList!![position].amount!!.toInt())
                hashMap.put(position,""+number.toInt() * mList!![position].amount!!.toInt())
                spl_prices = 0
                for(key in hashMap.keys){
                    println("Element at key $key = ${hashMap[key]}")
                    spl_prices += hashMap[key]!!.toInt()
                }
                Log.e("defaultspl_prices",""+spl_prices)

                tv_spl_amount.text = "Rs $spl_prices"
                if (paid_amount.isEmpty()) {
                    tv_due_amount.text = "Rs ${(base_price + spl_prices - 0)}"
                } else {
                    tv_due_amount.text =
                        "Rs ${(((base_price - coupon_price) - discount_price) + spl_prices - paid_amount.toInt())}"
                }
                if (jsarray_spl_features.length() > 0) {
                    for (i in 0 until jsarray_spl_features.length()) {
                        val obj = jsarray_spl_features.getJSONObject(i)
                        if (obj.get("feature_id") == mList!![position].id.toString()) {
                            jsarray_spl_features.remove(i)
                            break
                        }
                        Log.d("jsarray_spl_features", obj.toString())
                    }
                }
                val obj1 = JSONObject()
                obj1.put("feature_id", mList!![position].id.toString())
                obj1.put("feature_amount", mList!![position].amount.toString())
                obj1.put("no_of_persons", holder.integer_number.text.toString())
                jsarray_spl_features.put(obj1)
            }

        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var checkBox: CheckBox = view.findViewById(R.id.checkBox)
            var tv_amount: TextView = view.findViewById(R.id.tv_amount)
            var decrease: TextView = view.findViewById(R.id.decrease)
            var increase: TextView = view.findViewById(R.id.increase)
            var integer_number: TextView = view.findViewById(R.id.integer_number)
        }

    }
    private fun couponApi(coupon_code: String) {

        loading_dialog.show()
        coupon_price = 0

        val apiInterface = ApiInterface.create()
        val call = apiInterface.checkCouponApi(coupon_code)

        call.enqueue(object : Callback<CouponCodeResponse> {
            override fun onFailure(call: Call<CouponCodeResponse>, t: Throwable) {
                try{
                    Log.d("BookingFragment", "checkCouponApi error $t")
                    isCouponClicked = false
                    tv_apply_coupon.text = "Apply"
                    et_coupon.isFocusable = true
                    et_coupon.isFocusableInTouchMode = true
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(activity!!, "Please try again!", Toast.LENGTH_SHORT)
                        .show()
                }catch (e:Exception){
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<CouponCodeResponse>,
                response: Response<CouponCodeResponse>
            ) {

                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Log.d("BookingFragment", "checkCouponApi result ${response.body()!!.result}")
                    val couponCodeResponse = response.body()
                    if (couponCodeResponse!!.status == "1") {
                        isCouponClicked = true
                        tv_apply_coupon.text = "Remove"
                        et_coupon.isFocusable = false
                        et_coupon.isFocusableInTouchMode = false
                        val coupondata = couponCodeResponse.coupondata
                        discount_type = coupondata!!.amount_type!!
                        discount_code = coupondata.discount_code!!
                        if (discount_type == "amount") {
                            discount_amount = coupondata.amount!!
                            coupon_price = discount_amount.toInt()
                            discount_total_amount = discount_amount
                            if (paid_amount == "") {
                                paid_amount = 0.toString()
                            }
                            if (discount_amount == "") {
                                ll_discount.visibility = View.GONE
                                tv_coupon_success.visibility = View.GONE
                                tv_discount_amount.text = "Rs 0"
                                tv_due_amount.text =
                                    "Rs ${(((base_price - coupon_price) - discount_price)+ spl_prices - paid_amount.toInt())}"
                            } else {
                                ll_discount.visibility = View.VISIBLE
                                tv_coupon_success.visibility = View.VISIBLE
                                tv_discount_amount.text = "(-) Rs $coupon_price"
                                tv_due_amount.text =
                                    "Rs ${(((base_price - coupon_price) - discount_price)+ spl_prices - paid_amount.toInt())}"
                            }
                        } else if (discount_type == "percentage") {
                            discount_amount = coupondata.amount!!
                            val amt = ((base_price *discount_amount.toInt())/100)
                            val percent = base_price - amt
                            coupon_price = amt
                            Log.d("price","base_price ==== $base_price ======= $amt")
                            discount_total_amount = amt.toString()
                            if (paid_amount == "") {
                                paid_amount = 0.toString()
                            }
                            if (discount_amount == "") {
                                ll_discount.visibility = View.GONE
                                tv_coupon_success.visibility = View.GONE
                                tv_discount_amount.text = "Rs 0"
                                tv_due_amount.text =
                                    "Rs ${(((base_price - coupon_price) - discount_price) + spl_prices - paid_amount.toInt())}"
                            } else {
                                ll_discount.visibility = View.VISIBLE
                                tv_coupon_success.visibility = View.VISIBLE
                                tv_discount_amount.text = "(-) Rs $coupon_price"
                                tv_due_amount.text =
                                    "Rs ${(((base_price - coupon_price) - discount_price) + spl_prices - paid_amount.toInt())}"
                            }
                        }

                    } else if (couponCodeResponse.status == "2") {
                        isCouponClicked = false
                        tv_apply_coupon.text = "Apply"
                        et_coupon.isFocusable = true
                        et_coupon.isFocusableInTouchMode = true
                        Toast.makeText(activity!!, couponCodeResponse.result, Toast.LENGTH_SHORT)
                            .show()
                        ll_discount.visibility = View.GONE
                        tv_coupon_success.visibility = View.GONE
                        tv_discount_amount.text = "Rs 0"
                        tv_due_amount.text =
                            "Rs ${(((base_price - coupon_price) - discount_price) + spl_prices - paid_amount.toInt())}"

                    } else {
                        isCouponClicked = false
                        tv_apply_coupon.text = "Apply"
                        et_coupon.isFocusable = true
                        et_coupon.isFocusableInTouchMode = true
                        Toast.makeText(
                            activity!!,
                            "Something went wrong, Please try again!",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }
    private fun makeBlockingPaymentApi(final_object: JsonObject) {

        loading_dialog.show()

        val apiInterface = ApiInterface.create()
        val call = apiInterface.makeBlockingPaymentApi(final_object)

        call.enqueue(object : Callback<LoginResponse> {
            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                try{
                    Log.d("EditBookingFragment", "makePaymentApi error $t")

                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(activity!!, "Please try again!", Toast.LENGTH_SHORT)
                        .show()
                }catch (e:Exception){
                    e.printStackTrace()
                }

            }

            override fun onResponse(
                call: Call<LoginResponse>,
                response: Response<LoginResponse>
            ) {

                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Log.d(
                        "EditBookingFragment",
                        "makePaymentApi success ${response.body()!!.result}"
                    )
                    val responsee = response.body()
                    if (responsee!!.status == "1") {
                        // go to previous fragment
                        Toast.makeText(activity!!, "Payment successful!", Toast.LENGTH_SHORT)
                            .show()
                        if (activity!!.supportFragmentManager.backStackEntryCount > 0) {
                            activity!!.supportFragmentManager.popBackStack()
                        }
                    } else {
                        Toast.makeText(activity!!, responsee.result, Toast.LENGTH_SHORT)
                            .show()
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(activity!!)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view = LayoutInflater.from(activity!!).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

}
package com.graceconventioncenter.calendar.Fragments

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.util.SparseBooleanArray
import android.view.*
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.graceconventioncenter.calendar.Activity.ConnectionDetector
import com.graceconventioncenter.calendar.Activity.NavigationDrawerActivity
import com.graceconventioncenter.calendar.Adapters.SelectedHallsAdapter
import com.graceconventioncenter.calendar.ApiInterface.*
import com.graceconventioncenter.calendar.Helper.SessionManager
import com.graceconventioncenter.calendar.R
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class EditBookingFragment : Fragment() {

    private var rootview: View? = null
    lateinit var sessionManager: SessionManager
    lateinit var cd: ConnectionDetector
    private var isInternetConnected: Boolean? = false
    private lateinit var user_details: HashMap<String, String>
    private var user_name = ""
    private lateinit var radioGroup: RadioGroup
    private lateinit var sessionradiogroup: RadioGroup
    private lateinit var layout_paidamount: LinearLayout
    private lateinit var layout_splfeatures: LinearLayout
    private lateinit var layout_coupon: LinearLayout
    private lateinit var layout_customerform: LinearLayout
    private lateinit var layout_payment: LinearLayout
    private lateinit var layout_sppaymenttype: RelativeLayout
    internal var mChecked = SparseBooleanArray()
    internal var pChecked = SparseBooleanArray()

    private lateinit var iv_back: ImageView
    private lateinit var iv_nav_bar: ImageView
    private lateinit var tv_submit: TextView
    private lateinit var tv_date: TextView
    private lateinit var et_from: EditText
    private lateinit var et_todate: EditText
    private lateinit var et_first_name: EditText
    private lateinit var et_last_name: EditText
    private lateinit var et_mobile_no: EditText
    private lateinit var et_alternate_no: EditText
    private lateinit var et_email: EditText
    private lateinit var et_address_id : EditText
    private lateinit var et_city_id : EditText
    private lateinit var et_state_id : EditText
    private lateinit var et_pincode_id : EditText
    private lateinit var et_message_id : EditText
    private lateinit var et_type_of_event_id: EditText
    private lateinit var et_paid_amount: EditText
    private lateinit var et_message: EditText
    private lateinit var iv_drop_down: ImageView
    private lateinit var sp_payment_type: Spinner
    private lateinit var paymentAdapter: ArrayAdapter<String>

    private lateinit var rv_selected_halls: RecyclerView
    private lateinit var rv_spl_features: RecyclerView
    private lateinit var rv_spl_packages: RecyclerView
    private lateinit var selectedHallsAdapter: SelectedHallsAdapter
    private lateinit var specialFeaturesAdapter: SpecialFeaturesAdapter
    private lateinit var packagesAdapter: PackagesAdapter
    var payment_type = ""
    var date_server_stg = ""
    lateinit var loading_dialog: Dialog
    lateinit var ll_online_payment: LinearLayout
    var obj = JSONObject()
    lateinit var exist_features:ArrayList<BookingHistoryFeatureDataResponse>
    lateinit var exist_packages:ArrayList<BookingHistoryHallsDataResponse>
    lateinit var jsarray_spl_features: JSONArray
    lateinit var jsarray_packages_features: JSONArray
    //lateinit var spl_features_list: ArrayList<FeaturesDataResponse>
    lateinit var spl_packages_list: ArrayList<PackagesDataResponse>
    val hashMap:HashMap<Int,String> = HashMap<Int,String>() //define empty hashmap
    lateinit var tv_apply_coupon: TextView
    lateinit var tv_base_price: TextView
    lateinit var tv_paid_amount: TextView
    lateinit var tv_due_amount: TextView
    lateinit var ll_spl_amount: LinearLayout
    lateinit var ll_discount: LinearLayout
    lateinit var tv_spl_amount: TextView
    lateinit var tv_discount_amount: TextView
    lateinit var tv_coupon_success: TextView
    lateinit var tv_packages: TextView
    lateinit var et_coupon: EditText
    private lateinit var et_acc_holder: EditText
    private lateinit var et_acc_no: EditText
    private lateinit var et_cheque_no: EditText
    private lateinit var et_bank_name: EditText
    private lateinit var et_ifsc: EditText
    lateinit var featuresList: ArrayList<FeaturesDataResponse>
    lateinit var packagesList: ArrayList<PackagesDataResponse>
    var paid_amount = ""
    var base_price = 0
    var spl_prices = 0
    var pkg_prices = 0
    var coupon_price = 0
    var isCouponClicked = false
    var discount_code = ""
    var discount_type = ""
    var discount_amount = ""
    var discount_total_amount = ""
    var grand_total = ""
    var total_price = ""
    var staff_id = ""
    var bookingtype = ""
    var session_type = ""
    var default_session_type = ""
    var monthConverted = ""
    var postfromdate = ""
    var default_postfromdate = ""
    var posttodate = ""
    var default_posttodate = ""
    var booking_id = ""
    var fname = ""
    var lname = ""
    var emobile = ""
    var alt_mobile = ""
    var eemail = ""
    var eaddress = ""
    var estate = ""
    var ecity = ""
    var epincode = ""
    var etype_event = ""
    var str_paidamount = ""
    var str_dueamount = ""
    var str_couponprice = ""


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {

        if (rootview != null) {
            val parent = rootview!!.parent as ViewGroup
            parent.removeView(rootview)
        }
        try {
            rootview = inflater.inflate(R.layout.fragment_edit_booking, container, false)
        } catch (e: InflateException) {
            e.printStackTrace()
        }
        initialize(rootview)
        user_details = sessionManager.getUserDetails()
        user_name = user_details[SessionManager.KEY_NAME]!!
        staff_id = user_details[SessionManager.KEY_ID]!!
        loadingDialog()
        val bundle = arguments!!
        val customer_info =
            bundle.getSerializable("customer_info") as BookingHistoryDetailDataResponse
        booking_id = customer_info.booking_id!!
        postfromdate = customer_info.booking_date.toString()!!
        default_postfromdate = customer_info.booking_date.toString()!!
        posttodate = customer_info.to_date.toString()!!
        default_posttodate = customer_info.to_date.toString()!!
        session_type = customer_info.session.toString()!!
        default_session_type = customer_info.session.toString()!!
        exist_features = customer_info.feature_detals!!
        exist_packages = customer_info.halls_details!!

        fname = customer_info.first_name.toString()
        lname = customer_info.last_name.toString()
        emobile = customer_info.mobile.toString()
        alt_mobile = customer_info.alt_mobile.toString()
        eemail = customer_info.email.toString()
        eaddress = customer_info.address.toString()
        ecity = customer_info.city.toString()
        estate = customer_info.state.toString()
        epincode = customer_info.pincode.toString()
        etype_event = customer_info.type_of_event.toString()
        str_paidamount = customer_info.paid_amount.toString()
        str_dueamount = customer_info.due_amount.toString()
        str_couponprice = customer_info.discount_total_amount.toString()
        coupon_price = String.format("%.0f", str_couponprice.toDouble()).toInt()
       // paid_amount = str_paidamount
        tv_paid_amount.setText("Rs "+String.format("%.0f", str_paidamount.toDouble()))
        paid_amount = String.format("%.0f", str_paidamount.toDouble())
        tv_discount_amount.setText("Rs "+String.format("%.0f", str_couponprice.toDouble()).toString())
        val inputFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val fdate = inputFormat.parse(postfromdate)
        val tdate = inputFormat.parse(posttodate)
        val df = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
        val from_date = df.format(fdate)
        val to_date = df.format(tdate)
        //Log.e("date",from_date+"==="+date)
        et_from.setText(from_date)
        et_todate.setText(to_date)

        if (session_type.equals("First Half")) {
            sessionradiogroup.check(R.id.rd_firsthalf)
        }else if (session_type.equals("Second Half")){
            sessionradiogroup.check(R.id.rd_secondhalf)
        }else if (session_type.equals("Full Day")){
            sessionradiogroup.check(R.id.rd_fullday)
        }
        if (isInternetConnected!!) {
            featureListApi()
        } else {
            if (loading_dialog.isShowing)
                loading_dialog.dismiss()
            Toast.makeText(activity!!, "No network connection", Toast.LENGTH_SHORT).show()
        }

        base_price = customer_info.base_price!!.toInt()
        tv_base_price.text = "Rs $base_price"
        spl_prices = customer_info.spcl_price!!.toInt()
        tv_spl_amount.text = "Rs $spl_prices"
        et_first_name.setText(fname)
        et_last_name.setText(lname)
        et_mobile_no.setText(emobile)
        et_alternate_no.setText(alt_mobile)
        et_email.setText(eemail)
        et_address_id.setText(eaddress)
        et_city_id.setText(ecity)
        et_state_id.setText(estate)
        et_pincode_id.setText(epincode)
        et_type_of_event_id.setText(etype_event)

        return rootview
    }

    private fun initialize(view: View?) {

        sessionManager =
            SessionManager(activity!!)
        cd = ConnectionDetector(
            activity!!
        )
        isInternetConnected = cd.isConnectingToInternet
        user_details = HashMap()

        iv_nav_bar = view!!.findViewById(R.id.iv_nav_bar)
        iv_back = view.findViewById(R.id.iv_back)
        tv_submit = view.findViewById(R.id.tv_submit)
        tv_date = view.findViewById(R.id.tv_date)
        et_from = view.findViewById(R.id.et_from)
        et_todate = view.findViewById(R.id.et_todate)
        et_first_name = view.findViewById(R.id.et_first_name)
        et_first_name.setText(fname)
        et_last_name = view.findViewById(R.id.et_last_name)
        et_last_name.setText(lname)
        et_mobile_no = view.findViewById(R.id.et_mobile_no)
        et_mobile_no.setText(emobile)
        et_alternate_no = view.findViewById(R.id.et_alternate_no)
        et_alternate_no.setText(alt_mobile)
        et_email = view.findViewById(R.id.et_email)
        et_email.setText(eemail)
        et_address_id = view.findViewById(R.id.et_address_id)
        et_address_id.setText(eaddress)
        et_city_id = view.findViewById(R.id.et_city_id)
        et_city_id.setText(ecity)
        et_state_id = view.findViewById(R.id.et_state_id)
        et_state_id.setText(estate)
        et_pincode_id = view.findViewById(R.id.et_pincode_id)
        et_pincode_id.setText(epincode)
        et_message_id = view.findViewById(R.id.et_message_id)
        et_type_of_event_id = view.findViewById(R.id.et_type_of_event_id)
        et_type_of_event_id.setText(etype_event)


        et_paid_amount = view.findViewById(R.id.et_paid_amount)
        et_message = view.findViewById(R.id.et_message)
        sp_payment_type = view.findViewById(R.id.sp_payment_type)
        iv_drop_down = view.findViewById(R.id.iv_drop_down)
        ll_online_payment = view.findViewById(R.id.ll_online_payment)
        tv_apply_coupon = view.findViewById(R.id.tv_apply_coupon)
        tv_base_price = view.findViewById(R.id.tv_base_price)
        tv_paid_amount = view.findViewById(R.id.tv_paid_amount)
        tv_due_amount = view.findViewById(R.id.tv_due_amount)
        tv_spl_amount = view.findViewById(R.id.tv_spl_amount)
        ll_spl_amount = view.findViewById(R.id.ll_spl_amount)
        tv_discount_amount = view.findViewById(R.id.tv_discount_amount)
        tv_coupon_success = view.findViewById(R.id.tv_coupon_success)
        ll_discount = view.findViewById(R.id.ll_discount)
        et_coupon = view.findViewById(R.id.et_coupon)
        et_acc_holder = view.findViewById(R.id.et_acc_holder)
        et_acc_no = view.findViewById(R.id.et_acc_no)
        et_cheque_no = view.findViewById(R.id.et_cheque_no)
        et_bank_name = view.findViewById(R.id.et_bank_name)
        et_ifsc = view.findViewById(R.id.et_ifsc)

        layout_paidamount = view.findViewById(R.id.layout_paidamount)
        layout_splfeatures = view.findViewById(R.id.layout_splfeatures)
        layout_coupon = view.findViewById(R.id.layout_coupon)
        layout_payment = view.findViewById(R.id.layout_payment)
        layout_sppaymenttype = view.findViewById(R.id.layout_sppaymenttype)
        layout_customerform = view.findViewById(R.id.layout_customerform)
        tv_packages = view.findViewById(R.id.tv_packages)

        layout_splfeatures.visibility = View.VISIBLE
        layout_coupon.visibility = View.GONE
        layout_payment.visibility = View.VISIBLE
        tv_packages.visibility = View.VISIBLE
        layout_customerform.visibility = View.VISIBLE
        layout_paidamount.visibility = View.VISIBLE
        et_paid_amount.visibility = View.GONE
        layout_sppaymenttype.visibility = View.GONE
        if(coupon_price>0) {
            ll_discount.visibility = View.VISIBLE
        }

        sessionradiogroup = view.findViewById(R.id.sessionradiogroup)
        sessionradiogroup?.setOnCheckedChangeListener { group, checkedId ->
            if (R.id.rd_firsthalf == checkedId) {
                session_type = "First Half"
            }else if (R.id.rd_secondhalf == checkedId){
                session_type = "Second Half"
            }else if (R.id.rd_fullday == checkedId){
                session_type = "Full Day"
            }

            val datajobj = JSONObject()
            datajobj.put("session_type", session_type)
            datajobj.put("to_date", posttodate)
            datajobj.put("from_date",postfromdate)
            if(default_session_type.equals(session_type) && default_postfromdate.equals(postfromdate) && default_posttodate.equals(posttodate)) {
                datajobj.put("booking_id", booking_id)
                tv_base_price.text = "Rs $base_price"
                tv_due_amount.setText("Rs "+String.format("%.0f", str_dueamount.toDouble()))
                //Log.e("paidamount", ""+String.format("%.0f", str_paidamount.toDouble())+"--"+str_dueamount)
            }else{
                tv_base_price.text = "Rs 0"
                tv_due_amount.setText("Rs 0")
                //Log.e("paidamount", ""+String.format("%.0f", str_paidamount.toDouble())+"--"+str_dueamount)
            }
            Log.e("datajobj", "" + datajobj)
            val jsonParser = JsonParser()
            val jsonObject = jsonParser.parse(datajobj.toString()) as JsonObject
            getpackagesApi(jsonObject)
            featureListApi()
        }


        et_from.setOnClickListener {
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)


            val dpd = DatePickerDialog(activity!!, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                // Display Selected date in textbox
                if(monthOfYear+1<10){
                    monthConverted = "0"+(monthOfYear+1);
                }
                et_from.setText("" + dayOfMonth + "-" + monthConverted + "-" + year)
                postfromdate = year.toString() + "-" + monthConverted + "-" + dayOfMonth
                val datajobj = JSONObject()
                datajobj.put("session_type", session_type)
                datajobj.put("to_date", posttodate)
                datajobj.put("from_date",postfromdate)
                if(default_session_type.equals(session_type) && default_postfromdate.equals(postfromdate) && default_posttodate.equals(posttodate)){
                    datajobj.put("booking_id", booking_id)
                    tv_base_price.text = "Rs $base_price"
                    tv_due_amount.setText("Rs "+String.format("%.0f", str_dueamount.toDouble()))
                }else{
                    tv_base_price.text = "Rs 0"
                    tv_due_amount.setText("Rs 0")
                }


                Log.e("datajobj", "" + datajobj)
                val jsonParser = JsonParser()
                val jsonObject = jsonParser.parse(datajobj.toString()) as JsonObject
                getpackagesApi(jsonObject)
                featureListApi()
            }, year, month, day)
            dpd.getDatePicker().setMinDate(c.getTimeInMillis());
            dpd.show()
        }
        et_todate.setOnClickListener {
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)

            val dpd = DatePickerDialog(activity!!, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                if(monthOfYear+1<10){
                    monthConverted = "0"+(monthOfYear+1);
                }
                et_todate.setText("" + dayOfMonth + "-" + monthConverted + "-" + year)
                posttodate = year.toString() + "-" + monthConverted + "-" + dayOfMonth

                val datajobj = JSONObject()
                datajobj.put("session_type", session_type)
                datajobj.put("to_date", posttodate)
                datajobj.put("from_date",postfromdate)
                if(default_session_type.equals(session_type) && default_postfromdate.equals(postfromdate) && default_posttodate.equals(posttodate)){
                    datajobj.put("booking_id", booking_id)
                    tv_base_price.text = "Rs $base_price"
                    tv_due_amount.setText("Rs "+String.format("%.0f", str_dueamount.toDouble()))
                }else{
                    tv_base_price.text = "Rs 0"
                    tv_due_amount.setText("Rs 0")
                }


                Log.e("datajobj", "" + datajobj)
                val jsonParser = JsonParser()
                val jsonObject = jsonParser.parse(datajobj.toString()) as JsonObject
                getpackagesApi(jsonObject)
                featureListApi()
            }, year, month, day)
            dpd.getDatePicker().setMinDate(c.getTimeInMillis());
            dpd.show()
        }

        featuresList = ArrayList()
        //spl_features_list = ArrayList()
        packagesList = ArrayList()
        //spl_packages_list = ArrayList()

        rv_selected_halls = view.findViewById(R.id.rv_selected_halls)
        rv_selected_halls.visibility = View.VISIBLE
        val layoutManager = LinearLayoutManager(activity!!, RecyclerView.VERTICAL, false)
        rv_selected_halls.layoutManager = layoutManager
        rv_selected_halls.setHasFixedSize(true)

        rv_spl_features = view.findViewById(R.id.rv_spl_features)
        rv_spl_features.visibility = View.VISIBLE
        val layoutManager1 = LinearLayoutManager(activity!!, RecyclerView.VERTICAL, false)
        rv_spl_features.layoutManager = layoutManager1
        rv_spl_features.setHasFixedSize(true)

        paymentAdapter = object : ArrayAdapter<String>(
            activity!!,
            R.layout.spinner_item,
            resources.getStringArray(R.array.payment_types)
        ) {
            override fun isEnabled(position: Int): Boolean {
                return position != 0
            }

            override fun getDropDownView(
                position: Int,
                convertView: View?,
                parent: ViewGroup
            ): View {
                val view = super.getDropDownView(position, convertView, parent)
                val tv = view.findViewById(R.id.tv_spinner_header) as TextView
                if (position == 0) {
                    tv.setTextColor(Color.GRAY)
                }
                return view
            }
        }
        paymentAdapter.setDropDownViewResource(R.layout.spinner_item)

        iv_nav_bar.setOnClickListener {
            NavigationDrawerActivity.openDrawer()
        }

        iv_back.setOnClickListener {
            if (NavigationDrawerActivity.navigation_drawer.isDrawerOpen(GravityCompat.START)) {
                NavigationDrawerActivity.navigation_drawer.closeDrawer(GravityCompat.START)
            } else if (activity!!.supportFragmentManager.backStackEntryCount > 0) {
                activity!!.supportFragmentManager.popBackStack()
            }
        }

        tv_apply_coupon.setOnClickListener {
            if (isCouponClicked) {
                et_coupon.setText("")
                isCouponClicked = false
                tv_apply_coupon.text = "Apply"
                et_coupon.isFocusable = true
                et_coupon.isFocusableInTouchMode = true
                tv_coupon_success.visibility = View.GONE
                ll_discount.visibility = View.GONE
                tv_discount_amount.text = "Rs 0"
                coupon_price = 0
                tv_due_amount.text =
                    "Rs ${((base_price - coupon_price) + spl_prices - paid_amount.toInt())}"
            } else {
                if (isInternetConnected!!) {
                    if (et_coupon.text.isNotEmpty()) {
                        couponApi(et_coupon.text.toString())
                    } else {

                        et_coupon.isFocusable = true
                        et_coupon.isFocusableInTouchMode = true
                        Toast.makeText(activity!!, "Empty coupon code!", Toast.LENGTH_SHORT)
                            .show()
                    }
                } else {
                    Toast.makeText(activity!!, "No network connection", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }

        tv_submit.setOnClickListener {
            if (isInternetConnected!!) {

                    if (isValidBookingForm()) {
                        val datajobj = JSONObject()
                        total_price = (base_price + spl_prices).toString()
                        grand_total =
                            ((base_price - coupon_price) + spl_prices - paid_amount.toInt()).toString()
                        datajobj.put("first_name", et_first_name.text.toString())
                        datajobj.put("last_name", et_last_name.text.toString())
                        datajobj.put("mobile", et_mobile_no.text.toString())
                        datajobj.put("alt_mobile", et_alternate_no.text.toString())
                        datajobj.put("email", et_email.text.toString())
                        datajobj.put("booking_id", booking_id)
                        datajobj.put("base_price", base_price)
                        datajobj.put("spcl_price", spl_prices)
                        datajobj.put("total_price", total_price)
                        datajobj.put("grand_total", grand_total)

                        datajobj.put("city", et_city_id.text.toString())
                        datajobj.put("state", et_state_id.text.toString())
                        datajobj.put("address", et_address_id.text.toString())
                        datajobj.put("session_type", session_type)
                        datajobj.put("pincode", et_pincode_id.text.toString())
                        datajobj.put("type_of_event", et_type_of_event_id.text.toString())
                        datajobj.put("to_date", et_todate.text.toString())
                        datajobj.put("from_date", et_from.text.toString())
                        datajobj.put("staff_id", staff_id)

                        obj.put("customer", datajobj)
                        obj.put("halls", jsarray_packages_features)
                        obj.put("features", jsarray_spl_features)

                        Log.e("obj", "" + obj)
                        val jsonParser = JsonParser()
                        val jsonObject = jsonParser.parse(obj.toString()) as JsonObject
                        submitApi(jsonObject)
                    }

            } else {
                Toast.makeText(activity!!, "No network connection", Toast.LENGTH_SHORT)
                    .show()
            }
        }

        sp_payment_type.adapter = paymentAdapter

        sp_payment_type.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parentView: AdapterView<*>, selectedItemView: View, position: Int, id: Long
            ) {
                (parentView.getChildAt(0) as TextView).setTextColor(
                    ContextCompat.getColor(
                        activity!!,
                        R.color.text_color
                    )
                )
                payment_type = parentView.getItemAtPosition(position).toString()
                sp_payment_type.setSelection(position)
                et_acc_holder.setText("")
                et_acc_no.setText("")
                et_ifsc.setText("")
                et_cheque_no.setText("")
                et_bank_name.setText("")
                Log.d("BookingFragment", "payment_type : $payment_type")
                when {
                    payment_type.equals(resources.getString(R.string.cash), true) -> {
                        ll_online_payment.visibility = View.GONE
                        et_cheque_no.visibility = View.GONE
                    }
                    payment_type.equals(resources.getString(R.string.online), true) -> {
                        ll_online_payment.visibility = View.VISIBLE
                        et_acc_holder.visibility = View.VISIBLE
                        et_acc_no.visibility = View.VISIBLE
                        et_ifsc.visibility = View.VISIBLE
                        et_cheque_no.visibility = View.GONE
                    }
                    payment_type.equals(resources.getString(R.string.cheque), true) -> {
                        ll_online_payment.visibility = View.VISIBLE
                        et_cheque_no.visibility = View.VISIBLE
                        et_acc_holder.visibility = View.GONE
                        et_acc_no.visibility = View.GONE
                        et_ifsc.visibility = View.GONE
                    }
                }

                Log.d("BookingFragment", "payment_type : $payment_type")
            }

            override fun onNothingSelected(parentView: AdapterView<*>) {
                // do nothing
            }
        }
        tv_paid_amount.text = "Rs 0"
        //tv_due_amount.text = "Rs 0"
        tv_discount_amount.text = "Rs 0"

        /*et_paid_amount.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, end: Int) {
                tv_paid_amount.text = "Rs 0"
                tv_due_amount.text = "Rs 0"
                paid_amount = 0.toString()
            }

            override fun onTextChanged(s: CharSequence?, start: Int, count: Int, end: Int) {
                if (s!!.isEmpty()) {
                    paid_amount = 0.toString()
                    tv_paid_amount.text = "Rs 0"
                    tv_due_amount.text =
                        "Rs ${((base_price - coupon_price) + spl_prices - paid_amount.toInt())}"
                } else {
                    paid_amount = s.toString()
                    tv_paid_amount.text = "(-) Rs $s"
                    tv_due_amount.text =
                        "Rs ${((base_price - coupon_price) + spl_prices - paid_amount.toInt())}"
                }
            }
        })*/

    }


    private fun isValidBookingForm(): Boolean {
        if (et_first_name.text.toString().isEmpty()) {
            Toast.makeText(activity!!, "First name should not be empty", Toast.LENGTH_SHORT)
                .show()
        } else if (et_last_name.text.toString().isEmpty()) {
            Toast.makeText(activity!!, "Last name should not be empty", Toast.LENGTH_SHORT)
                .show()
        } else if (et_mobile_no.text.toString().isEmpty()) {
            Toast.makeText(activity!!, "Mobile number should not be empty", Toast.LENGTH_SHORT)
                .show()
        }  else if (et_email.text.toString().isEmpty()) {
            Toast.makeText(activity!!, "E-mail should not be empty", Toast.LENGTH_SHORT)
                .show()
        } else if (!isValidEmail(et_email.text.toString().trim())) {
            Toast.makeText(activity!!, "Invalid E-mail address", Toast.LENGTH_SHORT)
                .show()
        } else if (et_address_id.text.toString().isEmpty()) {
            Toast.makeText(activity!!, "Address should not be empty", Toast.LENGTH_SHORT)
                .show()
        } else if (et_city_id.text.toString().isEmpty()) {
            Toast.makeText(activity!!, "City should not be empty", Toast.LENGTH_SHORT)
                .show()
        } else if (et_state_id.text.toString().isEmpty()) {
            Toast.makeText(activity!!, "State should not be empty", Toast.LENGTH_SHORT)
                .show()
        } else if (et_pincode_id.text.toString().isEmpty()) {
            Toast.makeText(activity!!, "Pincode should not be empty", Toast.LENGTH_SHORT)
                .show()
        }  else if (et_type_of_event_id.text.toString().isEmpty()) {
            Toast.makeText(activity!!, "Type of Event should not be empty", Toast.LENGTH_SHORT)
                .show()
        }  else {
            return true
        }
        return false
    }
    private fun loadingDialog() {
        loading_dialog = Dialog(activity!!)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view = LayoutInflater.from(activity!!).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun showSuccessDialog() {
        val dialog = Dialog(activity!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view = LayoutInflater.from(activity!!).inflate(R.layout.success_dialog, null)
        dialog.setContentView(view)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        dialog.setCanceledOnTouchOutside(true)
        val tv_home = view.findViewById(R.id.tv_home) as TextView
        tv_home.setOnClickListener {
            dialog.dismiss()
            // move to dashboard activity
            val intent = Intent(activity!!, NavigationDrawerActivity::class.java)
            startActivity(intent)
            activity!!.finish()

            // move to dashboard fragment
            /* val mFragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
            mFragmentTransaction.add(R.id.content_frame, DashBoardFragment())
            mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
            mFragmentTransaction.commit()*/
        }
        dialog.show()
    }

    private fun isValidEmail(email: String): Boolean {
        val emailPattern =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
        val pattern = Pattern.compile(emailPattern)
        val matcher = pattern.matcher(email)
        return matcher.matches()
    }



    inner class SpecialFeaturesAdapter(context: Context, list: ArrayList<FeaturesDataResponse>) :
        RecyclerView.Adapter<SpecialFeaturesAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var mList: ArrayList<FeaturesDataResponse>? = null


        init {
            this.mContext = context
            mList = list
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {

            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_feature, parent, false)
            return ViewHolder(view)

        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
            holder.tv_amount.text = mList!![position].amount
            holder.checkBox.text = mList!![position].name
            holder.checkBox.setChecked(if (mChecked.get(position) == true) true  else false)
            if(mChecked.get(position) == true){
                holder.increase.isEnabled = true
                holder.decrease.isEnabled = true
                holder.integer_number.text = jsarray_spl_features.getJSONObject(position).getString("no_of_persons")
                holder.tv_amount.text = (jsarray_spl_features.getJSONObject(position).getString("no_of_persons").toInt()*mList!![position].amount!!.toInt()).toString()
                //Log.e("jsarray_spl_features.getJSONObject(position).getString(\"no_of_persons\")",jsarray_spl_features.getJSONObject(position).getString("no_of_persons"))
            }
            holder.checkBox.setOnCheckedChangeListener { compoundButton, isChecked ->
                if (isChecked) {
                    val obj1 = JSONObject()
                    obj1.put("feature_id", mList!![position].id.toString())
                    obj1.put("feature_amount", mList!![position].amount.toString())
                    obj1.put("no_of_persons", holder.integer_number.text.toString())
                    mChecked.put(position,true)
                    jsarray_spl_features.put(obj1)
                    hashMap.put(position,mList!![position].amount!!)
                    holder.increase.isEnabled = true
                    holder.decrease.isEnabled = true
                } else {
                    holder.increase.isEnabled = false
                    holder.decrease.isEnabled = false
                    hashMap.remove(position)
                    mChecked.put(position,false)
                    if (jsarray_spl_features.length() > 0) {
                        for (i in 0 until jsarray_spl_features.length()) {
                            val obj = jsarray_spl_features.getJSONObject(i)
                            if (obj.get("feature_id") == mList!![position].id.toString()) {
                                jsarray_spl_features.remove(i)
                                break
                            }
                            Log.d("jsarray_spl_features", obj.toString())
                        }
                    }
                   /* if (spl_features_list.contains(mList!![position])) {
                        spl_features_list.remove(mList!![position])
                    }*/
                }
               /* Log.d(
                    "adapter",
                    "spl_features_list===== ${spl_features_list.size} ===== $jsarray_spl_features"
                )*/
                if (jsarray_spl_features.length()  > 0) {
                    ll_spl_amount.visibility = View.VISIBLE

                    spl_prices = 0
                    for(key in hashMap.keys){
                        println("Element at key $key = ${hashMap[key]}")
                        spl_prices += hashMap[key]!!.toInt()
                    }

                    Log.e("defaultspl_prices",""+spl_prices)

                    tv_spl_amount.text = "Rs $spl_prices"

                        if (paid_amount.isEmpty()) {
                            tv_due_amount.text = "Rs ${(base_price + spl_prices - 0)}"
                        } else {
                            tv_due_amount.text =
                                "Rs ${((base_price - coupon_price) + spl_prices - paid_amount.toInt())}"
                        }

                } else {
                    //spl_features_list.clear()
                    ll_spl_amount.visibility = View.VISIBLE
                    spl_prices = 0
                    tv_spl_amount.text = "Rs $spl_prices"

                        if (paid_amount.isEmpty()) {
                            tv_due_amount.text = "Rs ${(base_price + spl_prices - 0)}"
                        } else {
                            tv_due_amount.text =
                                "Rs ${((base_price - coupon_price) + spl_prices - paid_amount.toInt())}"
                        }

                }
            }

            holder.increase.setOnClickListener {
                var add = holder.integer_number.text.toString().toInt() + 1
                holder.integer_number.setText("$add")
                holder.tv_amount.setText(""+add.toInt() * mList!![position].amount!!.toInt())

                Log.d("Adding"+position,""+add.toInt() * mList!![position].amount!!.toInt())
                hashMap.put(position,""+add.toInt() * mList!![position].amount!!.toInt())
                spl_prices = 0
                for(key in hashMap.keys){
                    println("Element at key $key = ${hashMap[key]}")
                    spl_prices += hashMap[key]!!.toInt()
                }
                Log.e("defaultspl_prices",""+spl_prices)

                tv_spl_amount.text = "Rs $spl_prices"
                if (paid_amount.isEmpty()) {
                    tv_due_amount.text = "Rs ${(base_price + spl_prices - 0)}"
                } else {
                    tv_due_amount.text =
                        "Rs ${((base_price - coupon_price) + spl_prices - paid_amount.toInt())}"
                }
                if (jsarray_spl_features.length() > 0) {
                    for (i in 0 until jsarray_spl_features.length()) {
                        val obj = jsarray_spl_features.getJSONObject(i)
                        if (obj.get("feature_id") == mList!![position].id.toString()) {
                            jsarray_spl_features.remove(i)
                            break
                        }
                        Log.d("jsarray_spl_features", obj.toString())
                    }
                }
                val obj1 = JSONObject()
                obj1.put("feature_id", mList!![position].id.toString())
                obj1.put("feature_amount", mList!![position].amount.toString())
                obj1.put("no_of_persons", holder.integer_number.text.toString())
                jsarray_spl_features.put(obj1)
            }
            holder.decrease.setOnClickListener {
                var number = holder.integer_number.text.toString().toInt() - 1
                if(number<1){
                    number = 1
                }
                holder.integer_number.setText("$number")
                holder.tv_amount.setText(""+number.toInt() * mList!![position].amount!!.toInt())
                Log.d("minus"+position,""+number.toInt() * mList!![position].amount!!.toInt())
                hashMap.put(position,""+number.toInt() * mList!![position].amount!!.toInt())
                spl_prices = 0
                for(key in hashMap.keys){
                    println("Element at key $key = ${hashMap[key]}")
                    spl_prices += hashMap[key]!!.toInt()
                }
                Log.e("defaultspl_prices",""+spl_prices)

                tv_spl_amount.text = "Rs $spl_prices"
                if (paid_amount.isEmpty()) {
                    tv_due_amount.text = "Rs ${(base_price + spl_prices - 0)}"
                } else {
                    tv_due_amount.text =
                        "Rs ${((base_price - coupon_price) + spl_prices - paid_amount.toInt())}"
                }
                if (jsarray_spl_features.length() > 0) {
                    for (i in 0 until jsarray_spl_features.length()) {
                        val obj = jsarray_spl_features.getJSONObject(i)
                        if (obj.get("feature_id") == mList!![position].id.toString()) {
                            jsarray_spl_features.remove(i)
                            break
                        }
                        Log.d("jsarray_spl_features", obj.toString())
                    }
                }
                val obj1 = JSONObject()
                obj1.put("feature_id", mList!![position].id.toString())
                obj1.put("feature_amount", mList!![position].amount.toString())
                obj1.put("no_of_persons", holder.integer_number.text.toString())
                jsarray_spl_features.put(obj1)
            }

        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var checkBox: CheckBox = view.findViewById(R.id.checkBox)
            var tv_amount: TextView = view.findViewById(R.id.tv_amount)
            var decrease: TextView = view.findViewById(R.id.decrease)
            var increase: TextView = view.findViewById(R.id.increase)
            var integer_number: TextView = view.findViewById(R.id.integer_number)
        }

    }

    inner class PackagesAdapter(context: Context, list: ArrayList<PackagesDataResponse>) :
        RecyclerView.Adapter<PackagesAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var mList: ArrayList<PackagesDataResponse>? = null

        init {
            this.mContext = context
            mList = list
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {

            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_package, parent, false)
            return ViewHolder(view)

        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {

            //holder.tv_amount.text = mList!![position].amount
            holder.checkBox.text = mList!![position].name
            holder.checkBox.setChecked(if (pChecked.get(position) == true) true  else false)

            holder.tv_availablehallspackage.text = mList!![position].hall_names
            //Log.e(" mList!![position].name", ""+position+"=="+mList!![position].available!!.size)
            if(mList!![position].available!!.size == 0){
                holder.tv_availabledate.text = "Not Available"
                holder.checkBox.isEnabled = false
            }else{
                val vaialabdates = mList!![position].available
                val stringsOrNulls = arrayOfNulls<String>(vaialabdates!!.size)
                val senddatestoserver = arrayOfNulls<String>(vaialabdates!!.size)
                val sum_samount = arrayOfNulls<Int>(vaialabdates!!.size)
                for (i in 0 until vaialabdates!!.size) {
                    //  Log.d("loopdate", vaialabdates[i])
                    val inputFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                    val dateee = inputFormat.parse(vaialabdates.get(i).adate)
                    val df1 = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                    val outputDateSend = df1.format(dateee)
                    senddatestoserver[i] = outputDateSend
                    val df = SimpleDateFormat("dd-MM", Locale.getDefault())
                    val outputDateSt = df.format(dateee)
                    stringsOrNulls[i] = outputDateSt
                    sum_samount[i] = vaialabdates.get(i).samount!!.toInt()
                }

                val commaSeperatedString = stringsOrNulls.joinToString { it -> "${it}" }
                val commaSeperatedString1 = senddatestoserver.joinToString { it -> "${it}" }
                holder.tv_availabledate.text = commaSeperatedString
                holder.tv_ssenddate.text = commaSeperatedString1
                val commaSeperatedString11 = sum_samount.sumBy{ it!!.toInt() }
                Log.e("SUM",""+commaSeperatedString11)
                holder.tv_amount.text = commaSeperatedString11.toString()
            }
            holder.checkBox.setOnCheckedChangeListener { compoundButton, isChecked ->
                if (isChecked) {
                    val avail_date_amount =  mList!![position].available
                    pChecked.put(position,true)
                    val avail_date_array = JSONArray()
                    for (i in 0 until avail_date_amount!!.size) {
                        val avail_date_amount_obj = JSONObject()
                        avail_date_amount_obj.put("adate", avail_date_amount.get(i).adate)
                        avail_date_amount_obj.put("samount", avail_date_amount.get(i).samount)
                        avail_date_array.put(avail_date_amount_obj)
                    }
                    val obj1 = JSONObject()
                    obj1.put("hall_id", mList!![position].id.toString())
                    obj1.put("hall_amount", mList!![position].amount.toString())
                    obj1.put("available",  avail_date_array)
                    obj1.put("hall_names", holder.tv_availablehallspackage.text.toString())
                    // val smount = holder.tv_amount.text.toString().toInt()
                    base_price += holder.tv_amount.text.toString().toInt()
                    tv_base_price.text = "Rs $base_price"
                    jsarray_packages_features.put(obj1)
                    //spl_packages_list.add(mList!![position])

                } else {
                    pChecked.put(position,false)
                    if (jsarray_packages_features.length() > 0) {
                        base_price = base_price.toInt() - holder.tv_amount.text.toString().toInt()
                        tv_base_price.text = "Rs $base_price"
                        for (i in 0 until jsarray_packages_features.length()) {
                            val obj = jsarray_packages_features.getJSONObject(i)
                            if (obj.get("hall_id") == mList!![position].id.toString()) {
                                jsarray_packages_features.remove(i)
                                break
                            }
                            Log.d("jsarray_packages_features", obj.toString())
                        }
                    }else {
                        base_price = base_price.toInt() - holder.tv_amount.text.toString().toInt()
                       // spl_packages_list.clear()
                        ll_spl_amount.visibility = View.VISIBLE
                        pkg_prices = 0
                        base_price = 0
                        for(key in hashMap.keys){
                            println("Element at key $key = ${hashMap[key]}")
                            pkg_prices += hashMap[key]!!.toInt()
                        }
                        tv_spl_amount.text = "Rs $pkg_prices"
                        if (paid_amount.isEmpty()) {
                            tv_due_amount.text = "Rs ${(base_price + pkg_prices - 0)}"
                        } else {
                            tv_due_amount.text =
                                "Rs ${((base_price - coupon_price) + pkg_prices - paid_amount.toInt())}"
                        }
                    }
                   /* if (spl_packages_list.contains(mList!![position])) {
                        spl_packages_list.remove(mList!![position])
                    }*/
                }
                /*Log.d(
                    "adapter",
                    "spl_packages_list===== ${spl_packages_list.size} ===== $jsarray_packages_features"
                )*/
                if (jsarray_packages_features.length() > 0) {
                    ll_spl_amount.visibility = View.VISIBLE
                    pkg_prices = 0
                    for(key in hashMap.keys){
                        println("Element at key $key = ${hashMap[key]}")
                        pkg_prices += hashMap[key]!!.toInt()
                    }
                    if (paid_amount.isEmpty()) {
                        tv_due_amount.text = "Rs ${(base_price + pkg_prices - 0)}"
                    } else {
                        tv_due_amount.text =
                            "Rs ${((base_price - coupon_price) + pkg_prices - paid_amount.toInt())}"
                    }
                } else {
                    ll_spl_amount.visibility = View.VISIBLE
                    pkg_prices = 0
                    for(key in hashMap.keys){
                        println("Element at key $key = ${hashMap[key]}")
                        pkg_prices += hashMap[key]!!.toInt()
                    }
                    tv_spl_amount.text = "Rs $pkg_prices"
                    if (paid_amount.isEmpty()) {
                        tv_due_amount.text = "Rs ${(base_price + pkg_prices - 0)}"
                    } else {
                        tv_due_amount.text =
                            "Rs ${((base_price - coupon_price) + pkg_prices - paid_amount.toInt())}"
                    }
                }
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var checkBox: CheckBox = view.findViewById(R.id.checkBox)
            var tv_amount: TextView = view.findViewById(R.id.tv_amount)
            var tv_availabledate: TextView = view.findViewById(R.id.tv_availabledate)
            var tv_availablehallspackage: TextView = view.findViewById(R.id.tv_availablehallspackage)
            var tv_ssenddate: TextView = view.findViewById(R.id.tv_ssenddate)


        }

    }

    private fun submitApi(final_object: JsonObject) {

        loading_dialog.show()

        val apiInterface = ApiInterface.create()

        val call = apiInterface.editBookingApi(final_object)

        call.enqueue(object : Callback<BookedListResponse> {
            override fun onFailure(call: Call<BookedListResponse>, t: Throwable) {
                try{
                    Log.d("BookingFragment", "hallBookingApi error $t")

                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(activity!!, "Please try again!", Toast.LENGTH_SHORT)
                        .show()
                }catch (e:Exception){
                    e.printStackTrace()
                }

            }

            override fun onResponse(
                call: Call<BookedListResponse>,
                response: Response<BookedListResponse>
            ) {

                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Log.d("BookingFragment", "hallBookingApi result ${response.body()!!.result}")
                    val loginResponse = response.body()
                    if (loginResponse!!.status == "1") {
                        showSuccessDialog()
                    } else if (loginResponse.status == "2") {
                        Toast.makeText(activity!!, loginResponse.result, Toast.LENGTH_SHORT)
                            .show()
                    } else {
                        Toast.makeText(
                            activity!!,
                            "Something went wrong, Please try again!",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }
    private fun blockingsubmitApi(final_object: JsonObject) {

        loading_dialog.show()

        val apiInterface = ApiInterface.create()

        val call = apiInterface.hallBlockingApi(final_object)

        call.enqueue(object : Callback<BookedListResponse> {
            override fun onFailure(call: Call<BookedListResponse>, t: Throwable) {
                try{
                    Log.d("BookingFragment", "hallBlokingApi error $t")

                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(activity!!, "Please try again!", Toast.LENGTH_SHORT)
                        .show()
                }catch (e:Exception){
                    e.printStackTrace()
                }

            }

            override fun onResponse(
                call: Call<BookedListResponse>,
                response: Response<BookedListResponse>
            ) {

                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Log.d("BookingFragment", "hallBookingApi result ${response.body()!!.result}")
                    val loginResponse = response.body()
                    if (loginResponse!!.status == "1") {
                        showSuccessDialog()
                    } else if (loginResponse.status == "2") {
                        Toast.makeText(activity!!, loginResponse.result, Toast.LENGTH_SHORT)
                            .show()
                    } else {
                        Toast.makeText(
                            activity!!,
                            "Something went wrong, Please try again!",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }
    private fun getpackagesApi(final_object: JsonObject) {

        loading_dialog.show()

        val apiInterface = ApiInterface.create()
        val call = apiInterface.getPackagesApi(final_object)
        call.enqueue(object : Callback<PackagesResponse> {
            override fun onFailure(call: Call<PackagesResponse>, t: Throwable) {
                try{
                    Log.d("Fragment", "PackagesResponse error $t")
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(activity!!, "Please try again!", Toast.LENGTH_SHORT)
                        .show()
                }catch (e:Exception){
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<PackagesResponse>,
                response: Response<PackagesResponse>
            ) {

                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Log.d("Fragment", "PackagesResponse success")

                    val packagesResponse = response.body()
                    if (packagesResponse!!.status == "1") {
                        packagesList = ArrayList()

                        jsarray_packages_features = JSONArray()

                        val data = packagesResponse.packagesList
                        if (data!!.size > 0) {
                            for (i in 0 until data.size) {
                                pChecked.put(i,false)
                                packagesList.add(data[i])
                            }
                        }
                        if(default_session_type.equals(session_type) && default_postfromdate.equals(postfromdate) && default_posttodate.equals(posttodate)){
                            for (i in 0 until exist_packages.size) {
                                val avail_date_array = JSONArray()
                                val avail_date_amount =  exist_packages!![i].available
                                for (i in 0 until avail_date_amount!!.size) {
                                    val avail_date_amount_obj = JSONObject()
                                    avail_date_amount_obj.put("adate", avail_date_amount.get(i).adate)
                                    avail_date_amount_obj.put("samount", avail_date_amount.get(i).samount)
                                    avail_date_array.put(avail_date_amount_obj)
                                }
                                val obje = JSONObject()
                                obje.put("hall_id", exist_packages!![i].hall_id.toString())
                                obje.put("hall_amount", exist_packages!![i].amount.toString())
                                obje.put("available",  avail_date_array)
                                obje.put("hall_names", exist_packages!![i].package_halls.toString())
                                // val smount = holder.tv_amount.text.toString().toInt()
                                //base_price += holder.tv_amount.text.toString().toInt()
                                //tv_base_price.text = "Rs $base_price"
                                jsarray_packages_features.put(obje)
                                //spl_packages_list.add(mList!![position])

                                //spl_features_list.add(exist_features!![i])
                                for (j in 0 until data.size) {
                                    if (exist_packages.get(i).hall_id.equals(data.get(j).id)) {
                                        pChecked.put(j, true)
                                    }
                                }
                            }
                        }
                        packagesAdapter = PackagesAdapter(activity!!, packagesList)
                        rv_selected_halls.adapter = packagesAdapter
                        packagesAdapter.notifyDataSetChanged()
                    } else {
                        Toast.makeText(activity!!, packagesResponse.result, Toast.LENGTH_SHORT)
                            .show()
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }
    private fun featureListApi() {

        loading_dialog.show()

        val apiInterface = ApiInterface.create()
        val call = apiInterface.featureListApi()
        call.enqueue(object : Callback<FeaturesResponse> {
            override fun onFailure(call: Call<FeaturesResponse>, t: Throwable) {
                try{
                    Log.d("Fragment", "FeaturesResponse error $t")
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(activity!!, "Please try again!", Toast.LENGTH_SHORT)
                        .show()
                }catch (e:Exception){
                    e.printStackTrace()
                }

            }

            override fun onResponse(
                call: Call<FeaturesResponse>,
                response: Response<FeaturesResponse>
            ) {

                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Log.d("Fragment", "FeaturesResponse success")

                    val featuresResponse = response.body()
                    if (featuresResponse!!.status == "1") {
                        featuresList = ArrayList()
                        //spl_features_list.clear()
                        hashMap.clear()
                        jsarray_spl_features = JSONArray()

                        val data = featuresResponse.details
                        Log.e("featuresizes",""+exist_features.size+"--"+data!!.size)
                        if (data!!.size > 0) {
                            for (i in 0 until data.size) {
                                mChecked.put(i,false)
                                featuresList.add(data[i])
                            }
                        }

                        if(default_session_type.equals(session_type) && default_postfromdate.equals(postfromdate) && default_posttodate.equals(posttodate)){
                            for (i in 0 until exist_features.size) {
                                val obj1 = JSONObject()
                                obj1.put("feature_id", exist_features!![i].feature_id.toString())
                                obj1.put("feature_amount", exist_features!![i].feature_amount.toString())
                                obj1.put("no_of_persons", exist_features!![i].no_of_persons.toString())
                                jsarray_spl_features.put(obj1)
                                hashMap.put(i,exist_features!![i].feature_amount!!)
                                //spl_features_list.add(exist_features!![i])
                                for (j in 0 until data.size) {
                                    if (exist_features.get(i).feature_id.equals(data.get(j).id)) {
                                        mChecked.put(j, true)
                                    }
                                }
                            }

                        }

                        Log.e("featuresizes",""+mChecked.size()+"--"+data!!.size)
                        specialFeaturesAdapter = SpecialFeaturesAdapter(activity!!, featuresList)
                        rv_spl_features.adapter = specialFeaturesAdapter
                        specialFeaturesAdapter.notifyDataSetChanged()
                    } else {
                        Toast.makeText(activity!!, featuresResponse.result, Toast.LENGTH_SHORT)
                            .show()
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun couponApi(coupon_code: String) {

        loading_dialog.show()
        coupon_price = 0

        val apiInterface = ApiInterface.create()
        val call = apiInterface.checkCouponApi(coupon_code)

        call.enqueue(object : Callback<CouponCodeResponse> {
            override fun onFailure(call: Call<CouponCodeResponse>, t: Throwable) {
                try{
                    Log.d("BookingFragment", "checkCouponApi error $t")
                    isCouponClicked = false
                    tv_apply_coupon.text = "Apply"
                    et_coupon.isFocusable = true
                    et_coupon.isFocusableInTouchMode = true
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(activity!!, "Please try again!", Toast.LENGTH_SHORT)
                        .show()
                }catch (e:Exception){
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<CouponCodeResponse>,
                response: Response<CouponCodeResponse>
            ) {

                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Log.d("BookingFragment", "checkCouponApi result ${response.body()!!.result}")
                    val couponCodeResponse = response.body()
                    if (couponCodeResponse!!.status == "1") {
                        isCouponClicked = true
                        tv_apply_coupon.text = "Remove"
                        et_coupon.isFocusable = false
                        et_coupon.isFocusableInTouchMode = false
                        val coupondata = couponCodeResponse.coupondata
                        discount_type = coupondata!!.amount_type!!
                        discount_code = coupondata.discount_code!!
                        if (discount_type == "amount") {
                            discount_amount = coupondata.amount!!
                            coupon_price = discount_amount.toInt()
                            discount_total_amount = discount_amount
                            if (paid_amount == "") {
                                paid_amount = 0.toString()
                            }
                            if (discount_amount == "") {
                                ll_discount.visibility = View.GONE
                                tv_coupon_success.visibility = View.GONE
                                tv_discount_amount.text = "Rs 0"
                                tv_due_amount.text =
                                    "Rs ${((base_price - coupon_price) + spl_prices - paid_amount.toInt())}"
                            } else {
                                ll_discount.visibility = View.VISIBLE
                                tv_coupon_success.visibility = View.VISIBLE
                                tv_discount_amount.text = "(-) Rs $coupon_price"
                                tv_due_amount.text =
                                    "Rs ${((base_price - coupon_price) + spl_prices - paid_amount.toInt())}"
                            }
                        } else if (discount_type == "percentage") {
                            discount_amount = coupondata.amount!!
                            val amt = ((base_price *discount_amount.toInt())/100)
                            val percent = base_price - amt
                            coupon_price = amt
                            Log.d("price","base_price ==== $base_price ======= $amt")
                            discount_total_amount = percent.toString()
                            if (paid_amount == "") {
                                paid_amount = 0.toString()
                            }
                            if (discount_amount == "") {
                                ll_discount.visibility = View.GONE
                                tv_coupon_success.visibility = View.GONE
                                tv_discount_amount.text = "Rs 0"
                                tv_due_amount.text =
                                    "Rs ${((base_price - coupon_price) + spl_prices - paid_amount.toInt())}"
                            } else {
                                ll_discount.visibility = View.VISIBLE
                                tv_coupon_success.visibility = View.VISIBLE
                                tv_discount_amount.text = "(-) Rs $coupon_price"
                                tv_due_amount.text =
                                    "Rs ${((base_price - coupon_price) + spl_prices - paid_amount.toInt())}"
                            }
                        }

                    } else if (couponCodeResponse.status == "2") {
                        isCouponClicked = false
                        tv_apply_coupon.text = "Apply"
                        et_coupon.isFocusable = true
                        et_coupon.isFocusableInTouchMode = true
                        Toast.makeText(activity!!, couponCodeResponse.result, Toast.LENGTH_SHORT)
                            .show()
                        ll_discount.visibility = View.GONE
                        tv_coupon_success.visibility = View.GONE
                        tv_discount_amount.text = "Rs 0"
                        tv_due_amount.text =
                            "Rs ${((base_price - coupon_price) + spl_prices - paid_amount.toInt())}"

                    } else {
                        isCouponClicked = false
                        tv_apply_coupon.text = "Apply"
                        et_coupon.isFocusable = true
                        et_coupon.isFocusableInTouchMode = true
                        Toast.makeText(
                            activity!!,
                            "Something went wrong, Please try again!",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }
}
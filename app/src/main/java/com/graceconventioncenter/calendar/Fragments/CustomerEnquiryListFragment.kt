package com.graceconventioncenter.calendar.Activity

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.graceconventioncenter.calendar.ApiInterface.ApiInterface
import com.graceconventioncenter.calendar.ApiInterface.EnquiryDataList
import com.graceconventioncenter.calendar.ApiInterface.EnquiryListResponse
import com.graceconventioncenter.calendar.R
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CustomerEnquiryListFragment  : Fragment() {

    lateinit var rv_enquiry_id:RecyclerView
    private lateinit var iv_back: ImageView
    private lateinit var iv_nav_bar: ImageView
    lateinit var enquiryListArray:ArrayList<EnquiryDataList>
    private var isInternetConnected: Boolean? = false
    lateinit var cd: ConnectionDetector
    lateinit var loading_dialog:Dialog

    private var rootview: View? = null

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {

        if (rootview != null) {
            val parent = rootview!!.parent as ViewGroup
            parent.removeView(rootview)

        }
        try {
            setHasOptionsMenu(true);
            rootview = inflater.inflate(R.layout.activity_customer_enquiry_list, container, false)
            activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)

            loadingDialog()
            cd = ConnectionDetector(activity!!)
            isInternetConnected = cd.isConnectingToInternet

              initialize(rootview!!)


            if (isInternetConnected!!) {
                GetEnquiryList()
            } else {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Toast.makeText(activity!!, "No network connection", Toast.LENGTH_SHORT)
                    .show()
            }

        } catch (e: InflateException) {
            e.printStackTrace()
        }


        return rootview
    }
fun initialize(view:View){
    rv_enquiry_id=view!!.findViewById(R.id.rv_enquiry_id)
    iv_nav_bar = view!!.findViewById(R.id.iv_nav_bar)
    iv_back = view.findViewById(R.id.iv_back)


    iv_nav_bar.setOnClickListener {
        NavigationDrawerActivity.openDrawer()
    }

    iv_back.setOnClickListener {
        if (NavigationDrawerActivity.navigation_drawer.isDrawerOpen(GravityCompat.START)) {
            NavigationDrawerActivity.navigation_drawer.closeDrawer(GravityCompat.START)
        } else if (activity!!.supportFragmentManager.backStackEntryCount > 0) {
            activity!!.supportFragmentManager.popBackStack()
        }
    }
}

    private fun loadingDialog() {
        loading_dialog = Dialog(activity!!)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view = LayoutInflater.from(activity!!).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun GetEnquiryList() {
        enquiryListArray= ArrayList()
        enquiryListArray.clear()
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getenquiryListApi()
        call.enqueue(object : Callback<EnquiryListResponse> {
            override fun onFailure(call: Call<EnquiryListResponse>, t: Throwable) {
                try{
                    loading_dialog.dismiss()
                }catch (e:Exception){
                    e.printStackTrace()
                }

            }

            override fun onResponse(call: Call<EnquiryListResponse>, response: Response<EnquiryListResponse>) {

                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val responsee = response.body()
                    if (responsee!!.status == "1") {
                        for (k in 0 until responsee.enqList!!.size){
                            enquiryListArray.add(responsee.enqList!!.get(k))
                        }

                    } else {
                        Toast.makeText(context, responsee.result, Toast.LENGTH_SHORT)
                            .show()
                    }

                    val vertical_layout = LinearLayoutManager(
                        context
                    )

                    rv_enquiry_id.layoutManager = vertical_layout
                    rv_enquiry_id.itemAnimator = DefaultItemAnimator()
                    val details_adapter_ll = MoreDataAdapter(
                        enquiryListArray, activity!!
                    )
                    //  details_adapter.setHasStableIds(true)
                    rv_enquiry_id.adapter = details_adapter_ll
                    details_adapter_ll!!.notifyDataSetChanged()
                    
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }
    inner class MoreDataAdapter(
        val mServiceList: ArrayList<EnquiryDataList>,
        val context: Context
    ) :
        RecyclerView.Adapter<ViewHolder2>() {


        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder2 {
            return ViewHolder2(
                LayoutInflater.from(context).inflate(
                    R.layout.item_enquiry,
                    parent,
                    false
                )
            )

        }

        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        override fun onBindViewHolder(holder: ViewHolder2, position: Int) {

            holder.tv_event_date.text=mServiceList[position].enquiry_date
            holder.tv_event_time.text=mServiceList[position].event_time
            holder.tv_event_type.text=mServiceList[position].event_type
            holder.tv_contactperson.text=mServiceList[position].name

            holder.cv_enquiry_history.setOnClickListener {

                val intent =Intent(context,EnquiryFullDetails::class.java)
                intent.putExtra("eq_date",mServiceList[position].enquiry_date)
                intent.putExtra("ev_date",mServiceList[position].function_date)
                intent.putExtra("ev_type",mServiceList[position].event_type)
                intent.putExtra("ev_time",mServiceList[position].event_time)
                intent.putExtra("name",mServiceList[position].name)
                intent.putExtra("email",mServiceList[position].email)
                intent.putExtra("mobile",mServiceList[position].mobile_number)
                intent.putExtra("address",mServiceList[position].address)
                startActivity(intent)
                /*val booking_fragment = BookingHistoryDetailviewFragment()
                val bundle = Bundle()
                bundle.putString("booking_id", mList!![position].booking_id)
                bundle.putString("unique_id", mList!![position].unique_id)
                booking_fragment.arguments = bundle
                changeFragment(booking_fragment, true)*/
            }


        }

        // Gets the number of profiles in the list
        override fun getItemCount(): Int {
            return mServiceList.size
        }
    }
    class ViewHolder2(view: View) : RecyclerView.ViewHolder(view) {
        val tv_event_date = view.findViewById<TextView>(R.id.tv_event_date)
        val tv_event_time = view.findViewById<TextView>(R.id.tv_event_time)
        val tv_event_type = view.findViewById<TextView>(R.id.tv_event_type)
        val tv_contactperson = view.findViewById<TextView>(R.id.tv_contactperson)
        val cv_enquiry_history = view.findViewById<CardView>(R.id.cv_enquiry_history)



    }


}

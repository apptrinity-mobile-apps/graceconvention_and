package com.graceconventioncenter.calendar.Fragments

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.graceconventioncenter.calendar.Activity.ConnectionDetector
import com.graceconventioncenter.calendar.Activity.NavigationDrawerActivity
import com.graceconventioncenter.calendar.Adapters.FeaturesListAdapter
import com.graceconventioncenter.calendar.Adapters.PaymentListAdapter
import com.graceconventioncenter.calendar.ApiInterface.*
import com.graceconventioncenter.calendar.Helper.SessionManager
import com.graceconventioncenter.calendar.R
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class BlockingHistoryDetailviewFragment : Fragment() {

    private var rootview: View? = null
    lateinit var sessionManager: SessionManager
    lateinit var cd: ConnectionDetector
    private var isInternetConnected: Boolean? = false
    private lateinit var user_details: HashMap<String, String>
    private var user_name = ""
    private var booking_id = ""
    private var unique_id = ""
    private lateinit var iv_back: ImageView
    private lateinit var iv_nav_bar: ImageView
    private lateinit var rv_payment_history: RecyclerView
    private lateinit var rv_hallslist: RecyclerView
    private lateinit var rv_features_list: RecyclerView
    private lateinit var tv_booking_id: TextView
    private lateinit var tv_booking_date: TextView
    private lateinit var tv_booking_todate: TextView
    private lateinit var tv_contactperson: TextView
    private lateinit var tv_contactnumber: TextView
    private lateinit var tv_cancel_booking: TextView
    private lateinit var tv_make_payment: TextView
    private lateinit var tv_edit_booking: TextView

    /*Payment Information*/
    private lateinit var tv_base_price_id: TextView
    private lateinit var tv_features_price_id: TextView
    private lateinit var tv_total_price_id: TextView
    private lateinit var tv_paid_amount_id: TextView
    private lateinit var tv_discount_id: TextView

    private lateinit var tv_final_due_amount_id: TextView

    private lateinit var hallslistAdapter: HallsListAdapter
    private lateinit var paymentlistAdapter: PaymentListAdapter
    private lateinit var featuresListAdapter: FeaturesListAdapter

    lateinit var loading_dialog: Dialog
    var bookedhallsArrayList = ArrayList<BookingHistoryHallsDataResponse>()
    var paymenthistoryArrayList = ArrayList<BookingHistoryPaymentDataResponse>()
    var featurehistoryArrayList = ArrayList<BookingHistoryFeatureDataResponse>()
    var bookingHistoryDetailDataResponse = BookingHistoryDetailDataResponse()

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {

        if (rootview != null) {
            val parent = rootview!!.parent as ViewGroup
            parent.removeView(rootview)

        }
        try {

            setHasOptionsMenu(true);
            rootview =
                inflater.inflate(R.layout.fragment_booking_history_detailview, container, false)
            initialize(rootview)
            user_details = sessionManager.getUserDetails()
            user_name = user_details[SessionManager.KEY_NAME]!!

            val bundle = arguments!!
            booking_id = bundle.getString("booking_id").toString()
            unique_id = bundle.getString("unique_id").toString()

            loadingDialog()
            if (isInternetConnected!!) {
                if (booking_id != "") {
                    bookedHistoryAPI(booking_id)
                }
            } else {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Toast.makeText(activity!!, "No network connection", Toast.LENGTH_SHORT)
                    .show()
            }
        } catch (e: InflateException) {
            e.printStackTrace()
        }

        return rootview
    }

    private fun initialize(view: View?) {

        sessionManager =
            SessionManager(activity!!)
        cd = ConnectionDetector(
            activity!!
        )
        isInternetConnected = cd.isConnectingToInternet
        user_details = HashMap()


        iv_nav_bar = view!!.findViewById(R.id.iv_nav_bar)
        iv_back = view.findViewById(R.id.iv_back)

        rv_payment_history = view.findViewById(R.id.rv_payment_history)
        rv_payment_history.visibility = View.GONE
        rv_hallslist = view.findViewById(R.id.rv_hallslist)
        rv_features_list = view.findViewById(R.id.rv_features_list)

        tv_booking_id = view.findViewById(R.id.tv_booking_id)
        tv_booking_date = view.findViewById(R.id.tv_booking_date)
        tv_booking_todate = view.findViewById(R.id.tv_booking_tdate)
        tv_contactperson = view.findViewById(R.id.tv_contactperson)
        tv_contactnumber = view.findViewById(R.id.tv_contactnumber)
        tv_cancel_booking = view.findViewById(R.id.tv_cancel_booking)

        tv_make_payment = view.findViewById(R.id.tv_make_payment)
        tv_make_payment.visibility = View.GONE
        tv_edit_booking = view.findViewById(R.id.tv_edit_booking)
        tv_edit_booking.text = "Update to booking"

        tv_base_price_id = view.findViewById(R.id.tv_base_price_id)
        tv_features_price_id = view.findViewById(R.id.tv_features_price_id)
        tv_total_price_id = view.findViewById(R.id.tv_total_price_id)
        tv_paid_amount_id = view.findViewById(R.id.tv_paid_amount_id)
        tv_discount_id = view.findViewById(R.id.tv_discount_id)

        tv_final_due_amount_id = view.findViewById(R.id.tv_final_due_amount_id)


        // disabling navigation drawer
        //NavigationDrawerActivity.disableDrawer()
        // un comment to enable navigation drawer
        iv_nav_bar.setOnClickListener {
            NavigationDrawerActivity.openDrawer()
        }

        iv_back.setOnClickListener {

            if (NavigationDrawerActivity.navigation_drawer.isDrawerOpen(GravityCompat.START)) {
                NavigationDrawerActivity.navigation_drawer.closeDrawer(GravityCompat.START)
            } else if (activity!!.supportFragmentManager.backStackEntryCount > 0) {
                activity!!.supportFragmentManager.popBackStack()
            }
        }

        tv_cancel_booking.setOnClickListener {

            val dialog = Dialog(activity!!)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            val view = LayoutInflater.from(activity!!).inflate(R.layout.cancel_dialog, null)
            dialog.setContentView(view)
            dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
            dialog.window!!.setLayout(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT
            )
            dialog.setCanceledOnTouchOutside(true)
            val et_reason = view.findViewById(R.id.et_reason) as EditText
            et_reason.visibility = View.GONE
            val tv_yes = view.findViewById(R.id.tv_yes) as TextView
            val tv_no = view.findViewById(R.id.tv_no) as TextView
            tv_no.setOnClickListener {
                dialog.dismiss()
            }

            tv_yes.setOnClickListener {
                dialog.dismiss()
                //if (et_reason.text.toString().isNotEmpty()) {
                    if (isInternetConnected!!) {
                        cancelBooking(booking_id)
                    } else {
                        if (loading_dialog.isShowing)
                            loading_dialog.dismiss()
                        Toast.makeText(activity!!, "No network connection", Toast.LENGTH_SHORT)
                            .show()
                    }
              /*  } else {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(activity!!, "Field is mandatory", Toast.LENGTH_SHORT)
                        .show()
                }*/

            }

            dialog.show()
        }

        tv_make_payment.setOnClickListener {
            val edit_booking_fragment = BlockingtobookingPaymentFragment()
            val bundle = Bundle()
            bundle.putSerializable("customer_info", bookingHistoryDetailDataResponse)
            edit_booking_fragment.arguments = bundle
            changeFragment(edit_booking_fragment, true)
        }

        tv_edit_booking.setOnClickListener {
           /* val edit_booking_fragment = EditBookingFragment()
            val bundle = Bundle()
            bundle.putSerializable("customer_info", bookingHistoryDetailDataResponse)
            edit_booking_fragment.arguments = bundle
            changeFragment(edit_booking_fragment, true)*/
            val edit_booking_fragment = BlockingtobookingPaymentFragment()
            val bundle = Bundle()
            bundle.putSerializable("customer_info", bookingHistoryDetailDataResponse)
            edit_booking_fragment.arguments = bundle
            changeFragment(edit_booking_fragment, true)
        }
    }

    private fun changeFragment(toFragment: Fragment, addToBackStack: Boolean) {

        val mFragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        mFragmentTransaction.add(R.id.content_frame, toFragment)
        mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
        if (addToBackStack)
            mFragmentTransaction.addToBackStack(null)
        mFragmentTransaction.commit()

    }

    private fun loadingDialog() {
        loading_dialog = Dialog(activity!!)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view = LayoutInflater.from(activity!!).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    inner class HallsListAdapter(
        context: Context,
        list: ArrayList<BookingHistoryHallsDataResponse>
    ) :
        RecyclerView.Adapter<HallsListAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var mList: ArrayList<BookingHistoryHallsDataResponse>? = null

        init {
            this.mContext = context
            mList = list
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {

            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_hall_detailview, parent, false)
            return ViewHolder(view)

        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
            holder.tv_booking_dates.text = mList!![position].booked_on
            holder.tv_booking_package_name.text = mList!![position].name
            holder.tv_hallsinpackage.text = mList!![position].package_halls
            holder.tv_booking_amount.text = "Price : Rs " + mList!![position].amount
            if (mList!![position].reason.isNullOrEmpty()) {
                holder.tv_cancel_reason.text = "Reason : "
            } else {
                holder.tv_cancel_reason.text = "Reason : " + mList!![position].reason
            }

            if (mList!![position].booking_status.equals("2")) {
                holder.tv_booking_status.text = activity!!.resources.getString(R.string.cancelled)
                holder.tv_cancel_reason.visibility = View.VISIBLE
            } else {
                holder.tv_booking_status.text = activity!!.resources.getString(R.string.cancel)
                holder.tv_cancel_reason.visibility = View.GONE
            }
            holder.ll_cancel_booking.setOnClickListener {
                if (mList!![position].booking_status.equals("2")) {

                } else {

                    val dialog = Dialog(activity!!)
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                    val view = LayoutInflater.from(activity!!).inflate(R.layout.cancel_dialog, null)
                    dialog.setContentView(view)
                    dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
                    dialog.window!!.setLayout(
                        WindowManager.LayoutParams.WRAP_CONTENT,
                        WindowManager.LayoutParams.WRAP_CONTENT
                    )
                    dialog.setCanceledOnTouchOutside(true)
                    val et_reason = view.findViewById(R.id.et_reason) as EditText
                    val tv_yes = view.findViewById(R.id.tv_yes) as TextView
                    val tv_no = view.findViewById(R.id.tv_no) as TextView
                    tv_no.setOnClickListener {
                        dialog.dismiss()
                    }

                    tv_yes.setOnClickListener {
                        dialog.dismiss()
                        if (et_reason.text.toString().isNotEmpty()) {
                            if (isInternetConnected!!) {
                                singleCancelBooking(
                                    mList!![position].booking_id.toString(),
                                    mList!![position].hall_id.toString(), et_reason.text.toString()
                                )
                            } else {
                                if (loading_dialog.isShowing)
                                    loading_dialog.dismiss()
                                Toast.makeText(
                                    activity!!,
                                    "No network connection",
                                    Toast.LENGTH_SHORT
                                )
                                    .show()
                            }
                        } else {
                            if (loading_dialog.isShowing)
                                loading_dialog.dismiss()
                            Toast.makeText(
                                activity!!,
                                "Field is mandatory",
                                Toast.LENGTH_SHORT
                            )
                                .show()
                        }

                    }

                    dialog.show()

                }
            }

        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var ll_cancel_booking: LinearLayout = view.findViewById(R.id.ll_cancel_booking)
            var tv_booking_dates: TextView = view.findViewById(R.id.tv_booking_dates)
            var tv_booking_package_name: TextView = view.findViewById(R.id.tv_booking_package_name)
            var tv_booking_amount: TextView = view.findViewById(R.id.tv_booking_amount)
            var tv_booking_status: TextView = view.findViewById(R.id.tv_booking_status)
            var tv_cancel_reason: TextView = view.findViewById(R.id.tv_cancel_reason)
            var tv_hallsinpackage: TextView = view.findViewById(R.id.tv_hallsinpackage)
        }

    }

    private fun bookedHistoryAPI(booking_id: String) {
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.bookingHistorySingleViewApi(booking_id)
        call.enqueue(object : Callback<BookingHistoryDetailViewResponse> {
            override fun onFailure(call: Call<BookingHistoryDetailViewResponse>, t: Throwable) {
                try {
                    Log.d("BookinghistoryFragment", "bookedhistory error $t")
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(activity!!, "Please try again!", Toast.LENGTH_SHORT)
                        .show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            override fun onResponse(
                call: Call<BookingHistoryDetailViewResponse>,
                response: Response<BookingHistoryDetailViewResponse>
            ) {

                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Log.d("DashboardFragment", "hallsListApi success")
                    val bookingHistoryResponse = response.body()
                    if (bookingHistoryResponse!!.status == "1") {
                        val data = bookingHistoryResponse.data
                        if (data!!.booking_status == "2") {
                            tv_make_payment.isClickable = false
                            tv_edit_booking.isClickable = false
                            tv_cancel_booking.isClickable = false
                            tv_cancel_booking.text =
                                activity!!.resources.getString(R.string.cancelled)
                            tv_make_payment.visibility = View.GONE
                            tv_edit_booking.visibility = View.GONE
                        } else {
                            tv_make_payment.isClickable = true
                            tv_edit_booking.isClickable = true
                            tv_cancel_booking.isClickable = true
                            tv_cancel_booking.text =
                                activity!!.resources.getString(R.string.unblock)
                            tv_make_payment.visibility = View.GONE
                            tv_edit_booking.visibility = View.VISIBLE
                        }
                        bookedhallsArrayList = ArrayList()
                        paymenthistoryArrayList = ArrayList()
                        featurehistoryArrayList = ArrayList()
                        bookingHistoryDetailDataResponse = data!!
                        val halls_data = data.halls_details
                        val payment_data = data.payment_detals
                        val features_data = data.feature_detals

                        val inputFormat =
                            SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                        val outputFormat = SimpleDateFormat("MM-dd-yyyy", Locale.getDefault())
                        val inputDateStr = data.booked_on
                        val date = inputFormat.parse(inputDateStr!!)
                        val outputDateStr = outputFormat.format(date!!)

                        val inputDateStr1 = data.to_date
                        val date1 = inputFormat.parse(inputDateStr1!!)
                        val outputDateStr1 = outputFormat.format(date1!!)

                        tv_booking_id.text = unique_id
                        tv_booking_date.text = outputDateStr
                        tv_booking_todate.text = outputDateStr1
                        tv_contactnumber.text = data.mobile
                        tv_contactperson.text = data.first_name

                        for (item: BookingHistoryHallsDataResponse in halls_data!!.iterator()) {
                            bookedhallsArrayList.add(item)
                        }
                        for (item: BookingHistoryPaymentDataResponse in payment_data!!.iterator()) {
                            paymenthistoryArrayList.add(item)
                        }
                        for (item: BookingHistoryFeatureDataResponse in features_data!!.iterator()) {
                            featurehistoryArrayList.add(item)
                        }

                        val rv_context = rv_hallslist.context
                        val rv_context_pay = rv_payment_history.context
                        val rv_context_features = rv_features_list.context
                        val controller = AnimationUtils.loadLayoutAnimation(
                            rv_context,
                            R.anim.layout_animation_fall_down
                        )
                        val controllerpay = AnimationUtils.loadLayoutAnimation(
                            rv_context_pay,
                            R.anim.layout_animation_fall_down
                        )
                        val controllerfeatures = AnimationUtils.loadLayoutAnimation(
                            rv_context_features,
                            R.anim.layout_animation_fall_down
                        )

                        hallslistAdapter = HallsListAdapter(activity!!, bookedhallsArrayList)
                        rv_hallslist.adapter = hallslistAdapter
                        hallslistAdapter.notifyDataSetChanged()
                        rv_hallslist.setHasFixedSize(true)
                        rv_hallslist.layoutManager = LinearLayoutManager(activity!!)
                        rv_hallslist.scheduleLayoutAnimation()
                        rv_hallslist.layoutAnimation = controller
                        hallslistAdapter.notifyDataSetChanged()

                        paymentlistAdapter =
                            PaymentListAdapter(activity!!, paymenthistoryArrayList)
                        rv_payment_history.adapter = paymentlistAdapter
                        hallslistAdapter.notifyDataSetChanged()
                        rv_payment_history.setHasFixedSize(true)
                        rv_payment_history.layoutManager = LinearLayoutManager(activity!!)
                        rv_payment_history.scheduleLayoutAnimation()
                        rv_payment_history.layoutAnimation = controllerpay
                        paymentlistAdapter.notifyDataSetChanged()

                        featuresListAdapter =
                            FeaturesListAdapter(activity!!, featurehistoryArrayList)
                        rv_features_list.adapter = featuresListAdapter
                        hallslistAdapter.notifyDataSetChanged()
                        rv_features_list.setHasFixedSize(true)
                        rv_features_list.layoutManager = LinearLayoutManager(activity!!)
                        rv_features_list.scheduleLayoutAnimation()
                        rv_features_list.layoutAnimation = controllerfeatures
                        featuresListAdapter.notifyDataSetChanged()

                        /*Payment Details List*/
                        tv_base_price_id.text = "Rs " + data.base_price.toString()
                        tv_features_price_id.text = "Rs " + data.spcl_price.toString()
                        tv_total_price_id.text = "Rs " + data.total_price.toString()
                        tv_paid_amount_id.text = "Rs " + data.paid_amount.toString()
                        tv_discount_id.text = "Rs " + data.discount_total_amount.toString()
                        tv_final_due_amount_id.text = "Rs " + data.due_amount.toString()



                    } else {
                        Toast.makeText(
                            activity!!,
                            bookingHistoryResponse.result,
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun singleCancelBooking(booking_id: String, hall_id: String, reason: String) {
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.singleCancelBookingApi(booking_id, hall_id, reason)
        call.enqueue(object : Callback<LoginResponse> {
            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                try {
                    Log.d("BookinghistoryFragment", "bookedhistory error $t")
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(activity!!, "Please try again!", Toast.LENGTH_SHORT)
                        .show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<LoginResponse>,
                response: Response<LoginResponse>
            ) {

                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Log.d("DashboardFragment", "hallsListApi success")
                    Toast.makeText(
                        activity!!,
                        "Successfully cancelled booking!",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                    bookedHistoryAPI(booking_id)

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun cancelBooking(booking_id: String) {
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.unBlockingApi(booking_id)
        call.enqueue(object : Callback<LoginResponse> {
            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                try {
                    Log.d("BlockinghistoryFragment", "cancelBookingApi error $t")
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(activity!!, "Please try again!", Toast.LENGTH_SHORT)
                        .show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<LoginResponse>,
                response: Response<LoginResponse>
            ) {

                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Log.d("DashboardFragment", "cancelBookingApi success")
                    Toast.makeText(activity!!, response.body()!!.result, Toast.LENGTH_SHORT)
                        .show()

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

}
package com.graceconventioncenter.calendar.Fragments

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.util.Preconditions
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.graceconventioncenter.calendar.Activity.ConnectionDetector
import com.graceconventioncenter.calendar.Activity.NavigationDrawerActivity
import com.graceconventioncenter.calendar.ApiInterface.AddEnquiryResponse
import com.graceconventioncenter.calendar.ApiInterface.ApiInterface
import com.graceconventioncenter.calendar.Helper.SessionManager
import com.graceconventioncenter.calendar.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern
import javax.xml.datatype.DatatypeConstants.MONTHS
import kotlin.collections.HashMap

class CustomerEnquiryFragment : Fragment() {

    private var rootview: View? = null
    lateinit var sessionManager: SessionManager
    lateinit var cd: ConnectionDetector
    private var isInternetConnected: Boolean? = false
    private lateinit var user_details: HashMap<String, String>
    private var user_name = ""
    lateinit var loading_dialog:Dialog
    lateinit var iv_nav_bar:ImageView
    private lateinit var   tv_submit:TextView
    private lateinit var et_enquiry_date_id: EditText
    private lateinit var et_name_id: EditText
    private lateinit var et_address_id: EditText
    private lateinit var et_email_id: EditText
    private lateinit var et_mobile_no_id: EditText
    private lateinit var et_founction_date_id: EditText
    private lateinit var et_event_type_id: EditText
    private lateinit var et_event_time_id: EditText
    var staff_id = ""

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {

        if (rootview != null) {
            val parent = rootview!!.parent as ViewGroup
            parent.removeView(rootview)
        }
        try {
            rootview = inflater.inflate(R.layout.fragment_cust_enquiry, container, false)

        } catch (e: InflateException) {
            e.printStackTrace()
        }
        initialize(rootview)
        user_details = sessionManager.getUserDetails()
        user_name = user_details[SessionManager.KEY_NAME]!!
        staff_id = user_details[SessionManager.KEY_ID]!!

        loadingDialog()

        return rootview
    }


    @SuppressLint("SimpleDateFormat")
    private fun initialize(view: View?) {

        sessionManager = SessionManager(activity!!)
        cd = ConnectionDetector(activity!!)
        isInternetConnected = cd.isConnectingToInternet
        user_details = HashMap()

       iv_nav_bar = view!!.findViewById(R.id.iv_nav_bar)

        iv_nav_bar.setOnClickListener {
            NavigationDrawerActivity.openDrawer()
        }
        tv_submit = view!!.findViewById(R.id.tv_submit)
        et_enquiry_date_id = view.findViewById(R.id.et_enquiry_date_id)
        et_name_id = view.findViewById(R.id.et_name_id)
        et_address_id = view.findViewById(R.id.et_address_id)
        et_email_id = view.findViewById(R.id.et_email_id)
        et_mobile_no_id = view.findViewById(R.id.et_mobile_no_id)
        et_founction_date_id = view.findViewById(R.id.et_founction_date_id)
        et_event_type_id = view.findViewById(R.id.et_event_type_id)
        et_event_time_id = view.findViewById(R.id.et_event_time_id)



        et_enquiry_date_id.setOnClickListener {
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)


            val dpd = DatePickerDialog(activity!!, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                // Display Selected date in textbox

                et_enquiry_date_id.setText("" + dayOfMonth + "-" + (monthOfYear+1) + "-" + year)
            }, year, month, day)

            dpd.show()
        }
        et_founction_date_id.setOnClickListener {
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)


            val dpd = DatePickerDialog(activity!!, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                // Display Selected date in textbox
                et_founction_date_id.setText("" + dayOfMonth + "-" + (monthOfYear+1) + "-" + year)
            }, year, month, day)

            dpd.show()
        }
        et_event_time_id.setOnClickListener {
            val cal = Calendar.getInstance()
            val timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
                cal.set(Calendar.HOUR_OF_DAY, hour)
                cal.set(Calendar.MINUTE, minute)
                et_event_time_id.setText(SimpleDateFormat("HH:mm").format(cal.time).toString())
            }
            TimePickerDialog(activity!!, timeSetListener, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), true).show()
        }


        tv_submit.setOnClickListener {
            if (isInternetConnected!!) {
                if (isValidForm()) {
                    val datajobj = JSONObject()

                    datajobj.put("enquiry_date", et_enquiry_date_id.text.toString())
                    datajobj.put("email", et_email_id.text.toString())
                    datajobj.put("address", et_address_id.text.toString())
                    datajobj.put("mobile_number", et_mobile_no_id.text.toString())
                    datajobj.put("event_type", et_event_type_id.text.toString())
                    datajobj.put("event_time", et_event_time_id.text.toString())
                    datajobj.put("function_date", et_founction_date_id.text.toString())
                    datajobj.put("name", et_name_id.text.toString())
                    datajobj.put("staff_id", staff_id)
                    val jsonParser = JsonParser()
                    val jsonObject = jsonParser.parse(datajobj.toString()) as JsonObject
                    submitApi(jsonObject)
                }
            } else {
                Toast.makeText(activity!!, "No network connection", Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }

    private fun submitApi(final_object: JsonObject) {

        loading_dialog.show()

        val apiInterface = ApiInterface.create()
        val call = apiInterface.addEnquiryApi(final_object)

        call.enqueue(object : Callback<AddEnquiryResponse> {
            override fun onFailure(call: Call<AddEnquiryResponse>, t: Throwable) {
                try{
                    Log.d("BookingFragment", "hallBookingApi error $t")

                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(activity!!, "Please try again!", Toast.LENGTH_SHORT)
                        .show()
                }catch (e:Exception){
                    e.printStackTrace()
                }

            }

            override fun onResponse(
                call: Call<AddEnquiryResponse>,
                response: Response<AddEnquiryResponse>
            ) {

                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Log.d("BookingFragment", "hallBookingApi result ${response.body()!!.result}")
                    val loginResponse = response.body()
                    if (loginResponse!!.status == "1") {
                        showSuccessDialog()
                    } else if (loginResponse.status == "2") {
                        Toast.makeText(activity!!, loginResponse.result, Toast.LENGTH_SHORT)
                            .show()
                    } else {
                        Toast.makeText(
                            activity!!,
                            "Something went wrong, Please try again!",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }



    private fun changeFragment(toFragment: Fragment, addToBackStack: Boolean) {
        val mFragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        mFragmentTransaction.add(R.id.content_frame, toFragment)
        mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
        if (addToBackStack)
            mFragmentTransaction.addToBackStack(null)
        mFragmentTransaction.commit()
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(activity!!)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view = LayoutInflater.from(activity!!).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }
    private fun isValidForm(): Boolean {
        if (et_enquiry_date_id.text.toString().isEmpty()) {
            Toast.makeText(activity!!, "Enquiry Date should not be empty", Toast.LENGTH_SHORT)
                .show()
        } else if (et_name_id.text.toString().isEmpty()) {
            Toast.makeText(activity!!, "Name should not be empty", Toast.LENGTH_SHORT)
                .show()
        } else if (et_address_id.text.toString().isEmpty()) {
            Toast.makeText(activity!!, "Address should not be empty", Toast.LENGTH_SHORT)
                .show()
        } /*else if (et_alternate_no.text.toString().isEmpty()) {
            Toast.makeText(
                activity!!,
                "Alternate Mobile number should not be empty",
                Toast.LENGTH_SHORT
            )
                .show()
        } else if (et_email_id.text.toString().isEmpty()) {
            Toast.makeText(activity!!, "E-mail should not be empty", Toast.LENGTH_SHORT)
                .show()
        } else if (!isValidEmail(et_email_id.text.toString().trim())) {
            Toast.makeText(activity!!, "Invalid E-mail address", Toast.LENGTH_SHORT)
                .show()
        }*/ else if (et_mobile_no_id.text.toString().isEmpty()) {
            Toast.makeText(activity!!, "Mobile Number should not be empty", Toast.LENGTH_SHORT)
                .show()
        } else if (et_founction_date_id.text.toString().isEmpty()) {
            Toast.makeText(activity!!, "Function Date should not be empty", Toast.LENGTH_SHORT)
                .show()
        } else if (et_event_type_id.text.toString().isEmpty()) {
            Toast.makeText(activity!!, "Event Type should not be empty", Toast.LENGTH_SHORT)
                .show()
        }else if (et_event_time_id.text.toString().isEmpty()) {
            Toast.makeText(activity!!, "Event Time should not be empty", Toast.LENGTH_SHORT)
                .show()
        }   else {
            return true
        }
        return false
    }
    private fun isValidEmail(email: String): Boolean {
        val emailPattern =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
        val pattern = Pattern.compile(emailPattern)
        val matcher = pattern.matcher(email)
        return matcher.matches()
    }
    private fun showSuccessDialog() {
        val dialog = Dialog(activity!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view = LayoutInflater.from(activity!!).inflate(R.layout.success_dialog, null)
        dialog.setContentView(view)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        dialog.setCanceledOnTouchOutside(true)
        val tv_home = view.findViewById(R.id.tv_home) as TextView
        tv_home.setOnClickListener {
            dialog.dismiss()
            // move to dashboard activity
            val intent = Intent(activity!!, NavigationDrawerActivity::class.java)
            startActivity(intent)
            activity!!.finish()

            // move to dashboard fragment
            /* val mFragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
            mFragmentTransaction.add(R.id.content_frame, DashBoardFragment())
            mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
            mFragmentTransaction.commit()*/
        }
        dialog.show()
    }


}

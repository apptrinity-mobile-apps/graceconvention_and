package com.graceconventioncenter.calendar.Fragments

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.Typeface
import android.graphics.drawable.AnimatedVectorDrawable
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.*
import android.view.animation.AnimationUtils
import android.view.animation.Interpolator
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.graceconventioncenter.calendar.ApiInterface.ApiInterface
import com.graceconventioncenter.calendar.ApiInterface.DayWiseBookingCountResponse
import com.graceconventioncenter.calendar.ApiInterface.HallsListDataResponse
import com.graceconventioncenter.calendar.ApiInterface.HallsListResponse
import com.graceconventioncenter.calendar.Activity.ConnectionDetector
import com.graceconventioncenter.calendar.Activity.NavigationDrawerActivity
import com.graceconventioncenter.calendar.R
import com.graceconventioncenter.calendar.Helper.SessionManager
import com.squareup.picasso.Picasso
import com.stacktips.view.CalendarListener
import com.stacktips.view.CustomCalendarView
import com.stacktips.view.utils.CalendarUtils.isPastDay
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class DashBoardFragment : Fragment() {

    private var rootview: View? = null
    lateinit var sessionManager: SessionManager
    lateinit var cd: ConnectionDetector
    private var isInternetConnected: Boolean? = false
    private lateinit var user_details: HashMap<String, String>
    private var user_name = ""
    private lateinit var tv_name: TextView
    private lateinit var tv_select_booking: TextView
    private lateinit var tv_empty: TextView
    private lateinit var iv_nav_bar: ImageView
    private lateinit var hallsAvailabilityAdapter: HallsAvailabilityAdapter
    private lateinit var rv_halls: RecyclerView
    private lateinit var calendar_view: CustomCalendarView
    private lateinit var currentCalendar: Calendar
    private lateinit var scrollView: NestedScrollView
    private lateinit var search_id: EditText

    var date_stg = ""
    var date_server_stg = ""
    var month_server_stg = ""
    var year_server_stg = ""
    var isPastDateSelected = false
    lateinit var loading_dialog: Dialog
    private lateinit var tv_book: TextView
    private lateinit var selectedHallsList: ArrayList<HallsListDataResponse>
    private lateinit var selectedDatesList: ArrayList<Date>
    private lateinit var first_half_array: ArrayList<String>
    private lateinit var second_half_array: ArrayList<String>
    private lateinit var full_day_array: ArrayList<String>
    private lateinit var specialDatesList: ArrayList<Date>
    private lateinit var blockDatesList: ArrayList<Date>
    var obj = JSONObject()
    lateinit var iv_search: ImageView

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {

        if (rootview != null) {
            val parent = rootview!!.parent as ViewGroup
            parent.removeView(rootview)
        }
        try {
            rootview = inflater.inflate(R.layout.fragment_dash_board, container, false)

        } catch (e: InflateException) {
            e.printStackTrace()
        }
        initialize(rootview)
        user_details = sessionManager.getUserDetails()
        user_name = user_details[SessionManager.KEY_NAME]!!
        if (user_name == "") {
            tv_name.text = resources.getString(R.string.hi)
        } else {
            tv_name.text = resources.getString(R.string.hi) + " " + user_name
        }
        loadingDialog()

        currentCalendar.add(Calendar.MONTH, 1)
        obj.put("year", currentCalendar.get(Calendar.YEAR))
        obj.put("month", currentCalendar.get(Calendar.MONTH))
        val jsonParser = JsonParser()
        val jsonObject = jsonParser.parse(obj.toString()) as JsonObject
        if (isInternetConnected!!) {
            Log.e("jsonObject", jsonObject.toString())
            bookingDayWiseCountApi(jsonObject)
        } else {
            if (loading_dialog.isShowing)
                loading_dialog.dismiss()
            Toast.makeText(activity!!, "No network connection", Toast.LENGTH_SHORT)
                .show()
        }

        calendar_view.setCalendarListener(object : CalendarListener {
            override fun onDateSelected(date: Date?) {
                loading_dialog.show()
                tv_book.visibility = View.VISIBLE
                rv_halls.visibility = View.VISIBLE
                val df = SimpleDateFormat("MM-dd-yyyy", Locale.getDefault())
                val df1 = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                val mf = SimpleDateFormat("MM", Locale.getDefault())
                val yf = SimpleDateFormat("yyyy", Locale.getDefault())
                month_server_stg = mf.format(date!!)
                year_server_stg = yf.format(date)
                date_stg = df.format(date)
                date_server_stg = df1.format(date)
                tv_select_booking.text = resources.getString(R.string.appointments_for_day)
                isPastDateSelected = isPastDay(date)
                Log.d(
                    "DashBoardFragment",
                    "Selected date is $date_stg=====$date_server_stg====$month_server_stg====$year_server_stg"
                )
                if (selectedDatesList.size > 0) {
                    calendar_view.markSelectedDays(selectedDatesList)
                    calendar_view.multicolorSelecter(
                        selectedDatesList,
                        first_half_array,
                        second_half_array,
                        full_day_array
                    )
                    calendar_view.markDayAsSelectedDay(date)
                }
                if (specialDatesList.size > 0) {
                    calendar_view.markSpecialDays(specialDatesList)
                    calendar_view.markDayAsSelectedDay(date)
                }
                if (blockDatesList.size > 0) {
                    calendar_view.markBlockedDays(blockDatesList)
                    calendar_view.markDayAsSelectedDay(date)
                }
                if (isInternetConnected!!) {
                    hallsListApi(date_server_stg, isPastDateSelected)
                } else {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(activity!!, "No network connection", Toast.LENGTH_SHORT)
                        .show()
                }
            }

            override fun onMonthChanged(date: Date?) {
                tv_select_booking.text = resources.getString(R.string.select_date_for_booking)
                tv_book.visibility = View.GONE
                rv_halls.visibility = View.GONE
                val mf = SimpleDateFormat("MM", Locale.getDefault())
                val yf = SimpleDateFormat("yyyy", Locale.getDefault())
                month_server_stg = mf.format(date!!)
                year_server_stg = yf.format(date)
                Log.d(
                    "DashBoardFragment",
                    "Selected month is $month_server_stg=====$year_server_stg"
                )
                if (isInternetConnected!!) {
                    obj = JSONObject()
                    obj.put("year", year_server_stg)
                    obj.put("month", month_server_stg)
                    val jParser = JsonParser()
                    val jObject = jParser.parse(obj.toString()) as JsonObject
                    bookingDayWiseCountApi(jObject)
                } else {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(activity!!, "No network connection", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        })

        return rootview
    }


    private fun initialize(view: View?) {

        sessionManager = SessionManager(activity!!)
        cd = ConnectionDetector(activity!!)
        isInternetConnected = cd.isConnectingToInternet
        user_details = HashMap()

        tv_name = view!!.findViewById(R.id.tv_name)
        tv_select_booking = view.findViewById(R.id.tv_select_booking)
        iv_nav_bar = view.findViewById(R.id.iv_nav_bar)
        calendar_view = view.findViewById(R.id.calendar_view)
        currentCalendar = Calendar.getInstance(Locale.getDefault())
        tv_book = view.findViewById(R.id.tv_book)
        tv_empty = view.findViewById(R.id.tv_empty)
        scrollView = view.findViewById(R.id.scrollView)
        iv_search = view.findViewById(R.id.iv_search)
        /*val imgr: InputMethodManager =
            activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imgr.showSoftInput(search_id, InputMethodManager.SHOW_IMPLICIT)*/


        rv_halls = view.findViewById(R.id.rv_halls)
        val layoutManager = LinearLayoutManager(activity!!, RecyclerView.VERTICAL, false)
        rv_halls.layoutManager = layoutManager
        rv_halls.setHasFixedSize(true)
        selectedHallsList = ArrayList()
        selectedDatesList = ArrayList()
        specialDatesList = ArrayList()
        blockDatesList = ArrayList()

        first_half_array = ArrayList<String>()
        second_half_array = ArrayList<String>()
        full_day_array = ArrayList<String>()

        iv_nav_bar.setOnClickListener {
            NavigationDrawerActivity.openDrawer()
        }

        tv_book.setOnClickListener {
            val booking_fragment = BookingFragment()
            val bundle = Bundle()
            bundle.putString("date_stg", date_stg)
            bundle.putString("date_server_stg", date_server_stg)
            bundle.putSerializable("selectedHallsList", selectedHallsList)
            booking_fragment.arguments = bundle
            changeFragment(booking_fragment, true)
        }


        iv_search.setOnClickListener {
            val fragment = BookingHistoryFragment()
            val bundle = Bundle()
            bundle.putString("query", "dashboard")
            fragment.arguments = bundle
            changeFragment(fragment, true)
        }

    }


    private fun changeFragment(toFragment: Fragment, addToBackStack: Boolean) {
        val mFragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        mFragmentTransaction.add(R.id.content_frame, toFragment)
        mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
        if (addToBackStack)
            mFragmentTransaction.addToBackStack(null)
        mFragmentTransaction.commit()
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(activity!!)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view = LayoutInflater.from(activity!!).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun focusOnView() {
        Handler().post {
            scrollView.scrollTo(0, (calendar_view.bottom / 2))
        }
    }

    private fun hallsListApi(booking_date: String, pastDateSelected: Boolean) {

        loading_dialog.show()

        val apiInterface = ApiInterface.create()
        val call = apiInterface.hallsListApi(booking_date)
        call.enqueue(object : Callback<HallsListResponse> {
            override fun onFailure(call: Call<HallsListResponse>, t: Throwable) {
                try {
                    Log.d("DashboardFragment", "hallsListApi error $t")
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    rv_halls.visibility = View.GONE
                    tv_empty.visibility = View.VISIBLE
                    Toast.makeText(activity!!, "Please try again!", Toast.LENGTH_SHORT)
                        .show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            override fun onResponse(
                call: Call<HallsListResponse>,
                response: Response<HallsListResponse>
            ) {

                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Log.d("DashboardFragment", "hallsListApi success")

                    selectedHallsList = ArrayList()

                    val hallsListResponse = response.body()
                    if (hallsListResponse!!.status == "1") {
                        val data = hallsListResponse.halls_list
                        val pastList = ArrayList<HallsListDataResponse>()
                        val rv_context = rv_halls.context
                        val controller = AnimationUtils.loadLayoutAnimation(
                            rv_context,
                            R.anim.layout_animation_fall_down
                        )
                        rv_halls.layoutAnimation = controller
                        if (pastDateSelected) {
                            // for older dates booking is disabled
                            for (i in 0 until data!!.size) {
                                // if (data[i].booking_status == "1") {
                                pastList.add(data[i])
                                // }
                            }
                            if (pastList.size > 0) {
                                hallsAvailabilityAdapter =
                                    HallsAvailabilityAdapter(
                                        activity!!,
                                        date_stg,
                                        pastList,
                                        pastDateSelected
                                    )
                                tv_empty.visibility = View.GONE
                                rv_halls.visibility = View.VISIBLE
                            } else {
                                tv_empty.visibility = View.VISIBLE
                                rv_halls.visibility = View.GONE
                            }

                        } else {
                            if (data!!.size > 0) {
                                hallsAvailabilityAdapter =
                                    HallsAvailabilityAdapter(
                                        activity!!,
                                        date_stg,
                                        data,
                                        pastDateSelected
                                    )
                                tv_empty.visibility = View.GONE
                                rv_halls.visibility = View.VISIBLE
                            } else {
                                tv_empty.visibility = View.VISIBLE
                                rv_halls.visibility = View.GONE
                            }
                        }
                        focusOnView()

                        rv_halls.adapter = hallsAvailabilityAdapter
                        hallsAvailabilityAdapter.notifyDataSetChanged()
                        rv_halls.scheduleLayoutAnimation()

                    } else {
                        Toast.makeText(activity!!, hallsListResponse.result, Toast.LENGTH_SHORT)
                            .show()
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun bookingDayWiseCountApi(jsonObject: JsonObject) {

        loading_dialog.show()

        val apiInterface = ApiInterface.create()
        val call = apiInterface.bookingDayWiseCountApi(jsonObject)
        call.enqueue(object : Callback<DayWiseBookingCountResponse> {
            override fun onFailure(call: Call<DayWiseBookingCountResponse>, t: Throwable) {
                try {
                    Log.d("DashboardFragment", "bookingDayWiseCountApi error $t")
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(activity!!, "Please try again!", Toast.LENGTH_SHORT)
                        .show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            override fun onResponse(
                call: Call<DayWiseBookingCountResponse>,
                response: Response<DayWiseBookingCountResponse>
            ) {

                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Log.d("DashboardFragment", "bookingDayWiseCountApi success")
                    val bookingDayWiseCountResponse = response.body()
                    blockDatesList = ArrayList()
                    specialDatesList = ArrayList()
                    selectedDatesList = ArrayList()

                    first_half_array = ArrayList<String>()
                    second_half_array = ArrayList<String>()
                    full_day_array = ArrayList<String>()

                    if (bookingDayWiseCountResponse!!.status == "1") {
                        val data = bookingDayWiseCountResponse.data
                        val specialday = bookingDayWiseCountResponse.specialday
                        val blockedday = bookingDayWiseCountResponse.blockeddays

                        for (i in 0 until data!!.size) {
                            if (data[i].count == "0") {

                            } else {
                                val date_stg = data[i].booked_on
                                Log.d("date_stg", date_stg)
                                val df =
                                    SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                                val date = df.parse(date_stg!!)
                                Log.d("date", "" + date)
                                selectedDatesList.add(date!!)

                                first_half_array.add(data[i].first_half.toString())
                                second_half_array.add(data[i].second_half.toString())
                                full_day_array.add(data[i].full_day.toString())

                            }
                        }

                        if (specialday!!.size > 0) {
                            for (i in 0 until specialday.size) {
                                val date = specialday[i].spcl_date
                                val df1 = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                                val outputDateStr = df1.parse(date!!)

                                specialDatesList.add(outputDateStr!!)
                            }
                        }
                        if (blockedday!!.size > 0) {
                            for (i in 0 until blockedday.size) {
                                val date = blockedday[i].blocked_date
                                val df1 = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                                val outputDateStr = df1.parse(date!!)

                                blockDatesList.add(outputDateStr!!)
                            }
                        }


                        Log.d(
                            "DashboardFragment",
                            "bookingDayWiseCountApi ${selectedDatesList.size}"
                        )


                        if (selectedDatesList.size > 0) {
                            calendar_view.multicolorSelecter(
                                selectedDatesList,
                                first_half_array,
                                second_half_array,
                                full_day_array
                            )
                        }
                        if (specialDatesList.size > 0) {
                            calendar_view.markSpecialDays(specialDatesList)
                        }
                        if (blockDatesList.size > 0) {
                            calendar_view.markBlockedDays(blockDatesList)
                        }

                    } else {
                        Toast.makeText(
                            activity!!,
                            bookingDayWiseCountResponse.result,
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    @SuppressLint("SetTextI18n")
    inner class HallsAvailabilityAdapter(
        context: Context,
        date: String,
        list: ArrayList<HallsListDataResponse>,
        isPastDaySelected: Boolean
    ) :
        RecyclerView.Adapter<HallsAvailabilityAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var mList: ArrayList<HallsListDataResponse>? = null
        private var selectedPositionsList: ArrayList<Int>? = null
        private var selectedList: ArrayList<String>? = null
        private var date_stg = ""
        private var isPastDay: Boolean = false

        init {
            this.mContext = context
            mList = list
            date_stg = date
            selectedList = ArrayList()
            selectedPositionsList = ArrayList()
            isPastDay = isPastDaySelected
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {

            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_hall_types, parent, false)
            return ViewHolder(view)

        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        @SuppressLint("NewApi")
        override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
            var isSelected = false
            if (mList!![position].name == mContext!!.getString(R.string.mini_conf_hall)) {
                Picasso.with(mContext!!)
                    .load(R.drawable.ic_mini_conference_hall)
                    .into(holder.iv_hall_img)
                // holder.iv_hall_img.setImageResource(R.drawable.ic_mini_conference_hall)
            } else if (mList!![position].name == mContext!!.getString(R.string.reception_hall)) {
                Picasso.with(mContext!!)
                    .load(R.drawable.ic_reception_hall)
                    .into(holder.iv_hall_img)
                //  holder.iv_hall_img.setImageResource(R.drawable.ic_reception_hall)
            } else if (mList!![position].name == mContext!!.getString(R.string.main_hall)) {
                Picasso.with(mContext!!)
                    .load(R.drawable.ic_main_hall)
                    .into(holder.iv_hall_img)
                // holder.iv_hall_img.setImageResource(R.drawable.ic_main_hall)
            } else if (mList!![position].name == mContext!!.getString(R.string.basic_package)) {
                Picasso.with(mContext!!)
                    .load(R.drawable.ic_basic_package)
                    .into(holder.iv_hall_img)
                // holder.iv_hall_img.setImageResource(R.drawable.ic_basic_package)
            } else if (mList!![position].name == mContext!!.getString(R.string.superior_package)) {
                Picasso.with(mContext!!)
                    .load(R.drawable.ic_superior_package)
                    .into(holder.iv_hall_img)
                // holder.iv_hall_img.setImageResource(R.drawable.ic_superior_package)
            }
            if (mList!![position].base_price == "") {
                holder.tv_booking_price.text = "Price : Rs 0"
            } else {
                holder.tv_booking_price.text = "Price : Rs ${mList!![position].base_price}"
            }
            holder.tv_booking_availability.text = /*mList!![position].booking_status*/"1"
            holder.tv_booking_package_name.text = mList!![position].name
            holder.tv_hallsinpackage.text = mList!![position].halls_list
            val customer_array = mList!![position].customer

            if (customer_array!!.size > 0) {
                holder.tv_booking_availability.text = "1"
                for (i in 0 until customer_array!!.size) {
                    // Add textviews
                    val textView1 = TextView(context);
                    textView1.setLayoutParams(
                        LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT
                        )
                    );
                    textView1.setText("Session : "+customer_array.get(i).session_type);
                    textView1.setPadding(20, 20, 20, 5); // in pixels (left, top, right, bottom)
                    textView1.setTextColor(resources.getColor(R.color.profile_text))
                    textView1.setTypeface(textView1.getTypeface(), Typeface.BOLD);
                    holder.ll_example.addView(textView1);
                    val textView2 = TextView(context);
                    textView2.setLayoutParams(
                        LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT
                        )
                    );
                    textView2.setText( customer_array.get(i).first_name + " " + mContext!!.getString(R.string.booked_slot));
                    textView2.setPadding(20, 5, 20, 5); // in pixels (left, top, right, bottom)
                    holder.ll_example.addView(textView2);
                    val textView3 = TextView(context);
                    textView3.setLayoutParams(
                        LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT
                        )
                    );
                    textView3.setText( mContext!!.getString(R.string.amount_paid) + " " + customer_array.get(i).paid_amount);
                    textView3.setPadding(20, 5, 20, 5); // in pixels (left, top, right, bottom)
                    holder.ll_example.addView(textView3);
                }
            } else {
                holder.tv_booking_availability.text = "0"
                holder.ll_example.visibility = View.GONE
                holder.tv_contact_details.visibility = View.GONE
            }

            holder.tv_contact_details.setOnClickListener {
                val dialog = Dialog(activity!!)
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                val view = LayoutInflater.from(activity!!).inflate(R.layout.contact_dialog, null)
                dialog.setContentView(view)
                dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
                dialog.window!!.setLayout(
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT
                )
                dialog.setCanceledOnTouchOutside(true)

                val main_layout = view.findViewById(R.id.ll_contact) as LinearLayout
                for (i in 0 until customer_array!!.size) {
                    // Add textviews
                    val textView1 = TextView(activity!!);
                    textView1.setLayoutParams(
                        LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT
                        )
                    );
                    textView1.setText("Session : "+customer_array.get(i).session_type);
                    textView1.setPadding(20, 20, 20, 5); // in pixels (left, top, right, bottom)
                    textView1.setTextColor(resources.getColor(R.color.profile_text))
                    textView1.setTypeface(textView1.getTypeface(), Typeface.BOLD);
                    main_layout.addView(textView1);
                    val textView2 = TextView(activity!!);
                    textView2.setLayoutParams(
                        LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT
                        )
                    );
                    textView2.setText("First Name : "+customer_array.get(i).first_name);
                    textView2.setPadding(20, 20, 20, 5); // in pixels (left, top, right, bottom)
                    textView2.setTextColor(resources.getColor(R.color.card_time))
                    main_layout.addView(textView2);
                    val textView3 = TextView(activity!!);
                    textView3.setLayoutParams(
                        LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT
                        )
                    );
                    textView3.setText("Last Name : "+customer_array.get(i).last_name);
                    textView3.setPadding(20, 20, 20, 5); // in pixels (left, top, right, bottom)
                    textView3.setTextColor(resources.getColor(R.color.card_time))
                    main_layout.addView(textView3);
                    val textView4 = TextView(activity!!);
                    textView4.setLayoutParams(
                        LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT
                        )
                    );
                    textView4.setText("Mobile : "+customer_array.get(i).mobile);
                    textView4.setPadding(20, 20, 20, 5); // in pixels (left, top, right, bottom)
                    textView4.setTextColor(resources.getColor(R.color.card_time))
                    main_layout.addView(textView4);
                    val textView5 = TextView(activity!!);
                    textView5.setLayoutParams(
                        LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT
                        )
                    );
                    textView5.setText("Email : "+customer_array.get(i).email);
                    textView5.setPadding(20, 20, 20, 5); // in pixels (left, top, right, bottom)
                    textView5.setTextColor(resources.getColor(R.color.card_time))
                    main_layout.addView(textView5);
                }
                /* val tv_fname = view.findViewById(R.id.tv_first_name_val) as TextView
                 tv_fname.setText(mList!![position].first_name)
                 val tv_lname = view.findViewById(R.id.tv_last_name_val) as TextView
                 tv_lname.setText(mList!![position].last_name)
                 val tv_mobile = view.findViewById(R.id.tv_mobile_val) as TextView
                 tv_mobile.setText(mList!![position].mobile)
                 val tv_altmobile = view.findViewById(R.id.tv_alt_mobile_val) as TextView
                 tv_altmobile.setText(mList!![position].alt_mobile)
                 val tv_email = view.findViewById(R.id.tv_email_val) as TextView
                 tv_email.setText(mList!![position].email)*/

                val tv_yes = view.findViewById(R.id.tv_yes) as TextView
                tv_yes.setOnClickListener {
                    dialog.dismiss()

                }

                dialog.show()
            }

            /* holder.tv_booking_status.setOnClickListener {
                 if (!isPastDay) {
                     val hallsAvailabilityPojo = mList!![position]
                     if (mList!![position].booking_status == "1") {
                         holder.tv_book_package.visibility = View.GONE
                     } else if (mList!![position].booking_status == "0") {
                         if (isSelected) {
                             isSelected = false
                             if (selectedHallsList.contains(hallsAvailabilityPojo)) {
                                 selectedHallsList.remove(hallsAvailabilityPojo)
                             }
                             holder.tv_booking_status.background = ContextCompat.getDrawable(
                                 mContext!!,
                                 R.drawable.curved_border_transparent_bg
                             )
                             holder.tv_booking_status.text = mContext!!.getString(R.string.select)
                         } else {
                             selectedHallsList.add(hallsAvailabilityPojo)
                             isSelected = true
                             holder.tv_booking_status.background =
                                 ContextCompat.getDrawable(
                                     mContext!!,
                                     R.drawable.curved_border_selected_bg
                                 )
                             holder.tv_booking_status.text = mContext!!.getString(R.string.selected)
                         }
                     }
                     if (selectedHallsList.size > 0) {
                         tv_book.visibility = View.VISIBLE
                     } else {
                         tv_book.visibility = View.VISIBLE
                     }
                     Log.d("adapter", "size===== ${selectedHallsList.size}")
                 }
             }*/
            holder.tv_booking_status.visibility = View.GONE
            holder.iv_edit_booking.setOnClickListener {
                if (!isPastDay) {
                    val edit_booking_fragment = EditBookingFragment()
                    val bundle = Bundle()
                    bundle.putSerializable("customer_info", mList!![position])
                    edit_booking_fragment.arguments = bundle
                    changeFragment(edit_booking_fragment, true)
                }
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

            var tv_booking_availability: TextView = view.findViewById(R.id.tv_booking_availability)
            var tv_booking_package_name: TextView = view.findViewById(R.id.tv_booking_package_name)
            //var tv_booking_name: TextView = view.findViewById(R.id.tv_booking_name)
           // var tv_booking_amount: TextView = view.findViewById(R.id.tv_booking_amount)
            var tv_booking_status: TextView = view.findViewById(R.id.tv_booking_status)
            var tv_contact_details: TextView = view.findViewById(R.id.tv_contact_details)
            var tv_book_package: TextView = view.findViewById(R.id.tv_book_package)
            var tv_empty: TextView = view.findViewById(R.id.tv_empty)
            var tv_booking_price: TextView = view.findViewById(R.id.tv_booking_price)
            var iv_edit_booking: ImageView = view.findViewById(R.id.iv_edit_booking)
            var iv_cancel_booking: ImageView = view.findViewById(R.id.iv_cancel_booking)
            var iv_hall_img: ImageView = view.findViewById(R.id.iv_hall_img)
            var tv_hallsinpackage: TextView = view.findViewById(R.id.tv_hallsinpackage)
            var ll_example: LinearLayout = view.findViewById(R.id.ll_example)

        }

    }

}

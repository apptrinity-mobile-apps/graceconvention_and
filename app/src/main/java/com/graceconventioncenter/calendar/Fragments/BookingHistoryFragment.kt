package com.graceconventioncenter.calendar.Fragments

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.view.animation.AnimationUtils
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.graceconventioncenter.calendar.ApiInterface.ApiInterface
import com.graceconventioncenter.calendar.ApiInterface.BookingHistoryDataResponse
import com.graceconventioncenter.calendar.ApiInterface.BookingHistoryResponse
import com.graceconventioncenter.calendar.Activity.ConnectionDetector
import com.graceconventioncenter.calendar.Activity.NavigationDrawerActivity
import com.graceconventioncenter.calendar.ApiInterface.DayWiseBookingCountResponse
import com.graceconventioncenter.calendar.R
import com.graceconventioncenter.calendar.Helper.SessionManager
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class BookingHistoryFragment : Fragment() {

    private var rootview: View? = null
    lateinit var sessionManager: SessionManager
    lateinit var cd: ConnectionDetector
    private var isInternetConnected: Boolean? = false
    private lateinit var user_details: HashMap<String, String>
    private var user_name = ""
    private lateinit var iv_back: ImageView
    private lateinit var iv_nav_bar: ImageView
    private lateinit var rv_booked_history: RecyclerView
    private lateinit var bookedHistoryAdapter: BookedHistoryAdapter
    var filter_type = ""
    lateinit var loading_dialog: Dialog
    var bookedhistoryArrayList = ArrayList<BookingHistoryDataResponse>()
    lateinit var scrollView: NestedScrollView
    lateinit var et_search: EditText
    lateinit var tv_nodata: TextView
    lateinit var sp_months_type: Spinner
    lateinit var sp_year_type: Spinner
    var search_query = ""
    var selected_year = ""
    var selected_month = ""
    var obj = JSONObject()

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {

        if (rootview != null) {
            val parent = rootview!!.parent as ViewGroup
            parent.removeView(rootview)

        }
        try {
            setHasOptionsMenu(true);
            rootview = inflater.inflate(R.layout.fragment_history_booking, container, false)
            activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
            initialize(rootview)
            loadingDialog()
            val bundle = arguments

            rv_booked_history.requestFocus()
            search_query = bundle!!.getString("query")!!
            if (search_query == "dashboard") {
                val imm =
                    activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.toggleSoftInput(
                    InputMethodManager.SHOW_FORCED,
                    InputMethodManager.HIDE_IMPLICIT_ONLY
                )
                et_search.requestFocus()

            } else if (search_query == "") {
                if (isInternetConnected!!) {
                    bookedHistoryAPI("")
                } else {
                    Toast.makeText(activity!!, "No network connection", Toast.LENGTH_SHORT).show()
                }
            }

            if (isInternetConnected!!) {
                bookedHistoryAPI("")
            } else {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Toast.makeText(activity!!, "No network connection", Toast.LENGTH_SHORT)
                    .show()
            }

        } catch (e: InflateException) {
            e.printStackTrace()
        }


        return rootview
    }

    @SuppressLint("NewApi")
    private fun initialize(view: View?) {

        sessionManager = SessionManager(activity!!)
        cd = ConnectionDetector(activity!!)
        isInternetConnected = cd.isConnectingToInternet
        user_details = HashMap()

        iv_nav_bar = view!!.findViewById(R.id.iv_nav_bar)
        iv_back = view.findViewById(R.id.iv_back)
        scrollView = view.findViewById(R.id.scrollView)
        et_search = view.findViewById(R.id.et_search)
        sp_months_type = view.findViewById(R.id.sp_months_type)
        sp_year_type = view.findViewById(R.id.sp_year_type)
        val monthsnumber = resources.getStringArray(R.array.monthsnumber)
        val currentyear = Calendar.getInstance().get(Calendar.YEAR);
        val year_array = arrayOf<String>(
            (currentyear - 1).toString(),
            currentyear.toString(),
            (currentyear + 1).toString()
        )
        val adapter2 = ArrayAdapter<String>(
            context!!,
            R.layout.spinner_item, year_array
        )
        sp_year_type.adapter = adapter2
        sp_year_type.setSelection(1)
        selected_year = currentyear.toString()

        sp_year_type.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View, position: Int, id: Long
            ) {
                /*Toast.makeText(
                    context,  currentyear.toString()+(currentyear-1).toString() +(currentyear+1).toString()+
                            "" + monthsnumber[position], Toast.LENGTH_SHORT
                ).show()*/
                selected_year = year_array[position]
                if (!selected_month.equals("0")) {
                    obj.put("year", selected_year)
                    obj.put("month", selected_month)
                    val jsonParser = JsonParser()
                    val jsonObject = jsonParser.parse(obj.toString()) as JsonObject

                    Log.e("jsonObject", jsonObject.toString())
                    bookingmonthwiseApi(jsonObject)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // write code to perform some action
            }
        }

        val adapter1 = ArrayAdapter.createFromResource(
            context!!, R.array.months,
            R.layout.spinner_item
        )
        sp_months_type.adapter = adapter1

        sp_months_type.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View, position: Int, id: Long
            ) {
                selected_month = monthsnumber[position]
                if (!selected_month.equals("0")) {
                    obj.put("year", selected_year)
                    obj.put("month", selected_month)
                    val jsonParser = JsonParser()
                    val jsonObject = jsonParser.parse(obj.toString()) as JsonObject

                    Log.e("jsonObject", jsonObject.toString())
                    bookingmonthwiseApi(jsonObject)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // write code to perform some action
            }
        }

        rv_booked_history = view.findViewById(R.id.rv_booked_history)
        rv_booked_history.setHasFixedSize(true)
        rv_booked_history.layoutManager = LinearLayoutManager(activity!!)
        rv_booked_history.scheduleLayoutAnimation()
        val rv_context = rv_booked_history.context
        val controller = AnimationUtils.loadLayoutAnimation(
            rv_context,
            R.anim.layout_animation_fall_down
        )
        rv_booked_history.layoutAnimation = controller

        tv_nodata = view.findViewById(R.id.tv_nodata)
        tv_nodata.visibility = View.GONE

// disabling navigation drawer
//NavigationDrawerActivity.disableDrawer()
// un comment to enable navigation drawer
        iv_nav_bar.setOnClickListener {
            NavigationDrawerActivity.openDrawer()
        }

        iv_back.setOnClickListener {
            val wrapper = ContextThemeWrapper(context, R.style.PopupMenu)
            val popup = PopupMenu(wrapper, iv_back, Gravity.CENTER)
            popup.menuInflater.inflate(R.menu.option_menu, popup.menu)

            popup.gravity = 20
            popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                override fun onMenuItemClick(item_menu: MenuItem): Boolean {
                    val i = item_menu.itemId
                    if (i == R.id.weekly_item) {
                        Toast.makeText(
                            activity!!,
                            "Selected  " + item_menu.title,
                            Toast.LENGTH_SHORT
                        )
                            .show()
                        return true
                    } else return if (i == R.id.monthly_item) {
                        Toast.makeText(
                            activity!!,
                            "Selected  " + item_menu.title,
                            Toast.LENGTH_SHORT
                        )
                            .show()
                        true
                    } else if (i == R.id.halfyearly_item) {
                        Toast.makeText(
                            activity!!,
                            "Selected  " + item_menu.title,
                            Toast.LENGTH_SHORT
                        )
                            .show()
                        true
                    } else if (i == R.id.quarterly_item) {
                        Toast.makeText(
                            activity!!,
                            "Selected  " + item_menu.title,
                            Toast.LENGTH_SHORT
                        )
                            .show()
                        true
                    } else if (i == R.id.yearly_item) {
                        Toast.makeText(
                            activity!!,
                            "Selected  " + item_menu.title,
                            Toast.LENGTH_SHORT
                        )
                            .show()
                        true
                    } else {
                        onMenuItemClick(item_menu)
                    }
                }
            })
            popup.show()
        }

        et_search.setOnClickListener {
            et_search.selectAll()
        }

        et_search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                Log.d("text", "afterTextChanged $p0")
                if (p0.isNullOrEmpty()) {
                    search_query = ""
                } else {
                    search_query = p0.toString()
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                Log.d("text", "beforeTextChanged $p0")
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                Log.d("text", "onTextChanged $p0")
            }

        })

        et_search.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    Log.d("text", search_query)

                    if (isInternetConnected!!) {
                        if (search_query == "") {
                            bookedHistoryAPI("")
                        } else {
                            bookedHistoryAPI(search_query)
                        }
                    } else {
                        Toast.makeText(activity!!, "No network connection", Toast.LENGTH_SHORT)
                            .show()
                    }
                    activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
                    val imm =
                        activity!!.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(view.windowToken, 0)
                    return true
                }
                return false
            }
        })
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(activity!!)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view = LayoutInflater.from(activity!!).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    inner class BookedHistoryAdapter(
        context: Context,
        list: ArrayList<BookingHistoryDataResponse>
    ) :
        RecyclerView.Adapter<BookedHistoryAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var mList: ArrayList<BookingHistoryDataResponse>? = null

        init {
            this.mContext = context
            mList = list
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {

            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_bookinghistory, parent, false)
            return ViewHolder(view)

        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
            holder.tv_contactperson.text =
                mList!![position].first_name + " " + mList!![position].last_name
            holder.tv_booking_id.text = mList!![position].unique_id
            holder.tv_price.text = mList!![position].grand_total
            holder.tv_eventype.text = mList!![position].eventtype
            holder.tv_date.text = mList!![position].booking_date

            holder.cv_booking_history.setOnClickListener {
                val booking_fragment = BookingHistoryDetailviewFragment()
                val bundle = Bundle()
                bundle.putString("booking_id", mList!![position].booking_id)
                bundle.putString("unique_id", mList!![position].unique_id)
                booking_fragment.arguments = bundle
                changeFragment(booking_fragment, true)
            }
            if (mList!![position].booking_status == "2") {
                holder.tv_booked_status.visibility = View.VISIBLE
            } else {
                holder.tv_booked_status.visibility = View.GONE
            }

        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv_booking_id: TextView = view.findViewById(R.id.tv_booking_id)
            var tv_price: TextView = view.findViewById(R.id.tv_price)
            var tv_eventype: TextView = view.findViewById(R.id.tv_eventype)
            var tv_contactperson: TextView = view.findViewById(R.id.tv_contactperson)
            var tv_date: TextView = view.findViewById(R.id.tv_date)
            var tv_booked_status: TextView = view.findViewById(R.id.tv_booked_status)
            var cv_booking_history: CardView = view.findViewById(R.id.cv_booking_history)
        }

    }

    private fun changeFragment(toFragment: Fragment, addToBackStack: Boolean) {

        val mFragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        mFragmentTransaction.add(R.id.content_frame, toFragment)
        mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
        if (addToBackStack)
            mFragmentTransaction.addToBackStack(null)
        mFragmentTransaction.commit()

    }

    private fun bookedHistoryAPI(search_param: String) {
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.bookingHistoryApi(search_param)
        call.enqueue(object : Callback<BookingHistoryResponse> {
            override fun onFailure(call: Call<BookingHistoryResponse>, t: Throwable) {
                try {
                    Log.d("BookinghistoryFragment", "bookedhistory error $t")
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(activity!!, "Please try again! chaiatanya", Toast.LENGTH_SHORT)
                        .show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            override fun onResponse(
                call: Call<BookingHistoryResponse>,
                response: Response<BookingHistoryResponse>
            ) {

                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Log.d("DashboardFragment", "hallsListApi success")
                    bookedhistoryArrayList = ArrayList()
                    val bookingHistoryResponse = response.body()
                    if (bookingHistoryResponse!!.status == "1") {
                        val data = bookingHistoryResponse.details
                        for (item: BookingHistoryDataResponse in data!!.iterator()) {
                            bookedhistoryArrayList.add(item)
                        }
                        Log.d("bookedHistoryAPI", "" + bookedhistoryArrayList.size)
                        if (bookedhistoryArrayList.size == 0) {
                            tv_nodata.visibility = View.VISIBLE
                            rv_booked_history.visibility = View.GONE
                        } else {
                            rv_booked_history.visibility = View.VISIBLE
                            tv_nodata.visibility = View.GONE
                        }
                        bookedHistoryAdapter =
                            BookedHistoryAdapter(activity!!, bookedhistoryArrayList)
                        rv_booked_history.adapter = bookedHistoryAdapter
                        bookedHistoryAdapter.notifyDataSetChanged()

                    } else {

                        Toast.makeText(
                            activity!!,
                            bookingHistoryResponse.result,
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun bookingmonthwiseApi(jsonObject: JsonObject) {

        loading_dialog.show()

        val apiInterface = ApiInterface.create()
        val call = apiInterface.bookingmonthwiseApi(jsonObject)
        call.enqueue(object : Callback<BookingHistoryResponse> {
            override fun onFailure(call: Call<BookingHistoryResponse>, t: Throwable) {
                try {
                    Log.d("bookingmonthwiseApi", "bookingmonthwiseApi error $t")
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(activity!!, "Please try again!", Toast.LENGTH_SHORT)
                        .show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            override fun onResponse(
                call: Call<BookingHistoryResponse>,
                response: Response<BookingHistoryResponse>
            ) {

                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Log.d("bookingmonthwiseApi", "bookingmonthwiseApi success")
                    bookedhistoryArrayList = ArrayList()
                    val bookingHistoryResponse = response.body()
                    if (bookingHistoryResponse!!.status == "1") {
                        val data = bookingHistoryResponse.details
                        for (item: BookingHistoryDataResponse in data!!.iterator()) {
                            bookedhistoryArrayList.add(item)
                        }

                        Log.d("bookedhistoryArrayList", "" + bookedhistoryArrayList.size)
                        if (bookedhistoryArrayList.size == 0) {
                            tv_nodata.visibility = View.VISIBLE
                            rv_booked_history.visibility = View.GONE
                        } else {
                            rv_booked_history.visibility = View.VISIBLE
                            tv_nodata.visibility = View.GONE
                        }
                        bookedHistoryAdapter =
                            BookedHistoryAdapter(activity!!, bookedhistoryArrayList)
                        rv_booked_history.adapter = bookedHistoryAdapter
                        bookedHistoryAdapter.notifyDataSetChanged()

                    } else {
                        Toast.makeText(
                            activity!!,
                            bookingHistoryResponse.result,
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

}
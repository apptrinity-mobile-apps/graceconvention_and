package com.graceconventioncenter.calendar.Fragments

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.graceconventioncenter.calendar.Activity.BlockedFullDetails
import com.graceconventioncenter.calendar.Activity.ConnectionDetector
import com.graceconventioncenter.calendar.Activity.NavigationDrawerActivity
import com.graceconventioncenter.calendar.ApiInterface.ApiInterface
import com.graceconventioncenter.calendar.ApiInterface.BlockingDataList
import com.graceconventioncenter.calendar.ApiInterface.BlockingListResponse
import com.graceconventioncenter.calendar.R
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BlockingListFragment  : Fragment() {

    lateinit var rv_enquiry_id:RecyclerView
    lateinit var tv_type_of_list:TextView
    private lateinit var iv_back: ImageView
    private lateinit var iv_nav_bar: ImageView
    lateinit var blockingListArray:ArrayList<BlockingDataList>
    private var isInternetConnected: Boolean? = false
    lateinit var cd: ConnectionDetector
    lateinit var loading_dialog:Dialog

    private var rootview: View? = null

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {

        if (rootview != null) {
            val parent = rootview!!.parent as ViewGroup
            parent.removeView(rootview)

        }
        try {
            setHasOptionsMenu(true);
            rootview = inflater.inflate(R.layout.activity_customer_enquiry_list, container, false)
            activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)

            loadingDialog()
            cd = ConnectionDetector(activity!!)
            isInternetConnected = cd.isConnectingToInternet

              initialize(rootview!!)


            if (isInternetConnected!!) {
                GetBlockingListApi()
            } else {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Toast.makeText(activity!!, "No network connection", Toast.LENGTH_SHORT)
                    .show()
            }

        } catch (e: InflateException) {
            e.printStackTrace()
        }


        return rootview
    }
fun initialize(view:View){
    rv_enquiry_id=view!!.findViewById(R.id.rv_enquiry_id)
    iv_nav_bar = view!!.findViewById(R.id.iv_nav_bar)
    iv_back = view.findViewById(R.id.iv_back)
    tv_type_of_list = view.findViewById(R.id.tv_type_of_list)

    tv_type_of_list.text="Blocked List"
    iv_nav_bar.setOnClickListener {
        NavigationDrawerActivity.openDrawer()
    }

    iv_back.setOnClickListener {
        if (NavigationDrawerActivity.navigation_drawer.isDrawerOpen(GravityCompat.START)) {
            NavigationDrawerActivity.navigation_drawer.closeDrawer(GravityCompat.START)
        } else if (activity!!.supportFragmentManager.backStackEntryCount > 0) {
            activity!!.supportFragmentManager.popBackStack()
        }
    }
}

    private fun loadingDialog() {
        loading_dialog = Dialog(activity!!)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view = LayoutInflater.from(activity!!).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun GetBlockingListApi() {
        blockingListArray= ArrayList()
        blockingListArray.clear()
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getblockingListApi()
        call.enqueue(object : Callback<BlockingListResponse> {
            override fun onFailure(call: Call<BlockingListResponse>, t: Throwable) {
                try{
                    loading_dialog.dismiss()
                }catch (e:Exception){
                    e.printStackTrace()
                }

            }

            override fun onResponse(call: Call<BlockingListResponse>, response: Response<BlockingListResponse>) {

                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val responsee = response.body()
                    if (responsee!!.status == "1") {
                        for (k in 0 until responsee.blockingList!!.size){
                            blockingListArray.add(responsee.blockingList!!.get(k))
                        }

                    } else {
                        Toast.makeText(context, responsee.result, Toast.LENGTH_SHORT)
                            .show()
                    }

                    val vertical_layout = LinearLayoutManager(
                        context
                    )

                    rv_enquiry_id.layoutManager = vertical_layout
                    rv_enquiry_id.itemAnimator = DefaultItemAnimator()
                    val details_adapter_ll = MoreDataAdapter(
                        blockingListArray, activity!!
                    )
                    //  details_adapter.setHasStableIds(true)
                    rv_enquiry_id.adapter = details_adapter_ll
                    details_adapter_ll!!.notifyDataSetChanged()
                    
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }
    inner class MoreDataAdapter(
        val mServiceList: ArrayList<BlockingDataList>,
        val context: Context
    ) :
        RecyclerView.Adapter<ViewHolder2>() {


        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder2 {
            return ViewHolder2(
                LayoutInflater.from(context).inflate(
                    R.layout.item_blocklist,
                    parent,
                    false
                )
            )

        }

        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        override fun onBindViewHolder(holder: ViewHolder2, position: Int) {

            holder.tv_event_date.text=mServiceList[position].from_date
            holder.tv_event_time.text=mServiceList[position].to_date
            holder.tv_event_type.text=mServiceList[position].type_of_event
            holder.tv_contactperson.text=mServiceList[position].first_name

            holder.cv_enquiry_history.setOnClickListener {
                val booking_fragment = BlockingHistoryDetailviewFragment()
                val bundle = Bundle()
                bundle.putString("booking_id", mServiceList[position].booking_id)
                bundle.putString("unique_id", mServiceList[position].unique_id)
                booking_fragment.arguments = bundle
                changeFragment(booking_fragment, true)
                /*val intent =Intent(context, BlockedFullDetails::class.java)
                intent.putExtra("blocked_id",mServiceList[position].unique_id)
                intent.putExtra("first_name",mServiceList[position].first_name)
                intent.putExtra("last_name",mServiceList[position].last_name)
                intent.putExtra("mobile",mServiceList[position].mobile)
                intent.putExtra("alt_mobile",mServiceList[position].alt_mobile)
                intent.putExtra("email",mServiceList[position].email)
                intent.putExtra("city",mServiceList[position].city)
                intent.putExtra("state",mServiceList[position].state)
                intent.putExtra("pincode",mServiceList[position].pincode)
                intent.putExtra("address",mServiceList[position].address)
                intent.putExtra("message",mServiceList[position].message)
                intent.putExtra("from_date",mServiceList[position].from_date)
                intent.putExtra("to_date",mServiceList[position].to_date)
                intent.putExtra("session_type",mServiceList[position].session_type)
                intent.putExtra("type_of_event",mServiceList[position].type_of_event)

                startActivity(intent)*/
                /*val booking_fragment = BookingHistoryDetailviewFragment()
                val bundle = Bundle()
                bundle.putString("booking_id", mList!![position].booking_id)
                bundle.putString("unique_id", mList!![position].unique_id)
                booking_fragment.arguments = bundle
                changeFragment(booking_fragment, true)*/
            }


        }

        // Gets the number of profiles in the list
        override fun getItemCount(): Int {
            return mServiceList.size
        }
    }
    class ViewHolder2(view: View) : RecyclerView.ViewHolder(view) {
        val tv_event_date = view.findViewById<TextView>(R.id.tv_event_date)
        val tv_event_time = view.findViewById<TextView>(R.id.tv_event_time)
        val tv_event_type = view.findViewById<TextView>(R.id.tv_event_type)
        val tv_contactperson = view.findViewById<TextView>(R.id.tv_contactperson)
        val cv_enquiry_history = view.findViewById<CardView>(R.id.cv_enquiry_history)



    }
    private fun changeFragment(toFragment: Fragment, addToBackStack: Boolean) {

        val mFragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        mFragmentTransaction.add(R.id.content_frame, toFragment)
        mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
        if (addToBackStack)
            mFragmentTransaction.addToBackStack(null)
        mFragmentTransaction.commit()

    }


}

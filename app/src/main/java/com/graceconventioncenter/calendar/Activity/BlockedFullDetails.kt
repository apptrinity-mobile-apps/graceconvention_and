package com.graceconventioncenter.calendar.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import com.graceconventioncenter.calendar.R

class BlockedFullDetails : AppCompatActivity() {

    lateinit var tv_blocked_id:TextView
    lateinit var tv_first_name:TextView
    lateinit var tv_last_name:TextView
    lateinit var tv_mobile_no_id:TextView
    lateinit var tv_alter_mobile_no_id:TextView
    lateinit var tv_email_id:TextView
    lateinit var tv_city_id:TextView
    lateinit var tv_state_id:TextView
    lateinit var tv_pincode_id:TextView
    lateinit var tv_address_id:TextView
    lateinit var tv_msg_id:TextView
    lateinit var tv_from_id:TextView
    lateinit var tv_to_id:TextView
    lateinit var tv_session_id:TextView
    lateinit var tv_type_id:TextView
    lateinit var iv_back:ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_blocked_full_details)

        val blocked_id=intent.getStringExtra("blocked_id")
        val first_name=intent.getStringExtra("first_name")
        val last_name=intent.getStringExtra("last_name")
        val mobile=intent.getStringExtra("mobile")
        val alt_mobile=intent.getStringExtra("alt_mobile")
        val email=intent.getStringExtra("email")
        val city=intent.getStringExtra("city")
        val state=intent.getStringExtra("state")
        val pincode=intent.getStringExtra("pincode")
        val address=intent.getStringExtra("address")
        val message=intent.getStringExtra("message")
        val from_date=intent.getStringExtra("from_date")
        val to_date=intent.getStringExtra("to_date")
        val session_type=intent.getStringExtra("session_type")
        val type_of_event=intent.getStringExtra("type_of_event")

        tv_blocked_id=findViewById(R.id.tv_blocked_id)
        tv_first_name=findViewById(R.id.tv_first_name)
        tv_last_name=findViewById(R.id.tv_last_name)
        tv_mobile_no_id=findViewById(R.id.tv_mobile_no_id)
        tv_alter_mobile_no_id=findViewById(R.id.tv_alter_mobile_no_id)
        tv_email_id=findViewById(R.id.tv_email_id)
        tv_city_id=findViewById(R.id.tv_city_id)
        tv_state_id=findViewById(R.id.tv_state_id)
        tv_pincode_id=findViewById(R.id.tv_pincode_id)
        tv_address_id=findViewById(R.id.tv_address_id)
        tv_msg_id=findViewById(R.id.tv_msg_id)
        tv_from_id=findViewById(R.id.tv_from_id)
        tv_to_id=findViewById(R.id.tv_to_id)
        tv_session_id=findViewById(R.id.tv_session_id)
        tv_type_id=findViewById(R.id.tv_type_id)

        iv_back=findViewById(R.id.iv_back)

        tv_blocked_id.text=blocked_id
        tv_first_name.text=first_name
        tv_last_name.text=last_name
        tv_mobile_no_id.text=mobile
        tv_alter_mobile_no_id.text=alt_mobile
        tv_email_id.text=email
        tv_city_id.text=city
        tv_state_id.text=state
        tv_pincode_id.text=pincode
        tv_address_id.text=address
        tv_msg_id.text=message
        tv_from_id.text=from_date
        tv_to_id.text=to_date
        tv_session_id.text=session_type
        tv_type_id.text=type_of_event


        iv_back.setOnClickListener {
            super.onBackPressed()
        }


    }
}

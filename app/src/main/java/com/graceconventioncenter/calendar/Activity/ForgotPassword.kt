package com.graceconventioncenter.calendar.Activity

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.graceconventioncenter.calendar.ApiInterface.ApiInterface
import com.graceconventioncenter.calendar.ApiInterface.LoginResponse
import com.graceconventioncenter.calendar.Helper.SessionManager
import com.graceconventioncenter.calendar.R
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ForgotPassword : AppCompatActivity() {

    lateinit var et_user_name: EditText
    lateinit var tv_submit: TextView
    lateinit var tv_text_login: TextView
    lateinit var tv_passwordstatus: TextView
    lateinit var tv_textstatus: TextView
    lateinit var tv_submit_login: TextView

    lateinit var sessionManager: SessionManager
    lateinit var cd: ConnectionDetector

    private var isInternetConnected: Boolean? = false
    private lateinit var loading_dialog: Dialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_forgotpassword)

        initialize()
        loadingDialog()
        tv_submit.setOnClickListener {

            if (et_user_name.text.toString().isEmpty()) {
                Toast.makeText(this, "user name should not be empty", Toast.LENGTH_SHORT)
                    .show()
            }  else {
                if (isInternetConnected!!) {
                    forgotpasswordApi(et_user_name.text.toString())
                } else {
                    Toast.makeText(this, "No network connection", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }

        tv_text_login.setOnClickListener {
            val intent =
                Intent(this@ForgotPassword, LoginActivity::class.java)
            startActivity(intent)
        }

        tv_submit_login.setOnClickListener {
            val intent =
                Intent(this@ForgotPassword, LoginActivity::class.java)
            startActivity(intent)
        }



    }

    private fun initialize() {
        et_user_name = findViewById(R.id.et_user_name)
        tv_text_login = findViewById(R.id.tv_text_login)
        tv_textstatus = findViewById(R.id.tv_textstatus)
        tv_submit_login = findViewById(R.id.tv_submit_login)
        tv_passwordstatus = findViewById(R.id.tv_passwordstatus)
        tv_submit = findViewById(R.id.tv_submit)

        sessionManager =
            SessionManager(this@ForgotPassword)
        cd =
            ConnectionDetector(this@ForgotPassword)
        isInternetConnected = cd.isConnectingToInternet
    }

    private fun forgotpasswordApi(email: String) {

        loading_dialog.show()

        val forgotpassword = ApiInterface.create()
        val call = forgotpassword.forgotpassApi(email)
        call.enqueue(object : Callback<LoginResponse> {
            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                try{
                    Log.d("LoginActivity", "loginApi error $t")

                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(this@ForgotPassword, "Please try again!", Toast.LENGTH_SHORT)
                        .show()
                }catch (e:Exception){
                    e.printStackTrace()
                }

            }
            override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {

                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Log.d("LoginActivity", "loginApi success ${response.body()!!.result}")
                    val loginResponse = response.body()
                    if (loginResponse!!.status == "1") {

                        tv_textstatus.text = "Email Has Been Sent!"
                        tv_passwordstatus.text = "Please check your inbox and enter the password"
                        et_user_name.visibility = View.GONE

                        /*Toast.makeText(this@ForgotPassword, loginResponse.result, Toast.LENGTH_SHORT)
                            .show()*/
                       /* val intent =
                            Intent(this@ForgotPassword, LoginActivity::class.java)
                        startActivity(intent)*/
//                        finish()
                    } else if (loginResponse.status == "2") {
                        Toast.makeText(this@ForgotPassword, loginResponse.result, Toast.LENGTH_SHORT)
                            .show()
                    } else {
                        Toast.makeText(
                            this@ForgotPassword,
                            "Something went wrong, Please try again!",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this@ForgotPassword)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view = LayoutInflater.from(this@ForgotPassword).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

}

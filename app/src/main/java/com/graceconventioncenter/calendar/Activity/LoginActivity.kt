package com.graceconventioncenter.calendar.Activity

import android.app.Dialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.graceconventioncenter.calendar.ApiInterface.ApiInterface
import com.graceconventioncenter.calendar.ApiInterface.LoginResponse
import com.graceconventioncenter.calendar.Helper.SessionManager
import com.graceconventioncenter.calendar.R
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

class LoginActivity : AppCompatActivity() {

    lateinit var et_user_name: EditText
    lateinit var et_password: EditText
    lateinit var tv_login: TextView
    lateinit var tv_forgot_password: TextView

    lateinit var sessionManager: SessionManager
    lateinit var cd: ConnectionDetector

    private var isInternetConnected: Boolean? = false
    private lateinit var loading_dialog: Dialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_login)

        initialize()
        loadingDialog()
        if (sessionManager.isLoggedIn) {
            // loginApi directly
            val intent = Intent(this@LoginActivity, NavigationDrawerActivity::class.java)
            startActivity(intent)
            finish()
        }

        tv_login.setOnClickListener {

            if (et_user_name.text.toString().isEmpty()) {
                Toast.makeText(this, "user name should not be empty", Toast.LENGTH_SHORT)
                    .show()
            } else if (et_password.text.toString().isEmpty()) {
                Toast.makeText(this, "password should not be empty", Toast.LENGTH_SHORT)
                    .show()
            } else {
                if (isInternetConnected!!) {
                    loginApi(et_user_name.text.toString(), et_password.text.toString())
                } else {
                    Toast.makeText(this, "No network connection", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }

        tv_forgot_password.setOnClickListener {
            val i = Intent(this@LoginActivity,
                ForgotPassword::class.java)
            startActivity(i)
            finish()
        }

    }

    private fun initialize() {
        et_user_name = findViewById(R.id.et_user_name)
        et_password = findViewById(R.id.et_password)
        tv_login = findViewById(R.id.tv_login)
        tv_forgot_password = findViewById(R.id.tv_forgot_password)

        sessionManager =
            SessionManager(this@LoginActivity)
        cd =
            ConnectionDetector(this@LoginActivity)
        isInternetConnected = cd.isConnectingToInternet
    }

    private fun loginApi(email: String, password: String) {

        loading_dialog.show()
        var staff_name = ""
        var staff_email = ""
        var staff_id = ""

        val login = ApiInterface.create()
        val call = login.loginApi(email, password)
        call.enqueue(object : Callback<LoginResponse> {
            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                try{
                    Log.d("LoginActivity", "loginApi error $t")

                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(this@LoginActivity, "Please try again!", Toast.LENGTH_SHORT)
                        .show()
                }catch (e:Exception){
                    e.printStackTrace()
                }

            }

            override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {

                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Log.d("LoginActivity", "loginApi success ${response.body()!!.result}")
                    val loginResponse = response.body()
                    if (loginResponse!!.status == "1") {
                        val data = loginResponse.admin_info
                        staff_name = data!!.staff_name!!
                        staff_email = data.staff_email!!
                        staff_id = data.staff_id!!
                        sessionManager.loginSession(
                            staff_email,
                            staff_name,
                            staff_id
                        )
                        val intent =
                            Intent(this@LoginActivity, NavigationDrawerActivity::class.java)
                        startActivity(intent)
                        finish()
                    } else if (loginResponse.status == "2") {
                        Toast.makeText(this@LoginActivity, loginResponse.result, Toast.LENGTH_SHORT)
                            .show()
                    } else {
                        Toast.makeText(
                            this@LoginActivity,
                            "Something went wrong, Please try again!",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this@LoginActivity)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view = LayoutInflater.from(this@LoginActivity).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

}

package com.graceconventioncenter.calendar.Activity

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.graceconventioncenter.calendar.ApiInterface.ApiInterface
import com.graceconventioncenter.calendar.ApiInterface.LoginResponse
import com.graceconventioncenter.calendar.Helper.SessionManager
import com.graceconventioncenter.calendar.R
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ChangePassword : AppCompatActivity() {

    lateinit var et_curr_password: EditText
    lateinit var et_new_password: EditText
    lateinit var tv_submit: TextView
    lateinit var iv_back: ImageView

    lateinit var sessionManager: SessionManager
    lateinit var user_details: HashMap<String, String>
    lateinit var cd: ConnectionDetector

    private var isInternetConnected: Boolean? = false
    private lateinit var loading_dialog: Dialog
    var staff_id = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_changepassword)

        initialize()
        user_details = HashMap()
        user_details = sessionManager.getUserDetails()
        staff_id = user_details[SessionManager.KEY_ID]!!

        loadingDialog()
        tv_submit.setOnClickListener {
            if (isInternetConnected!!) {
                if (isValid()) {
                    changePasswordApi(
                        staff_id,
                        et_curr_password.text.toString(),
                        et_new_password.text.toString()
                    )
                }
            } else {
                Toast.makeText(this@ChangePassword, "No network connection", Toast.LENGTH_SHORT)
                    .show()
            }
        }

        iv_back.setOnClickListener {
            onBackPressed()
        }

    }

    private fun initialize() {
        et_curr_password = findViewById(R.id.et_curr_password)
        et_new_password = findViewById(R.id.et_new_password)
        tv_submit = findViewById(R.id.tv_submit)
        iv_back = findViewById(R.id.iv_back)

        sessionManager =
            SessionManager(this@ChangePassword)
        cd =
            ConnectionDetector(this@ChangePassword)
        isInternetConnected = cd.isConnectingToInternet
    }

    private fun isValid(): Boolean {
        if (et_curr_password.text.toString().isEmpty()) {
            Toast.makeText(this@ChangePassword, "Field should not be empty!", Toast.LENGTH_SHORT)
                .show()
        } else if (et_new_password.text.toString().isEmpty()) {
            Toast.makeText(this@ChangePassword, "Field should not be empty!", Toast.LENGTH_SHORT)
                .show()
        } else if (et_curr_password.text.toString() == et_new_password.text.toString()) {
            Toast.makeText(this@ChangePassword, "Passwords should not be same!", Toast.LENGTH_SHORT)
                .show()
        } else {
            return true
        }

        return false
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this@ChangePassword)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view = LayoutInflater.from(this@ChangePassword).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun changePasswordApi(staff_id: String, curr_password: String, new_password: String) {

        loading_dialog.show()

        val apiInterface = ApiInterface.create()
        val call = apiInterface.changePasswordApi(staff_id, curr_password, new_password)
        call.enqueue(object : Callback<LoginResponse> {
            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {

                try{
                    Log.d("LoginActivity", "loginApi error $t")
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(this@ChangePassword, "Please try again!", Toast.LENGTH_SHORT)
                        .show()
                }catch (e:Exception){
                    e.printStackTrace()
                }

            }

            override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {

                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Log.d("LoginActivity", "loginApi success ${response.body()!!.result}")
                    val responsee = response.body()
                    if (responsee!!.status == "1") {
                        Toast.makeText(
                            this@ChangePassword,
                            "Password changed successfully!",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                        sessionManager.logout()
                        val i = Intent(this@ChangePassword, LoginActivity::class.java)
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(i)
                        finish()
                    } else {
                        Toast.makeText(this@ChangePassword, responsee.result, Toast.LENGTH_SHORT)
                            .show()
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

}

package com.graceconventioncenter.calendar.Activity

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.graceconventioncenter.calendar.Adapters.NavigationAdapter
import com.graceconventioncenter.calendar.Fragments.BlockingListFragment
import com.graceconventioncenter.calendar.Fragments.BookingHistoryFragment
import com.graceconventioncenter.calendar.Fragments.CustomerEnquiryFragment
import com.graceconventioncenter.calendar.Fragments.DashBoardFragment
import com.graceconventioncenter.calendar.Helper.RecyclerItemClickListener
import com.graceconventioncenter.calendar.Helper.SessionManager
import com.graceconventioncenter.calendar.R


@SuppressLint("StaticFieldLeak")
class NavigationDrawerActivity : AppCompatActivity() {

    lateinit var sessionManager: SessionManager
    lateinit var cd: ConnectionDetector

    private var isInternetConnected: Boolean? = false
    private lateinit var user_details: HashMap<String, String>
    private var user_name = ""

    private lateinit var actionBarDrawerToggle: ActionBarDrawerToggle
    private lateinit var mFragmentManager: FragmentManager
    private lateinit var navigationAdapter: NavigationAdapter
    private lateinit var rv_nav_list: RecyclerView
    private var header: Array<String>? = null
    private var selected_item = ""

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.navigation_drawer_activity)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)

        initialize()

        user_details = sessionManager.getUserDetails()
        user_name = user_details[SessionManager.KEY_NAME]!!

        if (savedInstanceState == null) {
            val tx = supportFragmentManager.beginTransaction()
            tx.replace(R.id.content_frame, CustomerEnquiryFragment())
            tx.commit()
        }

        rv_nav_list.addOnItemTouchListener(
            RecyclerItemClickListener(this, rv_nav_list,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        selected_item = navigationAdapter.getItem(position)
                        navigation_drawer.closeDrawer(GravityCompat.START)
                        navigationAdapter.setSelected(position)
                        when (selected_item) {
                            resources.getString(R.string.booking_history) -> {
                                val fragment = BookingHistoryFragment()
                                val bundle = Bundle()
                                bundle.putString("query", "")
                                fragment.arguments = bundle
                                changeFragment(fragment, true)
                            }
                            resources.getString(R.string.dash_board) -> {
                                changeFragment(CustomerEnquiryFragment(), false)
                            }
                            resources.getString(R.string.booking_calender) -> {
                                changeFragment(DashBoardFragment(), true)
                            }
                            resources.getString(R.string.change_password) -> {
                                val i = Intent(
                                    this@NavigationDrawerActivity,
                                    ChangePassword::class.java
                                )
                                startActivity(i)

                            }

                            resources.getString(R.string.cust_enquiry)->{
                                changeFragment(CustomerEnquiryListFragment(), true)
                            }
                            resources.getString(R.string.blocked_list)->{
                                changeFragment(BlockingListFragment(), true)
                            }
                            resources.getString(R.string.logout) -> {
                                val alertDialog = AlertDialog.Builder(this@NavigationDrawerActivity)
                                alertDialog.setTitle("Logout")
                                alertDialog.setMessage("Do you want to Logout?")
                                alertDialog.setPositiveButton(
                                    "Yes"
                                ) { dialog, p1 ->
                                    dialog.dismiss()
                                    sessionManager.logout()
                                    val i = Intent(
                                        this@NavigationDrawerActivity,
                                        LoginActivity::class.java
                                    )
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                    i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                                    startActivity(i)
                                    finish()
                                }
                                alertDialog.setNegativeButton(
                                    "No"
                                ) { dialog, p1 -> dialog.dismiss() }
                                alertDialog.show()
                            }
                        }
                    }
                })
        )

    }
    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        val view = currentFocus
        val ret = super.dispatchTouchEvent(event)
        if (view is EditText) {
            val w = currentFocus
            val location = IntArray(2)
            w!!.getLocationOnScreen(location)
            val x = event.rawX + w.left - location[0]
            val y = event.rawY + w.top - location[1]
            if (event.action == MotionEvent.ACTION_DOWN
                && (x < w.left || x >= w.right || y < w.top || y > w.bottom)
            ) {
                val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(window.currentFocus!!.windowToken, 0)
            }
        }
        return ret
    }
    private fun initialize() {

        sessionManager =
            SessionManager(this@NavigationDrawerActivity)
        cd =
            ConnectionDetector(this@NavigationDrawerActivity)
        isInternetConnected = cd.isConnectingToInternet
        navigation_drawer = findViewById(
            R.id.navigation_drawer
        )
        actionBarDrawerToggle =
            ActionBarDrawerToggle(
                this,
                navigation_drawer,
                R.string.app_name,
                R.string.app_name
            )

        rv_nav_list = findViewById(R.id.rv_nav_list)
        val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_nav_list.layoutManager = layoutManager
        rv_nav_list.setHasFixedSize(true)

        navigation_drawer.addDrawerListener(actionBarDrawerToggle)
        actionBarDrawerToggle.syncState()

        header = arrayOf(
            resources.getString(R.string.dash_board),
            resources.getString(R.string.cust_enquiry),
            resources.getString(R.string.booking_calender),
            resources.getString(R.string.booking_history),
            resources.getString(R.string.blocked_list),
            resources.getString(R.string.change_password),
            resources.getString(R.string.logout)
        )

        navigationAdapter = NavigationAdapter(this, header!!)
        rv_nav_list.adapter = navigationAdapter
        navigationAdapter.notifyDataSetChanged()

        mFragmentManager = supportFragmentManager

    }

    private fun changeFragment(fragment: Fragment, addToBackStack: Boolean) {
        val mFragmentTransaction = mFragmentManager.beginTransaction()
        mFragmentTransaction.add(R.id.content_frame, fragment)
        mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
        if (addToBackStack)
            mFragmentTransaction.addToBackStack(null)

        mFragmentTransaction.commit()

    }

    override fun onBackPressed() {
        when {
            navigation_drawer.isDrawerOpen(GravityCompat.START) -> {
                navigation_drawer.closeDrawer(GravityCompat.START)
            }
            mFragmentManager.backStackEntryCount > 0 -> {
                mFragmentManager.popBackStack()
            }
            else -> {
                finish()
            }
        }
    }

    companion object {
        internal lateinit var navigation_drawer: DrawerLayout

        fun openDrawer() {
            navigation_drawer.openDrawer(GravityCompat.START)
        }

        fun disableDrawer() {
            navigation_drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        }
    }
}

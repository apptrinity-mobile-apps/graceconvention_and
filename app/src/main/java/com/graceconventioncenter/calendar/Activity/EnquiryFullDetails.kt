package com.graceconventioncenter.calendar.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import com.graceconventioncenter.calendar.R

class EnquiryFullDetails : AppCompatActivity() {

    lateinit var tv_enquiry_date_id:TextView
    lateinit var tv_event_date_id:TextView
    lateinit var tv_event_type:TextView
    lateinit var tv_event_time_id:TextView
    lateinit var tv_contact_person_id:TextView
    lateinit var tv_email_id:TextView
    lateinit var tv_mobile_no_id:TextView
    lateinit var tv_address_id:TextView
    lateinit var iv_back:ImageView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enquiry_full_details)


        val eq_date=intent.getStringExtra("eq_date")
        val ev_date=intent.getStringExtra("ev_date")
        val ev_type=intent.getStringExtra("ev_type")
        val ev_time=intent.getStringExtra("ev_time")
        val name=intent.getStringExtra("name")
        val email=intent.getStringExtra("email")
        val mobile=intent.getStringExtra("mobile")
        val address=intent.getStringExtra("address")


        tv_enquiry_date_id=findViewById(R.id.tv_enquiry_date_id)
        tv_event_date_id=findViewById(R.id.tv_event_date_id)
        tv_event_type=findViewById(R.id.tv_event_type)
        tv_event_time_id=findViewById(R.id.tv_event_time_id)
        tv_contact_person_id=findViewById(R.id.tv_contact_person_id)
        tv_email_id=findViewById(R.id.tv_email_id)
        tv_mobile_no_id=findViewById(R.id.tv_mobile_no_id)
        tv_address_id=findViewById(R.id.tv_address_id)
        iv_back=findViewById(R.id.iv_back)

        tv_enquiry_date_id.text=eq_date
        tv_event_date_id.text=ev_date
        tv_event_type.text=ev_type
        tv_event_time_id.text=ev_time
        tv_contact_person_id.text=name
        tv_email_id.text=email
        tv_mobile_no_id.text=mobile
        tv_address_id.text=address

        iv_back.setOnClickListener {
            super.onBackPressed()
        }


    }
}
